﻿using System;
using System.Linq;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.WebStore
{
    public static class GlobalAttributeHelper
    {
        //returns the value of store level global attribute to show price to logged-in users only
        public static bool IsShowPriceToLoggedInUsersOnly()
            => Convert.ToBoolean(PortalAgent.CurrentPortal.GlobalAttributes?.Attributes?.FirstOrDefault(x => string.Equals(x.AttributeCode, "LoginToSeePricing", StringComparison.InvariantCultureIgnoreCase))?.AttributeValue);

        //returns the value of store level global attribute to show inventory of default and 'all warehouses combined' associated to current store. 
        public static bool IsShowAllLocationsInventory()
            => Convert.ToBoolean(PortalAgent.CurrentPortal.GlobalAttributes?.Attributes?.FirstOrDefault(x => string.Equals(x.AttributeCode, "DisplayAllWarehousesStock", StringComparison.InvariantCultureIgnoreCase))?.AttributeValue);

        //returns the value of store level global attribute to check Cloudflare is Enable or disable. 
        public static bool IsCloudflareEnabled()
            => Convert.ToBoolean(PortalAgent.CurrentPortal.GlobalAttributes?.Attributes?.FirstOrDefault(x => string.Equals(x.AttributeCode, "IsCloudflareEnabled", StringComparison.InvariantCultureIgnoreCase))?.AttributeValue);

        //gets the value of user level global attribute to show message set for user by Admin.  
        public static string GetUserMessage()
        {
            UserViewModel userViewModel = SessionHelper.GetDataFromSession<UserViewModel>(WebStoreConstants.UserAccountKey);
            return userViewModel?.UserGlobalAttributes?.FirstOrDefault(x => string.Equals(x.AttributeCode, "UserMessage", StringComparison.InvariantCultureIgnoreCase))?.AttributeValue;
        }

        //returns the value of store level global attribute to check placing Quote Request is Enable or disable for store. 
        public static bool IsQuoteRequestEnabled()
          => Convert.ToBoolean(PortalAgent.CurrentPortal.GlobalAttributes?.Attributes?.FirstOrDefault(x => string.Equals(x.AttributeCode, "EnableQuoteRequest", StringComparison.InvariantCultureIgnoreCase))?.AttributeValue);

        //returns the value of store level global attribute which signifies the number of days in which quote will get expire
        public static int GetQuoteExpireDays()
        => Convert.ToInt32(PortalAgent.CurrentPortal.GlobalAttributes?.Attributes?.FirstOrDefault(x => string.Equals(x.AttributeCode, "QuoteExpireInDays", StringComparison.InvariantCultureIgnoreCase))?.AttributeValue);

    }
}
