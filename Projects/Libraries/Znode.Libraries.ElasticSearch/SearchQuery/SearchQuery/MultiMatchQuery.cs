﻿using Nest;

using System.Collections.Generic;

using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Search;

namespace Znode.Libraries.ElasticSearch
{
    public class MultiMatchQueryBuilder : BaseQuery, ISearchQuery
    {
        #region Protected Variables
        protected readonly FunctionScoreQuery functionScoreQuery = new FunctionScoreQuery();
        protected readonly BoolQuery finalBoolQuery = new BoolQuery();
        #endregion

        #region Public Methods

        public virtual SearchRequest<dynamic> GenerateQuery(IZnodeSearchRequest request)
        {
          MultiMatchQuery multiMatchQuery = new MultiMatchQuery();
            List<Field> multipleFields = new List<Field>();

            foreach (ElasticSearchAttributes item in request.SearchableAttibute)
            {
                multipleFields.Add(new Field(item.AttributeCode.ToLower(), item.BoostValue));
            }

            multiMatchQuery.Query = request.SearchText;
            multiMatchQuery.Fields = multipleFields.ToArray();

            if (!string.IsNullOrEmpty(GetFeatureValue(request, "MinimumShouldMatch")))
                multiMatchQuery.MinimumShouldMatch = GetFeatureValue(request, "MinimumShouldMatch");

            multiMatchQuery.Operator = GetOperator(request);

            switch (request.SubQueryType?.ToLower())
            {
                /*Nivi Code Changed TextQueryType.Best to TextQueryType.BoolPrefix*/
                case "best":
                    multiMatchQuery.Type = TextQueryType.BoolPrefix;
                    break;

                case "most":
                    multiMatchQuery.Type = TextQueryType.MostFields;
                    break;

                case "cross":
                    multiMatchQuery.Type = TextQueryType.CrossFields;
                    multiMatchQuery.Analyzer = "standard";
                    break;

                case "phrase":
                    multiMatchQuery.Type = TextQueryType.Phrase;
                    break;
            }

            //Check if auto correct is active.
            if (IsFeatureActive(request, "AutoCorrect"))
            {
                multiMatchQuery.Fuzziness = Fuzziness.Auto;
            }

            functionScoreQuery.ScoreMode = FunctionScoreMode.Average;
            functionScoreQuery.BoostMode = FunctionBoostMode.Average;

            functionScoreQuery.Functions = AddFunctionToSearchQuery(request);

            finalBoolQuery.Must = new List<QueryContainer> { multiMatchQuery };

            //If boost and bury conditions are not present then add condition for exact Match first.
            CheckAndAddBoostQueryForMultiMatchQuery(request);

            //set conditions for boost or bury the products.
            BoolQuery boostAndBuryCondition = GetBoostOrBuryItem(request);

            finalBoolQuery.Should = new List<QueryContainer> { boostAndBuryCondition };

            finalBoolQuery.Filter = request.FilterValues;

            functionScoreQuery.Query = finalBoolQuery;

            AggregationBase aggregationBase = null;

            if (request.GetCategoriesHeirarchy)
                aggregationBase = GetCategoryAggregation();

            if (request.GetFacets && request.FacetableAttribute?.Count > 0)
                aggregationBase = GetFacetAggregation(aggregationBase, request.FacetableAttribute);

            List<ISort> SortSettings = AddSortToSearchQuery(request);

            SearchRequest<dynamic> searchRequest = new SearchRequest<dynamic>(request.IndexName);
            searchRequest.Query = functionScoreQuery;
            searchRequest.From = request.StartIndex;
            searchRequest.Size = request.PageSize;

            if (SortSettings.Count > 0)
                searchRequest.Sort = SortSettings;

            if (HelperUtility.IsNotNull(aggregationBase))
                searchRequest.Aggregations = aggregationBase;

            searchRequest.Suggest = GetSuggestionQuery(request);


            if (IsFeatureActive(request, "DfsQueryThenFetch"))
            {
                searchRequest.SearchType = Elasticsearch.Net.SearchType.DfsQueryThenFetch;
            }

            return searchRequest;
        }

        #endregion
    }
}
