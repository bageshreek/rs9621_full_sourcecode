﻿namespace Znode.Engine.Api.Cache
{
    public interface IDashboardCache
    {
        /// <summary>
        /// Gets the list of top products
        /// </summary>
        /// <param name="routeUri">routeUri for top product list</param>
        /// <param name="routeTemplate">routeTemplate for top product list</param>
        /// <returns>returns the list of top products as string</returns>
        string GetDashboardTopProducts(string routeUri, string routeTemplate);

        /// <summary>
        /// Gets the list of top categories
        /// </summary>
        /// <param name="routeUri">routeUri for top categories list</param>
        /// <param name="routeTemplate">routeTemplate for top categories list</param>
        /// <returns>returns the list of top categories as string</returns>
        string GetDashboardTopCategories(string routeUri, string routeTemplate);

        /// <summary>
        /// Gets the list of top brands
        /// </summary>
        /// <param name="routeUri">routeUri for top brands list</param>
        /// <param name="routeTemplate">routeTemplate for top brands list</param>
        /// <returns>returns the list of top brands as string</returns>
        string GetDashboardTopBrands(string routeUri, string routeTemplate);

        /// <summary>
        /// Gets the list of top searches
        /// </summary>
        /// <param name="routeUri">routeUri for top brands list</param>
        /// <param name="routeTemplate">routeTemplate for top brands list</param>
        /// <returns>returns the list of top searches as string</returns>
        string GetDashboardTopSearches(string routeUri, string routeTemplate);

        /// <summary>
        /// Gets the total orders, total new customers, total sales and average total sales
        /// </summary>
        /// <param name="routeUri">routeUri for total orders, total new customers, total sales and average total sales</param>
        /// <param name="routeTemplate">routeTemplate for total orders, total new customers, total sales and average total sales</param>
        /// <returns>returns total orders, total new customers, total sales and average total saless</returns>
        string GetDashboardSalesDetails(string routeUri, string routeTemplate);

        /// <summary>
        /// Gets dashboard low inventory product count
        /// </summary>
        /// <param name="routeUri">routeUri for low inventory product count</param>
        /// <param name="routeTemplate">routeTemplate for low inventory product count</param>
        /// <returns>returns the dashboard low inventory product count</returns>
        string GetDashboardLowInventoryProductCount(string routeUri, string routeTemplate);
    }
}
