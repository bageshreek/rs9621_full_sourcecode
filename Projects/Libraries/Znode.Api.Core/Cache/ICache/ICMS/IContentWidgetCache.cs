﻿
namespace Znode.Engine.Api.Cache
{
    public interface IContentWidgetCache
    {

        /// <summary>
        /// Get List
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>List</returns>
        string List(string routeUri, string routeTemplate);

        /// <summary>
        /// Get Content Widget
        /// </summary>
        /// <param name="widgetKey">widgetKey</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>ContentWidgetResponseModel</returns>
        string GetContentWidget(string widgetKey, string routeUri, string routeTemplate);

        /// <summary>
        /// Get Associated Variant
        /// </summary>
        /// <param name="widgetKey">widgetKey</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate"><routeTemplate/param>
        /// <returns>List of Associated Variant</returns>
        string GetAssociatedVariants(string widgetKey, string routeUri, string routeTemplate);

    }
}
