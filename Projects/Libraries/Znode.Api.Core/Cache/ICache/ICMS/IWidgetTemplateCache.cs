﻿

namespace Znode.Engine.Api.Cache
{
    public interface IWidgetTemplateCache
    {
        /// <summary>
        /// Get the List of Widget Templates
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns></returns>
        string List(string routeUri, string routeTemplate);

        /// <summary>
        /// Get widget Template
        /// </summary>
        /// <param name="templateCode">templateCode</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>WidgetTemplateModel model</returns>
        string GetWidgetTemplate(string templateCode, string routeUri, string routeTemplate);


    }
}
