﻿namespace Znode.Engine.Api.Cache
{
    public interface IAnalyticsCache
    {
        /// <summary>
        /// Gets analytics dashboard data
        /// </summary>
        /// <param name="routeUri">URI to route</param>
        /// <param name="routeTemplate">Template of route</param>
        /// <returns>Response in string format</returns>
        string GetAnalyticsDashboardData(string routeUri, string routeTemplate);

        /// <summary>
        /// Gets analytics JSON key
        /// </summary>
        /// <param name="routeUri">URI to route</param>
        /// <param name="routeTemplate">Template of route</param>
        /// <returns>Response in string format</returns>
        string GetAnalyticsJSONKey(string routeUri, string routeTemplate);
    }
}
