﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Api.Cache
{
    public class ContentWidgetCache : BaseCache, IContentWidgetCache
    {

        #region Private Variables
        private readonly IContentWidgetService _service;
        #endregion

        #region Constructor
        public ContentWidgetCache(IContentWidgetService contentWidgetService)
        {
            _service = contentWidgetService;
        }
        #endregion

        //Get List
        public virtual string List(string routeUri, string routeTemplate)
        {
            //Get data from cache.
            string data = GetFromCache(routeUri);
            if (string.IsNullOrEmpty(data))
            {
                ContentWidgetListModel list = _service.List(Expands, Filters, Sorts, Page);
                if (list?.WidgetList?.Count > 0)
                {
                    //Create response.
                    ContentWidgetListResponseModel response = new ContentWidgetListResponseModel { WidgetList = list.WidgetList };

                    //apply pagination parameters.
                    response.MapPagingDataFromModel(list);
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        //Get Content Widget
        public virtual string GetContentWidget(string widgetKey, string routeUri, string routeTemplate)
        {
            //Get data from Cache
            string data = GetFromCache(routeUri);
            if (string.IsNullOrEmpty(data))
            {
                //Create Response
                ContentWidgetResponseModel model = _service.GetContentWidget(widgetKey);
                if (HelperUtility.IsNotNull(model))
                {
                    ContentWidgetResponse response = new ContentWidgetResponse { ContentWidgetModel = model };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        //Get Associated Variant
        public virtual string GetAssociatedVariants(string widgetKey, string routeUri, string routeTemplate)
        {
            string data = GetFromCache(routeUri);
            if (string.IsNullOrEmpty(data))
            {

                List<AssociatedVariantModel> model = _service.GetAssociatedVariants(widgetKey);
                if (HelperUtility.IsNotNull(model))
                {
                    ContentWidgetListResponseModel response = new ContentWidgetListResponseModel { AssociatedVariants = model };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }

            }
            return data;
        }
    }
}
