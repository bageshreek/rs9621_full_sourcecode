﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.Api.Models
{
    public class ApplicationSettingListModel : BaseListModel
    {
        public List<ApplicationSettingDataModel> ApplicationSettingList { get; set; }

        public ApplicationSettingListModel()
        {
            ApplicationSettingList = new List<ApplicationSettingDataModel>();
        }
    }
}