﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Libraries.Framework.Business;

namespace Znode.Engine.Api.Controllers
{
    public class AnalyticsController : BaseController
    {
        #region Private Variables

        private readonly IAnalyticsCache _cache;
        private readonly IAnalyticsService _service;

        #endregion

        #region Constructor
        public AnalyticsController(IAnalyticsService service)
        {
            _service = service;
            _cache = new AnalyticsCache(_service);
        }
        #endregion

        #region Public Methods

        /// <summary>
        ///Get analytics dashboard data
        /// </summary>
        /// <returns>Analytics dashboard data</returns>
        [ResponseType(typeof(AnalyticsResponse))]
        [HttpGet]
        public virtual HttpResponseMessage GetAnalyticsDashboardData()
        {
            HttpResponseMessage response;

            try
            {
                string data = _cache.GetAnalyticsDashboardData(RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse<AnalyticsResponse>(data) : CreateNoContentResponse();
            }
            catch (ZnodeException ex)
            {
                response = CreateInternalServerErrorResponse(new AnalyticsResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Reports.ToString(), TraceLevel.Warning);
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new AnalyticsResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Reports.ToString(), TraceLevel.Error);
            }
            return response;
        }

        /// <summary>
        ///Get analytics JSON key
        /// </summary>
        /// <returns>Analytics JSON key</returns>
        [ResponseType(typeof(AnalyticsResponse))]
        [HttpGet]
        public virtual HttpResponseMessage GetAnalyticsJSONKey()
        {
            HttpResponseMessage response;

            try
            {
                string data = _cache.GetAnalyticsJSONKey(RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse<AnalyticsResponse>(data) : CreateNoContentResponse();
            }
            catch (ZnodeException ex)
            {
                AnalyticsResponse data = new AnalyticsResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode };
                response = CreateInternalServerErrorResponse(data);
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Reports.ToString(), TraceLevel.Warning);
            }
            catch (Exception ex)
            {
                AnalyticsResponse data = new AnalyticsResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Reports.ToString(), TraceLevel.Error);
            }
            return response;
        }

        /// <summary>
        /// Update analytics details
        /// </summary>
        /// <param name="model">AnalyticsModel</param>
        /// <returns>true if the analytics data is updated</returns>
        [ResponseType(typeof(TrueFalseResponse))]
        [HttpPost]
        public virtual HttpResponseMessage UpdateAnalyticsDetails(AnalyticsModel model)
        {
            HttpResponseMessage response;

            try
            {
                bool updated = _service.UpdateAnalyticsDetails(model);
                response = CreateOKResponse(new TrueFalseResponse { IsSuccess = updated });
            }
            catch (ZnodeException ex)
            {
                TrueFalseResponse data = new TrueFalseResponse { IsSuccess = false, HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode };
                response = CreateInternalServerErrorResponse(data);
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Reports.ToString(), TraceLevel.Warning);
            }
            catch (Exception ex)
            {
                TrueFalseResponse data = new TrueFalseResponse { IsSuccess = false, HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Reports.ToString(), TraceLevel.Error);
            }
            return response;
        }

        #endregion
    }
}
