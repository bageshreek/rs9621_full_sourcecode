﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Helper;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Libraries.Framework.Business;

namespace Znode.Engine.Api.Controllers
{
     public  class ContentWidgetController : BaseController
    {

        #region Private Variables
        private readonly IContentWidgetCache _cache;
        private readonly IContentWidgetService _service;
        #endregion

        #region Constructor
        public ContentWidgetController(IContentWidgetService service)
        {
            _service = service;
            _cache = new ContentWidgetCache(_service);
        }
        #endregion


        /// <summary>
        /// List of Content Widgets
        /// </summary>
        /// <returns>ContentWidgetListResponseModel</returns>
        [ResponseType(typeof(ContentWidgetListResponseModel))]
        [HttpGet]
        public virtual HttpResponseMessage List()
        {
            HttpResponseMessage response;
            try
            {
                string data = _cache.List(RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse<ContentWidgetListResponseModel>(data) : CreateNoContentResponse();
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
                ContentWidgetListResponseModel data = new ContentWidgetListResponseModel { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Create List of Content Widgets
        /// </summary>
        /// <param name="model">ContentWidgetCreateModel model</param>
        /// <returns>ContentWidgetResponseModel model</returns>
        [ResponseType(typeof(ContentWidgetResponse))]
        [HttpPost, ValidateModel]
        public virtual HttpResponseMessage Create([FromBody] ContentWidgetCreateModel model)
        {
            HttpResponseMessage response;
            try
            {
                ContentWidgetResponseModel widgetModel = _service.Create(model);
                response = !Equals(widgetModel, null) ? CreateCreatedResponse(new ContentWidgetResponse { ContentWidgetModel = widgetModel }) : CreateInternalServerErrorResponse();
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Warning);
                ContentWidgetResponse data = new ContentWidgetResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
                ContentWidgetResponse data = new ContentWidgetResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }
        /// <summary>
        /// Update Content Widgets
        /// </summary>
        /// <param name="model">ContentWidgetUpdateModel model</param>
        /// <returns>ContentWidgetResponseModel model</returns>
        [HttpPut]
        [ResponseType(typeof(ContentWidgetResponse))]
        public virtual HttpResponseMessage Update([FromBody] ContentWidgetUpdateModel model)
        {
            HttpResponseMessage response;
            try
            {
                ContentWidgetResponseModel widgetModel = _service.Update(model);
                response = !Equals(widgetModel, null) ? CreateCreatedResponse(new ContentWidgetResponse { ContentWidgetModel = widgetModel, ErrorCode = 0 }) : CreateInternalServerErrorResponse();
            }

            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Warning);
                response = CreateInternalServerErrorResponse(new ContentWidgetResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
                response = CreateInternalServerErrorResponse(new ContentWidgetResponse { HasError = true, ErrorMessage = ex.Message });
            }
            return response;
        }

        /// <summary>
        /// Get Content Widget
        /// </summary>
        /// <param name="widgetKey">widgetKey</param>
        /// <returns>ContentWidgetResponseModel model</returns>
        [HttpGet]
        [ResponseType(typeof(ContentWidgetResponse))]
        public virtual HttpResponseMessage GetContentWidget(string widgetKey)
        {
            HttpResponseMessage response;

            try
            {
                string data = _cache.GetContentWidget(widgetKey, RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse<ContentWidgetResponse>(data) : CreateNoContentResponse();
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Warning);
                response = CreateInternalServerErrorResponse(new ContentWidgetResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
                response = CreateInternalServerErrorResponse(new ContentWidgetResponse { HasError = true, ErrorMessage = ex.Message });
            }
            return response;
        }


        /// <summary>
        /// Get Associated Variants
        /// </summary>
        /// <param name="widgetKey">widgetKey</param>
        /// <returns>List Of Variants</returns>
        [HttpGet]
        [ResponseType(typeof(ContentWidgetListResponseModel))]
        public virtual HttpResponseMessage GetAssociatedVariants(string widgetKey)
        {
            HttpResponseMessage response;

            try
            {
                string variants = _cache.GetAssociatedVariants(widgetKey, RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(variants) ? CreateOKResponse<ContentWidgetListResponseModel>(variants) : CreateNoContentResponse();
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Warning);
                response = CreateInternalServerErrorResponse(new ContentWidgetListResponseModel { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
                response = CreateInternalServerErrorResponse(new ContentWidgetListResponseModel { HasError = true, ErrorMessage = ex.Message });
            }
            return response;
        }

        /// <summary>
        /// Associate Variants
        /// </summary>
        /// <param name="model">AssociatedVariantModel model</param>
        /// <returns>List of AssociatedVariantModel</returns>
        [ResponseType(typeof(ContentWidgetListResponseModel))]
        [HttpPost]
        public virtual HttpResponseMessage AssociateVariant(AssociatedVariantModel model)
        {
            HttpResponseMessage response;
            try
            {
                List<AssociatedVariantModel> associatedVariants  = _service.AssociateVariant(model);
                response = !Equals(associatedVariants, null) ? CreateCreatedResponse(new ContentWidgetListResponseModel { AssociatedVariants = associatedVariants, ErrorCode = 0 }) : CreateInternalServerErrorResponse();

            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Warning);
                response = CreateInternalServerErrorResponse(new ContentWidgetListResponseModel { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
                response = CreateInternalServerErrorResponse(new ContentWidgetListResponseModel { HasError = true, ErrorMessage = ex.Message });
            }
            return response;
        }

        /// <summary>
        /// Delete Content Widgets
        /// </summary>
        /// <param name="ContentWidgetIds"></param>
        /// <returns>Status</returns>
        [ResponseType(typeof(TrueFalseResponse))]
        [HttpPost, ValidateModel]
        public virtual HttpResponseMessage DeleteContentWidget(ParameterModel ContentWidgetIds)
        {
            HttpResponseMessage response;
            try
            {
                bool Isdeleted = _service.DeleteContentWidget(ContentWidgetIds);
                response = CreateOKResponse(new TrueFalseResponse { IsSuccess = Isdeleted });
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Warning);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Delete Content Widget By Widget Key
        /// </summary>
        /// <param name="widgetKey">widgetKey</param>
        /// <returns></returns>
        [ResponseType(typeof(TrueFalseResponse))]
        [HttpPost, ValidateModel]
        public virtual HttpResponseMessage DeleteContentWidgetByWidgetKey(string widgetKey)
        {
            HttpResponseMessage response;
            try
            {
                bool Isdeleted = _service.DeleteContentWidgetByWidgetKey(widgetKey);
                response = CreateOKResponse(new TrueFalseResponse { IsSuccess = Isdeleted });
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Warning);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Delete Associated Variants
        /// </summary>
        /// <param name="variantId">variantId</param>
        /// <returns>Status</returns>
        [ResponseType(typeof(TrueFalseResponse))]
        [HttpPost, ValidateModel]
        public virtual HttpResponseMessage DeleteAssociatedVariant(int variantId)
        {
            HttpResponseMessage response;
            try
            {
                bool Isdeleted = _service.DeleteAssociatedVariant(variantId);
                response = CreateOKResponse(new TrueFalseResponse { IsSuccess = Isdeleted });
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Warning);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Verify if the Widget Key Exist
        /// </summary>
        /// <param name="widgetKey"></param>
        /// <returns>status</returns>
        [ResponseType(typeof(TrueFalseResponse))]
        [HttpGet]
        public virtual HttpResponseMessage IsWidgetKeyExists(string widgetKey)
        {
            HttpResponseMessage response;
            try
            {
                response = CreateOKResponse(new TrueFalseResponse { IsSuccess = _service.IsWidgetKeyExists(widgetKey) });
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Warning);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
            }
            return response;
        }

        /// <summary>
        /// Associate Widget Template
        /// </summary>
        /// <param name="variantId">variantId</param>
        /// <param name="widgetTemplateId">widgetTemplateId</param>
        /// <returns></returns>
        [ResponseType(typeof(TrueFalseResponse))]
        [HttpPut]
        public virtual HttpResponseMessage AssociateWidgetTemplate(int variantId, int widgetTemplateId)
        {
            HttpResponseMessage response;
            try
            {
                response = CreateOKResponse(new TrueFalseResponse { IsSuccess = _service.AssociateWidgetTemplate(variantId, widgetTemplateId) });
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Warning);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
            }
            return response;
        }

    }

}

