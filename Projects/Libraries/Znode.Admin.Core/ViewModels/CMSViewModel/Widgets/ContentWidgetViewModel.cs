﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Znode.Libraries.Resources;

namespace Znode.Engine.Admin.ViewModels
{
    public class ContentWidgetViewModel : BaseViewModel
    {
        
        [Required(ErrorMessageResourceType = typeof(Admin_Resources), ErrorMessageResourceName = ZnodeAdmin_Resources.WidgetKeyRequired)]
        [Display(Name = ZnodeAdmin_Resources.WidgetKey, ResourceType = typeof(Admin_Resources))]
        [RegularExpression(@"^[A-Za-z][a-zA-Z0-9]*$", ErrorMessageResourceName = ZnodeAdmin_Resources.ErrorAlphanumericOnly, ErrorMessageResourceType = typeof(Admin_Resources))]
        [MaxLength(100, ErrorMessageResourceName = ZnodeAdmin_Resources.WidgetKeyLimit, ErrorMessageResourceType = typeof(Admin_Resources))]
        public string WidgetKey { get; set; }

        [Display(Name = ZnodeAdmin_Resources.IsGlobalContentWidget, ResourceType = typeof(Admin_Resources))]
        public bool IsGlobalContentWidget { get; set; }

        [MaxLength(1000)]
        [Display(Name = ZnodeAdmin_Resources.LabelTags, ResourceType = typeof(Admin_Resources))]
        public string Tags { get; set; }


        [Required(ErrorMessageResourceType = typeof(Admin_Resources), ErrorMessageResourceName = ZnodeAdmin_Resources.RequiredWidgetFamily)]
        [Display(Name = ZnodeAdmin_Resources.WidgetFamily, ResourceType = typeof(Admin_Resources))]
        public string FamilyCode { get; set; }
        
        [Required(ErrorMessageResourceType = typeof(Admin_Resources), ErrorMessageResourceName = ZnodeAdmin_Resources.WidgetNameRequired)]
        [Display(Name = ZnodeAdmin_Resources.WidgetName, ResourceType = typeof(Admin_Resources))]
        [MaxLength(100, ErrorMessageResourceName = ZnodeAdmin_Resources.WidgetNameLimit, ErrorMessageResourceType = typeof(Admin_Resources))]
        public string Name { get; set; }

        [Display(Name = ZnodeAdmin_Resources.WidgetTemplate, ResourceType = typeof(Admin_Resources))]   
        public string TemplateName { get; set; }
        public int WidgetTemplateId { get; set; }

        public string StoreName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Admin_Resources), ErrorMessageResourceName = ZnodeAdmin_Resources.PortalIdRequired)]
        [Display(Name = ZnodeAdmin_Resources.LabelSelectStore, ResourceType = typeof(Admin_Resources))]
        public int PortalId { get; set; }
        public int ContentWidgetId { get; set; }
        public int WidgetProfileVariantId { get; set; }
        public List<WidgetVariantViewModel> WidgetVariants { get; set; }

        public string FamilyName { get; set; }
        public List<SelectListItem> WidgetFamilies { get; set; }
        public List<SelectListItem> Variants { get; set; }
        public List<SelectListItem> WidgetTemplates { get; set; }

        public string StoreCode { get; set; }


    }
}
