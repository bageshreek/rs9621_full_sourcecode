﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Admin.ViewModels;
using Znode.Engine.Api.Client.Sorts;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Admin.Agents
{
    public interface IContentWidgetAgent
    {
        /// <summary>
        /// List type of Content Widgets
        /// </summary>
        /// <param name="filters">filters</param>
        /// <param name="sortCollection">sortCollection</param>
        /// <param name="pageIndex">pageIndex</param>
        /// <param name="recordPerPage">recordPerPage</param>
        /// <returns>ContentWidgetListViewModel model</returns>
        ContentWidgetListViewModel List(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Bind the Content Widget Model
        /// </summary>
        /// <param name="model">ContentWidgetViewModel model</param>
        void BindContentWidgetModel(ContentWidgetViewModel model);

        /// <summary>
        /// Create Content Widget
        /// </summary>
        /// <param name="model">ContentWidgetViewModel model</param>
        /// <returns>ContentWidgetViewModel model</returns>
        ContentWidgetViewModel Create(ContentWidgetViewModel model);

        /// <summary>
        /// Edit Content Widget
        /// </summary>
        /// <param name="WidgetKey">widgetKey</param>
        /// <returns>ContentWidgetViewModel model</returns>
        ContentWidgetViewModel Edit(string widgetKey);

        /// <summary>
        /// Update Content Widget
        /// </summary>
        /// <param name="model">ContentWidgetViewModel model</param>
        /// <returns>ContentWidgetViewModel model</returns>
        ContentWidgetViewModel Update(ContentWidgetViewModel model);

        /// <summary>
        /// Get Locale List
        /// </summary>
        /// <returns>List of Available Locales</returns>
        List<SelectListItem> GetAvailableLocales();

        /// <summary>
        /// Get Unassociated Profiles of a Content Widget
        /// </summary>
        /// <param name="WidgetKey">widgetKey</param>
        /// <param name="localeId">localeId</param>
        /// <returns>List of Unassociated Profiles</returns>
        List<SelectListItem> GetUnassociatedProfiles(string widgetKey, int localeId);

        /// <summary>
        /// Associate Variant to a Content Widget
        /// </summary>
        /// <param name="model">WidgetVariantViewModel model</param>
        /// <returns>List of Associated Variants</returns>
        List<WidgetVariantViewModel> AssociateVariant(WidgetVariantViewModel model);

        /// <summary>
        /// Delete Associated Content Widget Variant
        /// </summary>
        /// <param name="variantId">variantId</param>
        /// <param name="errorMessage">errorMessage</param>
        /// <returns>Status of Deletion</returns>
        bool DeleteAssociatedVariant(int variantId, string widgetKey, out string errorMessage);

        /// <summary>
        /// Delete Content Widget
        /// </summary>
        /// <param name="contentWidgetIds">contentWidgetIds</param>
        /// <param name="errorMessage">errorMessage</param>
        /// <returns>Status of Deletion</returns>
        bool DeleteContentWidget(string contentWidgetIds, out string errorMessage);

        /// <summary>
        /// Verify if the Content Widget Key Exist
        /// </summary>
        /// <param name="widgetKey">widgetKey</param>
        /// <returns>Status of presence</returns>
        bool IsWidgetExist(string widgetKey);

        /// <summary>
        /// Associate Widget Template
        /// </summary>
        /// <param name="variantId"></param>
        /// <param name="widgetTemplateId"></param>
        bool AssociateWidgetTemplate(int variantId, int widgetTemplateId);

    }
}
