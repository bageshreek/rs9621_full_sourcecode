﻿using System;
using System.Diagnostics;
using Znode.Engine.Admin.Helpers;
using Znode.Engine.Admin.ViewModels;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Engine.Admin.Agents
{
    public class AnalyticsAgent : BaseAgent, IAnalyticsAgent
    {
        #region Private Variables
        private readonly IAnalyticsClient _analyticsClient;
        #endregion

        #region Constructor
        public AnalyticsAgent(IAnalyticsClient analyticsClient)
        {
            _analyticsClient = GetClient<IAnalyticsClient>(analyticsClient);
        }
        #endregion

        #region Public Methods
        // Method to get analytics dashboard data
        public virtual AnalyticsViewModel GetAnalyticsDashboardData()
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.Reports.ToString(), TraceLevel.Info);

            AnalyticsViewModel analyticsDashboardDetails = new AnalyticsViewModel();
            try
            {
                analyticsDashboardDetails.AnalyticsAccessToken = _analyticsClient.GetAnalyticsDashboardData()?.AnalyticsAccessToken;
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.Reports.ToString(), TraceLevel.Warning);
                switch (ex.ErrorCode)
                {
                    case ErrorCodes.InvalidData:
                        analyticsDashboardDetails = (AnalyticsViewModel)GetViewModelWithErrorMessage(analyticsDashboardDetails, ex.Message);
                        break;
                    default:
                        analyticsDashboardDetails = (AnalyticsViewModel)GetViewModelWithErrorMessage(analyticsDashboardDetails, Admin_Resources.Error);
                        break;
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Reports.ToString(), TraceLevel.Error);
                analyticsDashboardDetails = (AnalyticsViewModel)GetViewModelWithErrorMessage(analyticsDashboardDetails, Admin_Resources.Error);
            }
            return analyticsDashboardDetails;
        }

        #endregion
    }
}
