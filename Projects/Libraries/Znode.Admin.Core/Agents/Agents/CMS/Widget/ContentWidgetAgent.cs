﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Admin.Extensions;
using Znode.Engine.Admin.Helpers;
using Znode.Engine.Admin.Models;
using Znode.Engine.Admin.ViewModels;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;

namespace Znode.Engine.Admin.Agents
{
    public class ContentWidgetAgent : BaseAgent, IContentWidgetAgent
    {

        #region Private Variables
        protected readonly IContentWidgetClient _contentWidgetClient;
        protected readonly IPortalClient _portalClient;
        protected readonly IProfileClient _profileClient;
        protected readonly IGlobalAttributeFamilyClient _globalAttributesFamilyClient;
        protected readonly IWidgetTemplateClient _widgetTemplateClient;

        #endregion

        #region Constructor
        public ContentWidgetAgent(IContentWidgetClient contentWidgetClient, IPortalClient portalClient, IGlobalAttributeFamilyClient globalAttributeFamilyClient, IProfileClient profileClient, IWidgetTemplateClient widgetTemplate)
        {
            _contentWidgetClient = GetClient<IContentWidgetClient>(contentWidgetClient);
            _portalClient = GetClient<IPortalClient>(portalClient);
            _globalAttributesFamilyClient = GetClient<IGlobalAttributeFamilyClient>(globalAttributeFamilyClient);
            _profileClient = GetClient<IProfileClient>(profileClient);
            _widgetTemplateClient = GetClient<IWidgetTemplateClient>(widgetTemplate);
        }
        #endregion


        #region Public Methods

        //Get the List of Content Widgets
        public virtual ContentWidgetListViewModel List(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            ContentWidgetListModel listModel =  _contentWidgetClient.List(null, filters, sortCollection, pageIndex, recordPerPage);
            ContentWidgetListViewModel viewModel = new ContentWidgetListViewModel { ContentWidgets = listModel?.WidgetList?.ToViewModel<ContentWidgetViewModel>().ToList() };

            //Set Paging Data 
            SetListPagingData(viewModel, listModel);

            //Bind the tools dropdown
            SetToolMenu(viewModel);

            if (listModel?.WidgetList?.Count > 0)
                return viewModel;
            else
                return new ContentWidgetListViewModel() { ContentWidgets = new List<ContentWidgetViewModel>() };
        }
       
        //Create Content Widget
        public virtual ContentWidgetViewModel Create(ContentWidgetViewModel model)
        {
            model.StoreCode = model.PortalId == 0 ? string.Empty : _portalClient.GetPortalList(null, GetPortalFilter(model), null, null, null)?.PortalList.FirstOrDefault(x => x.PortalId == model.PortalId).StoreCode;
            model.IsGlobalContentWidget = model.PortalId == 0;
            model = _contentWidgetClient.Create(model.ToModel<ContentWidgetCreateModel>()).ToViewModel<ContentWidgetViewModel>();
            return model;
        }

        //Edit Content Widget
        public virtual ContentWidgetViewModel Edit(string widgetKey)
        {
            ContentWidgetResponseModel widgetResponseData = _contentWidgetClient.GetContentWidget(widgetKey);
            ContentWidgetViewModel widget = HelperUtility.IsNotNull(widgetResponseData) ? widgetResponseData.ToViewModel<ContentWidgetViewModel>() : new ContentWidgetViewModel();
            widget.WidgetTemplates = GetWidgetTemplates();            
            SaveInSession(AdminConstants.WidgetVariantSessionKey + widgetKey, widget.WidgetVariants);
            widget.Variants = GetVariantDropdown(widget.WidgetVariants);
            return widget;
        }

        //Update Content Widget
        public virtual ContentWidgetViewModel Update(ContentWidgetViewModel model)
            => _contentWidgetClient.Update(model.ToModel<ContentWidgetUpdateModel>()).ToViewModel<ContentWidgetViewModel>();

        //Associate Content Widget Variant
        public virtual List<WidgetVariantViewModel> AssociateVariant(WidgetVariantViewModel model)
        {
            List<WidgetVariantViewModel> widgetVariants = _contentWidgetClient.AssociateVariant(model.ToModel<AssociatedVariantModel>()).ToViewModel<WidgetVariantViewModel>().ToList();
            SaveInSession(AdminConstants.WidgetVariantSessionKey + model.WidgetKey, widgetVariants);
            return widgetVariants;

        }

        //Get Unassociated Content Widget Profile Variant
        public virtual List<SelectListItem> GetUnassociatedProfiles(string widgetKey, int localeId)
        {
            bool isAllVariants = false;
            ProfileListModel profileList = _profileClient.GetProfileList(null, null, null, null);

            List<WidgetVariantViewModel> variants = GetFromSession<List<WidgetVariantViewModel>>(AdminConstants.WidgetVariantSessionKey + widgetKey);
            if (HelperUtility.IsNull(variants) || variants?.Count < 1)
                variants = GetVariants(widgetKey);

            //Get the Default Locale Id
            int defaultLocaleId = Convert.ToInt32(DefaultSettingHelper.DefaultLocale);

            //List of associated profiles base on selected Locale 
            List<int?> profileIds = variants.Where(x => x.LocaleId == localeId).Select(x => x.ProfileId).ToList();

            //Conditions based on Locales selection
            if (localeId == defaultLocaleId)
            {
                if (profileIds.Count > 0)
                    profileList.Profiles = profileList.Profiles.Where(x => !profileIds.Contains(x.ProfileId) && profileIds.Any(zx => zx != null)).ToList();
                else
                    isAllVariants = true;
            }
            else
            {
                List<int?> defaultProfileIds = variants.Where(x => x.LocaleId == defaultLocaleId).Select(x => x.ProfileId).ToList();
                if(((defaultProfileIds.Count == 0 || defaultProfileIds.Any(zx => zx != null) || (profileIds.Count > 0)) && ( defaultProfileIds.Count < profileList.Profiles.Count || profileIds.Count > 0)))
                        profileList.Profiles = profileList.Profiles.Where(x => !profileIds.Contains(x.ProfileId) && (defaultProfileIds.Contains(x.ProfileId) || defaultProfileIds.Any(zx => zx == null)) && (profileIds.Any(zx => zx != null)||profileIds.Count == 0)).ToList();
                else
                    isAllVariants = true;
            }
            //return the List of profiles based on the selected Locale
            return GetProfileVariantDropdown(profileList, isAllVariants);
        }

        //Delete variant associated to Content Widget
        public virtual bool DeleteAssociatedVariant(int variantId, string widgetKey, out string errorMessage)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            errorMessage = string.Empty;
            try
            {
                RemoveInSession(AdminConstants.WidgetVariantSessionKey + widgetKey);
                return _contentWidgetClient.DeleteAssociatedVariant(variantId);
            }
            catch (ZnodeException ex)
            {
               ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
               errorMessage = Admin_Resources.ErrorDeleteDefaultVariant;
               return false;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                errorMessage = Admin_Resources.ErrorFailedToDelete;
                return false;
            }
        }

        //Delete Content Widget
        public virtual bool DeleteContentWidget(string contentWidgetIds, out string errorMessage)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            errorMessage = string.Empty;
            try
            {
                return _contentWidgetClient.DeleteContentWidget(new ParameterModel { Ids = contentWidgetIds });
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
                errorMessage = Admin_Resources.DeleteContentWidget;
                return false;
                
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                errorMessage = Admin_Resources.ErrorFailedToDelete;
                return false;
            }
        }


        public virtual bool AssociateWidgetTemplate(int variantId, int widgetTemplateId)
           => _contentWidgetClient.AssociateWidgetTemplate(variantId, widgetTemplateId);


        //Verify if the Widget Key Exist
        public virtual bool IsWidgetExist(string widgetKey)
            => !string.IsNullOrEmpty(widgetKey) ? _contentWidgetClient.IsWidgetKeyExist(widgetKey) : true;

        //Bind widget model with the available Widget Families
        public void BindContentWidgetModel(ContentWidgetViewModel model)
        {
            model.WidgetFamilies = GetWidgetFamilies();
            model.WidgetTemplates = GetWidgetTemplates();
        }

        //Get Available Locale
        public virtual List<SelectListItem> GetAvailableLocales()
         =>  new LocaleAgent(GetClient<LocaleClient>()).GetLocalesList();

        //Get Profile Variant Dropdown
        public virtual List<SelectListItem> GetProfileVariantDropdown(ProfileListModel profileVariants, bool isAllVariants = false)
        {
            List<SelectListItem> profiles = new List<SelectListItem>();
            if (profileVariants.Profiles?.Count > 0)
            {
                if (isAllVariants)
                    profiles.Add(new SelectListItem { Text = Admin_Resources.LabelAllProfiles, Value = "0" });
                profileVariants.Profiles.ForEach(x => { profiles.Add(new SelectListItem { Text = x.ProfileName, Value = x.ProfileId.ToString() }); });
            }
            return profiles;
        }


        //Get Associated variants
        public virtual List<WidgetVariantViewModel> GetVariants(string widgetKey)
        {
            List<WidgetVariantViewModel> variants = _contentWidgetClient.GetAssociatedVariants(widgetKey).ToViewModel<WidgetVariantViewModel>().ToList();

            SaveInSession(AdminConstants.WidgetVariantSessionKey + widgetKey, variants);
            return variants;
        }

        #endregion


        #region Private Methods

        //Bind Tools Dropdown
        protected virtual void SetToolMenu(ContentWidgetListViewModel model)
        {
            if (HelperUtility.IsNotNull(model))
            {
                model.GridModel = new GridModel();
                model.GridModel.FilterColumn = new FilterColumnListModel();
                model.GridModel.FilterColumn.ToolMenuList = new List<ToolMenuModel>();
                model.GridModel.FilterColumn.ToolMenuList.Add(new ToolMenuModel { DisplayText = Admin_Resources.ButtonDelete, JSFunctionName = "EditableText.prototype.DialogDelete('ContentWidgetDeletePopup')", ControllerName = "ContentWidget", ActionName = "Delete" });
            }
        }

        //Get Variant Dropdown
        protected virtual List<SelectListItem> GetVariantDropdown(List<WidgetVariantViewModel> widgetVariants)
        {
            List<SelectListItem> variants = new List<SelectListItem>();
            if (widgetVariants?.Count > 0)
                widgetVariants.ForEach(x => { variants.Add(new SelectListItem { Text = $"Variant For {x.ProfileName} And {x.LocaleName}", Value = x.WidgetProfileVariantId.ToString() }); });
            return variants;
        }

        //Get Widget Families
        protected virtual List<SelectListItem> GetWidgetFamilies()
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(ZnodeGlobalEntityEnum.GlobalEntityId.ToString(), FilterOperators.Equals, ((int)EntityType.Widget).ToString());
            List<GlobalAttributeFamilyModel> familyList = _globalAttributesFamilyClient.GetAttributeFamilyList(null, filters, null, null, null).AttributeFamilyList;
            List<SelectListItem> widgetFamilies = new List<SelectListItem>();

            familyList?.ForEach(item => { widgetFamilies.Add(new SelectListItem() { Text = item.AttributeFamilyName, Value = item.FamilyCode }); });

            return widgetFamilies;
        }

        protected virtual List<SelectListItem> GetWidgetTemplates()
        {
            List<WidgetTemplateModel> widgetTemplateList = _widgetTemplateClient.List(null, null, null, null, null).WidgetTemplates;
            List<SelectListItem> widgetTemplate = new List<SelectListItem>();

            widgetTemplateList?.ForEach(item => { widgetTemplate.Add(new SelectListItem() { Text = $"{item.Name} | {item.Code}", Value = item.WidgetTemplateId.ToString() }); });

            widgetTemplate.Insert(0, new SelectListItem { Text = "--Select Template--", Value = "" });
            return widgetTemplate;
        }

        protected virtual FilterCollection GetPortalFilter(ContentWidgetViewModel model)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(FilterKeys.PortalId, FilterOperators.Equals, model.PortalId.ToString());
            return filters;
        }
        #endregion
    }
}
