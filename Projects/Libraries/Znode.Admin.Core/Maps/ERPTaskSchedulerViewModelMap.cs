﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Admin.ViewModels;
namespace Znode.Engine.Admin.Maps
{
    public class ERPTaskSchedulerViewModelMap
    {
        #region Public Methods
        // Bind List of Repeat Task For Duration to dropdown.
        public static List<SelectListItem> GetRepeatTaskForDurations()
        {
            List<SelectListItem> repeatTaskForDurationsList = new List<SelectListItem>();
            repeatTaskForDurationsList = (from item in GetRepeatTaskForDurationDictionary()
                                          select new SelectListItem
                                          {
                                              Text = item.Value,
                                              Value = item.Key,
                                              Selected = Equals(item.Key, 0),
                                          }).ToList();

            return repeatTaskForDurationsList;
        }

        // Bind List of ERP Task Scheduler Days to dropdown.
        public static List<SelectListItem> GetERPTaskSchedulerDays()
        {
            List<SelectListItem> repeatERPTaskSchedulerDaysList = new List<SelectListItem>();
            repeatERPTaskSchedulerDaysList = (from item in GetRepeatERPTaskSchedulerDayDictionary()
                                              select new SelectListItem
                                              {
                                                  Text = item.Value,
                                                  Value = item.Key,
                                                  Selected = Equals(item.Key, 0),
                                              }).ToList();

            return repeatERPTaskSchedulerDaysList;
        }

        // Bind List of ERP Task Scheduler WeekDays to dropdown.
        public static List<SelectListItem> GetERPTaskSchedulerWeekDays()
        {
            List<SelectListItem> repeatERPTaskSchedulerWeekDaysList = new List<SelectListItem>();
            repeatERPTaskSchedulerWeekDaysList = (from item in GetRepeatERPTaskSchedulerWeekDaysDictionary()
                                                  select new SelectListItem
                                                  {
                                                      Text = item.Value,
                                                      Value = item.Key,
                                                      Selected = Equals(item.Key, 0),
                                                  }).ToList();

            return repeatERPTaskSchedulerWeekDaysList;
        }

        // Bind List of ERP Task Scheduler Months to dropdown.
        public static List<SelectListItem> GetERPTaskSchedulerMonths()
        {
            List<SelectListItem> repeatERPTaskSchedulerMonthsList = new List<SelectListItem>();
            repeatERPTaskSchedulerMonthsList = (from item in GetRepeatERPTaskSchedulerMonthsDictionary()
                                                select new SelectListItem
                                                {
                                                    Text = item.Value,
                                                    Value = item.Key,
                                                    Selected = Equals(item.Key, 0),
                                                }).ToList();

            return repeatERPTaskSchedulerMonthsList;
        }

        // Bind List of ERP Task Scheduler OnDays to dropdown.
        public static List<SelectListItem> GetERPTaskSchedulerOnDays()
        {
            List<SelectListItem> repeatERPTaskSchedulerOnDaysList = new List<SelectListItem>();
            repeatERPTaskSchedulerOnDaysList = (from item in GetRepeatERPTaskSchedulerOnDaysDictionary()
                                                select new SelectListItem
                                                {
                                                    Text = item.Value,
                                                    Value = item.Key,
                                                    Selected = Equals(item.Key, 0),
                                                }).ToList();

            return repeatERPTaskSchedulerOnDaysList;
        }

        // Bind List of Repeat Task Every to dropdown.
        public static List<SelectListItem> GetRepeatTaskEvery()
        {
            List<SelectListItem> repeatTaskTaskEveryList = new List<SelectListItem>();
            repeatTaskTaskEveryList = (from item in GetRepeatTaskEveryDictionary()
                                          select new SelectListItem
                                          {
                                              Text = item.Value,
                                              Value = item.Key,
                                              Selected = Equals(item.Key, 0),
                                          }).ToList();

            return repeatTaskTaskEveryList;
        }
        #endregion

        #region Private Methods
        //Dropdown for Repeat Task For Duration in minute. 
        private static Dictionary<string, string> GetRepeatTaskForDurationDictionary()
        {
            Dictionary<string, string> RepeatTaskForDuration = new Dictionary<string, string>();
            RepeatTaskForDuration.Add("0m", "Indefinitely");
            RepeatTaskForDuration.Add("15m", "15 minutes");
            RepeatTaskForDuration.Add("30m", "30 minutes");
            RepeatTaskForDuration.Add("60m", "1 hour");
            RepeatTaskForDuration.Add("720m", "12 hours");
            RepeatTaskForDuration.Add("1440m", "1 day");
            return RepeatTaskForDuration;
        }

        //Dropdown for Repeat Task For Days.
        private static Dictionary<string, string> GetRepeatERPTaskSchedulerDayDictionary()
        {
            Dictionary<string, string> RepeatERPTaskSchedulerDay = new Dictionary<string, string>();
            for (int index = 1; index <= 31; index++)
            {
                RepeatERPTaskSchedulerDay.Add(Convert.ToString(index), Convert.ToString(index));
            }
            return RepeatERPTaskSchedulerDay;
        }
        //Dropdown for Repeat Task For WeekDays.
        private static Dictionary<string, string> GetRepeatERPTaskSchedulerWeekDaysDictionary()
        {
            Dictionary<string, string> RepeatERPTaskSchedulerWeekDays = new Dictionary<string, string>();
            foreach (ERPWeekDaysEnum erpWeekDaysEnum in Enum.GetValues(typeof(ERPWeekDaysEnum)))
            {
                RepeatERPTaskSchedulerWeekDays.Add(erpWeekDaysEnum.ToString(), erpWeekDaysEnum.ToString());
            }
            return RepeatERPTaskSchedulerWeekDays;
        }

        //Dropdown for Repeat Task For Months.
        private static Dictionary<string, string> GetRepeatERPTaskSchedulerMonthsDictionary()
        {
            Dictionary<string, string> RepeatERPTaskSchedulerMonth = new Dictionary<string, string>();
            foreach (ERPMonthsEnum erpMonthsEnum in Enum.GetValues(typeof(ERPMonthsEnum)))
            {
                RepeatERPTaskSchedulerMonth.Add(erpMonthsEnum.ToString(), erpMonthsEnum.ToString());
            }
            return RepeatERPTaskSchedulerMonth;
        }

        //Dropdown for Repeat Task For OnDays.
        private static Dictionary<string, string> GetRepeatERPTaskSchedulerOnDaysDictionary()
        {
            Dictionary<string, string> RepeatERPTaskSchedulerOnDay = new Dictionary<string, string>();
            foreach (ERPOnDaysEnum erpOnDaysEnum in Enum.GetValues(typeof(ERPOnDaysEnum)))
            {
                RepeatERPTaskSchedulerOnDay.Add(erpOnDaysEnum.ToString(), erpOnDaysEnum.ToString());
            }
            return RepeatERPTaskSchedulerOnDay;
        }

        //Dropdown for Repeat Task For Duration in minute. 
        private static Dictionary<string, string> GetRepeatTaskEveryDictionary()
        {
            Dictionary<string, string> RepeatTaskEvery = new Dictionary<string, string>();
            RepeatTaskEvery.Add("5", "5 minutes");
            RepeatTaskEvery.Add("10", "10 minutes");
            RepeatTaskEvery.Add("15", "15 minutes");
            RepeatTaskEvery.Add("30", "30 minutes");
            RepeatTaskEvery.Add("60", "1 hour");
            return RepeatTaskEvery;
        }

        #endregion
    }

}