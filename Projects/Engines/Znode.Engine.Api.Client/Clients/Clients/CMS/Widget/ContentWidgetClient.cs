﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Api.Client
{
    public class ContentWidgetClient : BaseClient, IContentWidgetClient
    {
        // Gets the List of Content Widget
        public virtual ContentWidgetListModel List(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            //Get Endpoint.
            string endpoint = ContentWidgetEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            //Get response
            ApiStatus status = new ApiStatus();
            ContentWidgetListResponseModel response = GetResourceFromEndpoint<ContentWidgetListResponseModel>(endpoint, status);
            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };

            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            ContentWidgetListModel list = new ContentWidgetListModel { WidgetList = response?.WidgetList };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        //Create Content Widget
        public virtual ContentWidgetResponseModel Create(ContentWidgetCreateModel model)
        {
            //Get Endpoint.
            string endpoint = ContentWidgetEndpoint.Create();

            //Get Serialize object as a response.
            ApiStatus status = new ApiStatus();
            ContentWidgetResponse response = PostResourceToEndpoint<ContentWidgetResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return response?.ContentWidgetModel;
        }

        //Get Content Widget
        public virtual ContentWidgetResponseModel GetContentWidget(string widgetKey)
        {
            //Get Endpoint.
            string endpoint = ContentWidgetEndpoint.GetContentWidget(widgetKey);

            ApiStatus status = new ApiStatus();
            ContentWidgetResponse response = GetResourceFromEndpoint<ContentWidgetResponse>(endpoint, status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return response?.ContentWidgetModel;
        }

        //Update Content Widget
        public virtual ContentWidgetResponseModel Update(ContentWidgetUpdateModel model)
        {
            string endpoint = ContentWidgetEndpoint.Update();

            ApiStatus status = new ApiStatus();
            ContentWidgetResponse response = PutResourceToEndpoint<ContentWidgetResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return response?.ContentWidgetModel;
        }

        //Get List Of Associated Variants to a Content Widget
        public virtual List<AssociatedVariantModel> GetAssociatedVariants(string widgetKey)
        {
            string endpoint = ContentWidgetEndpoint.GetAssociatedVariants(widgetKey);

            ApiStatus status = new ApiStatus();
            ContentWidgetListResponseModel response = GetResourceFromEndpoint<ContentWidgetListResponseModel>(endpoint, status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);


            return response.AssociatedVariants;
        }

        //Associate Variant to a Content Widget
        public virtual List<AssociatedVariantModel> AssociateVariant(AssociatedVariantModel model)
        {
            string endpoint = ContentWidgetEndpoint.AssociateVariant();
            ApiStatus status = new ApiStatus();
            ContentWidgetListResponseModel response = PostResourceToEndpoint<ContentWidgetListResponseModel>(endpoint, JsonConvert.SerializeObject(model), status);


            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);
            return response.AssociatedVariants;
        }

        //Delete Associated Variant
        public virtual bool DeleteAssociatedVariant(int variantId)
        {
            //Get Endpoint.
            string endpoint = ContentWidgetEndpoint.DeleteAssociateVariant(variantId);

            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = PostResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(variantId), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return response.IsSuccess;
        }

        //Delete Content Widget
        public virtual bool DeleteContentWidget(ParameterModel contentWidgetIds)
        {
            //Get Endpoint.
            string endpoint = ContentWidgetEndpoint.DeleteContentWidget();

            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = PostResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(contentWidgetIds), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return response.IsSuccess;
        }

        //Associate Widget Template to widget
        public virtual bool AssociateWidgetTemplate(int variantId, int widgetTemplateId)
        {
            //Get Endpoint.
            string endpoint = ContentWidgetEndpoint.AssociateWidgetTemplate(variantId, widgetTemplateId);

            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = PutResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(variantId), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return response.IsSuccess;
        }
        //Verify if the Widget Key Exist
        public virtual bool IsWidgetKeyExist(string widgetKey)
        {
            //Get Endpoint.
            string endpoint = ContentWidgetEndpoint.IsWidgetKeyExist(widgetKey);

            //Get response.
            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = GetResourceFromEndpoint<TrueFalseResponse>(endpoint, status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return response.IsSuccess;
        }

    }
}
