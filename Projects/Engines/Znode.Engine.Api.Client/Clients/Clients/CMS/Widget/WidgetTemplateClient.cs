﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Api.Client
{
    public class WidgetTemplateClient : BaseClient, IWidgetTemplateClient
    {
        //Get the List of Widget Template
        public virtual WidgetTemplateListModel List(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            string endpoint = WidgetTemplateEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            ApiStatus status = new ApiStatus();
            WidgetTemplateListResponse response = GetResourceFromEndpoint<WidgetTemplateListResponse>(endpoint, status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            WidgetTemplateListModel list = new WidgetTemplateListModel { WidgetTemplates = response?.WidgetTemplates };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        //Crete Widget Template
        public virtual WidgetTemplateModel Create(WidgetTemplateCreateModel model)
        {
            string endpoint = WidgetTemplateEndpoint.Create();

            ApiStatus status = new ApiStatus();
            WidgetTemplateListResponse response = PostResourceToEndpoint<WidgetTemplateListResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return response?.widgetTemplate;
        }

        //Get Widget Template
        public virtual WidgetTemplateModel GetWidgetTemplate(string templateCode)
        {
            string endpoint = WidgetTemplateEndpoint.Get(templateCode);

            ApiStatus status = new ApiStatus();
            WidgetTemplateListResponse response = GetResourceFromEndpoint<WidgetTemplateListResponse>(endpoint, status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return response?.widgetTemplate;
        }

        //Update Widget Template
        public virtual WidgetTemplateModel Update(WidgetTemplateUpdateModel model)
        {
            string endpoint = WidgetTemplateEndpoint.Update();

            ApiStatus status = new ApiStatus();
            WidgetTemplateListResponse response = PutResourceToEndpoint<WidgetTemplateListResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return response?.widgetTemplate;
        }

        //Delete Widget Template
        public virtual bool Delete(ParameterModel widgetTemplateIds)
        {
            string endpoint = WidgetTemplateEndpoint.Delete();

            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = PostResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(widgetTemplateIds), status);
            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return response.IsSuccess;
        }

        //Validate if the Widget Template Exist
        public virtual bool IsWidgetTemplateExist(string templateCode)
        {
            string endpoint = WidgetTemplateEndpoint.IsWidgetTemplateExist(templateCode);

            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = GetResourceFromEndpoint<TrueFalseResponse>(endpoint, status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return response.IsSuccess;
        }
    }
}
