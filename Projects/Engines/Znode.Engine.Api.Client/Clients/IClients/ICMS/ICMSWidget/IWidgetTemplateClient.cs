﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Api.Client
{
    public interface IWidgetTemplateClient : IBaseClient
    {
        /// <summary>
        /// Get the List of Widget Template
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">pageIndex</param>
        /// <param name="pageSize">pageSize</param>
        /// <returns>WidgetTemplateListModel model</returns>
        WidgetTemplateListModel List(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Create Widget Template
        /// </summary>
        /// <param name="model">WidgetTemplateCreateModel model</param>
        /// <returns>WidgetTemplateModel model</returns>
        WidgetTemplateModel Create(WidgetTemplateCreateModel model);

        /// <summary>
        /// Get Widget Template
        /// </summary>
        /// <param name="templateCode">template code</param>
        /// <returns>WidgetTemplateModel model</returns>
        WidgetTemplateModel GetWidgetTemplate(string templateCode);

        /// <summary>
        /// Update Widget Template
        /// </summary>
        /// <param name="model">WidgetTemplateUpdateModel model</param>
        /// <returns>WidgetTemplateModel model</returns>
        WidgetTemplateModel Update(WidgetTemplateUpdateModel model);

        /// <summary>
        /// Delete Widget Template
        /// </summary>
        /// <param name="widgetTemplateIds">Widget Template Ids</param>
        /// <returns>status</returns>
        bool Delete(ParameterModel widgetTemplateIds);

        /// <summary>
        /// Validate if the Widget Template exists
        /// </summary>
        /// <param name="templateCode">Template code</param>
        /// <returns>status</returns>
        bool IsWidgetTemplateExist(string templateCode);


    }
}
