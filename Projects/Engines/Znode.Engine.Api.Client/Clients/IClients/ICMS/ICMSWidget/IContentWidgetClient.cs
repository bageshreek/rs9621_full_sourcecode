﻿using System.Collections.Generic;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Api.Client
{
    public interface IContentWidgetClient : IBaseClient
    {
        /// <summary>
        /// Get the List of Content Widget
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">pageIndex</param>
        /// <param name="pageSize">pageSize</param>
        /// <returns>ContentWidgetListModel model</returns>
        ContentWidgetListModel List(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Create Content Widget
        /// </summary>
        /// <param name="model">ContentWidgetResponseModel model</param>
        /// <returns>ContentWidgetResponseModel model</returns>
        ContentWidgetResponseModel Create(ContentWidgetCreateModel model);

        /// <summary>
        /// Get Content Widget
        /// </summary>
        /// <param name="widgetKey">widgetKey</param>
        /// <returns>ContentWidgetResponseModel model</returns>
        ContentWidgetResponseModel GetContentWidget(string widgetKey);

        /// <summary>
        /// Update Content Widget
        /// </summary>
        /// <param name="model">ContentWidgetUpdateModel model</param>
        /// <returns>ContentWidgetResponseModel model</returns>
        ContentWidgetResponseModel Update(ContentWidgetUpdateModel model);

        /// <summary>
        /// Get Associated Variants to a Content Widget
        /// </summary>
        /// <param name="widgetKey">widgetKey</param>
        /// <returns>List ofAssociatedVariantModel</returns>
        List<AssociatedVariantModel> GetAssociatedVariants(string widgetKey);

        /// <summary>
        /// Associate Variant to a Content Widget
        /// </summary>
        /// <param name="model">AssociatedVariantModel model</param>
        /// <returns>List of AssociatedVariantModel</returns>
        List<AssociatedVariantModel> AssociateVariant(AssociatedVariantModel model);

        /// <summary>
        /// Delete Associated Variant
        /// </summary>
        /// <param name="variantId">variantId</param>
        /// <returns>status of deletion</returns>
        bool DeleteAssociatedVariant(int variantId);

        /// <summary>
        /// Delete Content Widget
        /// </summary>
        /// <param name="contentWidgetIds">contentWidgetIds</param>
        /// <returns>status of deletion</returns>
        bool DeleteContentWidget(ParameterModel contentWidgetIds);

        /// <summary>
        /// Verify if the Widget Key Exist
        /// </summary>
        /// <param name="widgetKey">widgetKey</param>
        /// <returns>status of presence</returns>
        bool IsWidgetKeyExist(string widgetKey);

        /// <summary>
        /// Associate Widget Template
        /// </summary>
        /// <param name="variantId">variantId</param>
        /// <param name="widgetTemplateId">widgetTemplateId</param>
        /// <returns>status</returns>
        bool AssociateWidgetTemplate(int variantId, int widgetTemplateId);



    }
}
