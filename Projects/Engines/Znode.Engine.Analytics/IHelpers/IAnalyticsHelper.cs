﻿using Znode.Engine.Api.Models;

namespace Znode.Engine.Analytics
{
    public interface IAnalyticsHelper
    {
        /// <summary>
        /// To get the access token from the google API from the passed JSON string
        /// </summary>
        /// <param name="analyticsJSONkey">JSON string for the service account</param>
        /// <returns>AnalyticsModel containing the access token and expiry time</returns>
        AnalyticsModel GetAccessToken(string analyticsJSONKey);
    }
}
