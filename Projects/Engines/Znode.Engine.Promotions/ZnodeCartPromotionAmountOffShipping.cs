using System;
using System.Collections.Generic;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Promotions
{
    public class ZnodeCartPromotionAmountOffShipping : ZnodeCartPromotionType
    {
        #region Constructor
        public ZnodeCartPromotionAmountOffShipping()
        {
            Name = "Amount Off Shipping";
            Description = "Applies an amount off shipping for an order; affects the shopping cart.";
            AvailableForFranchise = false;

            Controls.Add(ZnodePromotionControl.Store);
            Controls.Add(ZnodePromotionControl.Profile);
            Controls.Add(ZnodePromotionControl.DiscountAmount);
            Controls.Add(ZnodePromotionControl.MinimumOrderAmount);
            Controls.Add(ZnodePromotionControl.Coupon);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Calculates the amount off shipping for the order.
        /// </summary>
        public override void Calculate(int? couponIndex, List<PromotionModel> allPromotions)
        {
            OrderBy = nameof(PromotionModel.OrderMinimum);
            ApplicablePromolist = ZnodePromotionManager.GetPromotionsByType(ZnodeConstant.PromotionClassTypeCart, ClassName, allPromotions, OrderBy);


            bool isCouponValid = false;
            if (!Equals(couponIndex, null))
            {
                isCouponValid = ValidateCoupon(couponIndex);
            }

            if (Equals(PromotionBag.Coupons, null))
            {
                if (!ZnodePromotionHelper.IsApplicablePromotion(ApplicablePromolist, PromotionBag.PromoCode, ShoppingCart.SubTotal, true, false))
                    return;

                ApplyDiscount(couponIndex);
            }
            else if (!Equals(PromotionBag.Coupons, null) && isCouponValid)
            {
                foreach (CouponModel coupon in PromotionBag.Coupons)
                {
                    if ((coupon.AvailableQuantity > 0 || IsExistingOrderCoupon(coupon.Code)) && CheckCouponCodeValid(ShoppingCart.Coupons[couponIndex.Value].Coupon, coupon.Code))
                    {
                        if (!ZnodePromotionHelper.IsApplicablePromotion(ApplicablePromolist, PromotionBag.PromoCode, ShoppingCart.SubTotal, true, true))
                            return;

                        ApplyDiscount(couponIndex, coupon.Code);
                        if (IsUniqueCouponApplied(PromotionBag, ShoppingCart.Coupons[couponIndex.Value].CouponApplied))
                        {
                            break;
                        }
                    }
                }
                AddPromotionMessage(couponIndex);
            }
        }

        #endregion

        #region Private Method
        //to Apply Discount
        private void ApplyDiscount(int? couponIndex, string couponCode = "")
        {
            decimal subTotal = GetCartSubTotal(ShoppingCart);
            if (PromotionBag.MinimumOrderAmount <= subTotal)
            {
                ShoppingCart.Shipping.ShippingDiscount += PromotionBag.Discount;
                ShoppingCart.IsAnyPromotionApplied = true;
            }
            else
            {
                ShoppingCart.IsAnyPromotionApplied = false;
            }

            if (!string.IsNullOrEmpty(couponCode) && ShoppingCart.IsAnyPromotionApplied)
            {
                SetCouponApplied(couponCode);
                ShoppingCart.Coupons[couponIndex.Value].CouponApplied = true;
                ShoppingCart.Shipping.ShippingDiscountDescription = couponCode;
                ShoppingCart.Shipping.ShippingDiscountType = (int)OrderDiscountTypeEnum.COUPONCODE;
            }
            else if (ShoppingCart.IsAnyPromotionApplied)
            {
                ShoppingCart.IsAnyPromotionApplied = true;
                ShoppingCart.Shipping.ShippingDiscountDescription = PromotionBag.PromoCode;
                ShoppingCart.Shipping.ShippingDiscountType = (int)OrderDiscountTypeEnum.PROMOCODE;
            }
        }
        #endregion
    }
}
