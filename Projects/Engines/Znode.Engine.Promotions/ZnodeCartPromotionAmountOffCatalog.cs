using System;
using System.Collections.Generic;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Entities;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Promotions
{
    public class ZnodeCartPromotionAmountOffCatalog : ZnodeCartPromotionType
    {
        #region Private Variable
        private readonly ZnodePromotionHelper promotionHelper = new ZnodePromotionHelper();
        #endregion

        #region Constructor
        public ZnodeCartPromotionAmountOffCatalog()
        {
            Name = "Amount Off Catalog";
            Description = "Applies an amount off products for a particular catalog; affects the shopping cart.";
            AvailableForFranchise = false;

            Controls.Add(ZnodePromotionControl.Store);
            Controls.Add(ZnodePromotionControl.Profile);
            Controls.Add(ZnodePromotionControl.DiscountAmount);
            Controls.Add(ZnodePromotionControl.RequiredCatalog);
            Controls.Add(ZnodePromotionControl.RequiredCatalogMinimumQuantity);
            Controls.Add(ZnodePromotionControl.MinimumOrderAmount);
            Controls.Add(ZnodePromotionControl.Coupon);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Calculates the amount off products for a particular catalog in the shopping cart.
        /// </summary>
        public override void Calculate(int? couponIndex, List<PromotionModel> allPromotions)
        {
            ApplicablePromolist = ZnodePromotionManager.GetPromotionsByType(ZnodeConstant.PromotionClassTypeCart, ClassName, allPromotions, OrderBy);
            bool isCouponValid = false;
            if (!Equals(couponIndex, null))
            {
                isCouponValid = ValidateCoupon(couponIndex);
            }

            //to get all catalog of promotion by PromotionId
            List<CatalogModel> promotionsCatalog = promotionHelper.GetPromotionCatalogs(PromotionBag.PromotionId);

            bool isPromotionApplied = false;
            // Loop through each cart Item
            foreach (ZnodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {
                 isPromotionApplied = false;
                // Get the catalogs for the category
                List<CatalogModel> productCatalogs = promotionHelper.GetCatalogByProduct(cartItem.Product.ProductID);

                foreach (CatalogModel promotion in promotionsCatalog)
                {
                   
                    foreach (CatalogModel product in productCatalogs)
                    {
                        if (promotion.PimCatalogId == product.PimCatalogId)
                        {
                            ApplyDiscount(out isPromotionApplied, isCouponValid, couponIndex, cartItem);
                            // Break out of the catalogs loop
                            break;
                        }
                    }
                    if (isPromotionApplied)
                        // Break out of the category loop
                        break;
                }
            }
            AddPromotionMessage(couponIndex);
        }
        #endregion

        #region Private Method

        //to apply discount
        private void ApplyDiscount(out bool isPromotionApplied, bool isCouponValid, int? couponIndex, ZnodeShoppingCartItem cartItem)
        {
            decimal subTotal = GetCartSubTotal(ShoppingCart);
            decimal cartCount = GetCartQuantity();
            decimal promotionBagDiscount = PromotionBag.Discount;
            bool discountApplied = false;
            if (Equals(PromotionBag.Coupons, null))
            {
                if (!ZnodePromotionHelper.IsApplicablePromotion(ApplicablePromolist, PromotionBag.PromoCode, cartCount, ShoppingCart.SubTotal, false))
                {
                    isPromotionApplied = false;
                    return;
                }

                if (cartItem.Product.ZNodeGroupProductCollection.Count > 0)
                {
                    discountApplied = ApplyDiscountForGroupProducts(cartItem, subTotal, cartCount, promotionBagDiscount);
                }
                else if (PromotionBag.RequiredCatalogMinimumQuantity <= cartCount && PromotionBag.MinimumOrderAmount <= subTotal)
                {
                    // Product discount amount
                    if (IsDiscountApplicable(cartItem.Product.DiscountAmount, cartItem.Product.FinalPrice))
                    {
                        SetProductDiscount(cartItem, promotionBagDiscount, PromotionBag.PromoCode, OrderDiscountTypeEnum.PROMOCODE, cartItem.Product.OrdersDiscount);
                        SetPromotionalPriceAndDiscount(cartItem, PromotionBag.Discount);
                    }
                    discountApplied = true;
                    ShoppingCart.IsAnyPromotionApplied = discountApplied;
                }
            }
            else if (!Equals(PromotionBag.Coupons, null) && isCouponValid)
            {
                if (!ZnodePromotionHelper.IsApplicablePromotion(ApplicablePromolist, PromotionBag.PromoCode, cartCount, ShoppingCart.SubTotal, true))
                {
                    isPromotionApplied = false;
                    return;
                }

                foreach (CouponModel coupon in PromotionBag.Coupons)
                {
                    if ((coupon.AvailableQuantity > 0 || IsExistingOrderCoupon(coupon.Code)) && CheckCouponCodeValid(ShoppingCart.Coupons[couponIndex.Value].Coupon, coupon.Code))
                    {
                        if (PromotionBag.RequiredCatalogMinimumQuantity <= cartCount && PromotionBag.MinimumOrderAmount <= subTotal)
                        {
                            SetProductDiscount(cartItem, promotionBagDiscount, coupon.Code, OrderDiscountTypeEnum.COUPONCODE, cartItem.Product.OrdersDiscount);
                            SetPromotionalPriceAndDiscount(cartItem, PromotionBag.Discount);
                            SetCouponApplied(coupon.Code);
                            ShoppingCart.Coupons[couponIndex.Value].CouponApplied = true;
                            discountApplied = true;
                        }

                        if (IsUniqueCouponApplied(PromotionBag, ShoppingCart.Coupons[couponIndex.Value].CouponApplied))
                        {
                            break;
                        }
                    }
                }
            }
            isPromotionApplied = discountApplied;
        }

        private bool ApplyDiscountForGroupProducts(ZnodeShoppingCartItem cartItem, decimal subTotal, decimal cartCount, decimal promotionBagDiscount)
        {
            bool discountApplied = false;

            foreach (ZnodeProductBaseEntity group in cartItem.Product.ZNodeGroupProductCollection)
            {
                if (group.FinalPrice > 0.0M && PromotionBag.RequiredCatalogMinimumQuantity <= cartCount && PromotionBag.MinimumOrderAmount <= subTotal)
                {
                    if (IsDiscountApplicable(group.DiscountAmount, group.FinalPrice))
                    {
                        SetProductDiscount(cartItem, promotionBagDiscount, PromotionBag.PromoCode, OrderDiscountTypeEnum.PROMOCODE, group.OrdersDiscount);
                        discountApplied = true;
                    }
                    ShoppingCart.IsAnyPromotionApplied = discountApplied;
                }
            }

            return discountApplied;
        }

        //Set Product Discount
        private void SetProductDiscount(ZnodeShoppingCartItem cartItem, decimal promotionBagDiscount, string discountCode, OrderDiscountTypeEnum discountType, List<OrderDiscountModel> ordersDiscount)
        {
            cartItem.Product.DiscountAmount += promotionBagDiscount;
            cartItem.Product.OrdersDiscount = SetOrderDiscountDetails(discountCode, promotionBagDiscount, discountType, ordersDiscount);
        }
    }
    #endregion
}


