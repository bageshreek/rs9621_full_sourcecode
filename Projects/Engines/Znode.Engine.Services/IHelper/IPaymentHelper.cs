﻿using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IPaymentHelper
    {
        /// <summary>
        /// Process Payment
        /// </summary>
        /// <param name="convertToOrderModel"></param>
        /// <param name="cartModel"></param>
        /// <returns></returns>
        GatewayResponseModel ProcessPayment(ConvertQuoteToOrderModel convertToOrderModel, ShoppingCartModel cartModel);

        bool IsPaypalExpressPayment(string paymentType);

        bool IsCreditCardPayment(string paymentType);

        bool IsAmazonPayPayment(string paymentType);

        bool CapturePayment(string paymentTransactionToken);
    }
}
