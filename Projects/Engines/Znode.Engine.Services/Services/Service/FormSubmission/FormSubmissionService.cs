﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Utilities;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using Znode.Libraries.Resources;
using Znode.Libraries.Framework.Business;
using System.Diagnostics;

namespace Znode.Engine.Services
{
    public class FormSubmissionService : BaseService, IFormSubmissionService
    {
        #region Priavte Variable
        private readonly IZnodeRepository<ZnodeFormBuilder> _formBuilderRepository;
        private readonly IZnodeRepository<ZnodeGlobalAttributeGroup> _globalAttributeGroupRepository;
        private readonly IZnodeRepository<ZnodeFormBuilderAttributeMapper> _formMapperRepository;
        private readonly IZnodeRepository<ZnodeGlobalAttributeGroupLocale> _attributeGroupLocaleRepository;
        private readonly IZnodeRepository<ZnodeFormBuilderSubmit> _formSubmitRepository;
        #endregion

        #region Public Constructor
        public FormSubmissionService()
        {
            _formBuilderRepository = new ZnodeRepository<ZnodeFormBuilder>();
            _globalAttributeGroupRepository = new ZnodeRepository<ZnodeGlobalAttributeGroup>();
            _formMapperRepository = new ZnodeRepository<ZnodeFormBuilderAttributeMapper>();
            _attributeGroupLocaleRepository = new ZnodeRepository<ZnodeGlobalAttributeGroupLocale>();
            _formSubmitRepository = new ZnodeRepository<ZnodeFormBuilderSubmit>();
        }
        #endregion

        #region Public Method
        //Get Form Submission list.
        public virtual FormSubmissionListModel GetFormSubmissionList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

            //Bind the Filter, sorts & Paging details.
            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            ZnodeLogging.LogMessage("pageListModel to set SP parameters ", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new object[] { pageListModel?.ToDebugString() });

            IZnodeViewRepository<FormSubmissionModel> objStoredProc = new ZnodeViewRepository<FormSubmissionModel>();
            objStoredProc.SetParameter("@WhereClause", pageListModel.SPWhereClause, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@Order_BY", pageListModel.OrderBy, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@RowsCount", pageListModel.TotalRowCount, ParameterDirection.Output, DbType.Int32);
            List<FormSubmissionModel> formSubmissionList = objStoredProc.ExecuteStoredProcedureList("Znode_GetFormBuilderSubmitList @WhereClause,@Rows,@PageNo,@Order_BY,@RowsCount OUT", 4, out pageListModel.TotalRowCount).ToList();
            ZnodeLogging.LogMessage("formSubmissionList count ", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new object[] { formSubmissionList?.Count });

            FormSubmissionListModel listModel = new FormSubmissionListModel();
            listModel.FormSubmissionList = formSubmissionList?.Count > 0 ? formSubmissionList : new List<FormSubmissionModel>();
            listModel.BindPageListModel(pageListModel);

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

            return listModel;
        }

        //Get Form Submission Details
        public virtual FormBuilderAttributeGroupModel GetFormSubmitDetails(int formSubmitId)
        {
           ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

            FormBuilderAttributeGroupModel model = new FormBuilderAttributeGroupModel();
            if (formSubmitId < 1)
                throw new ZnodeException(ErrorCodes.InvalidData,Admin_Resources.IdCanNotBeLessThanOne);

            ZnodeLogging.LogMessage("Input parameters formSubmitId ", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new object[] { formSubmitId });

            int localeId = _formSubmitRepository.GetById(formSubmitId)?.LocaleId ?? 0;
            ZnodeLogging.LogMessage("localeId to set parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new object[] { localeId });

            localeId = localeId == 0 ? Convert.ToInt32(DefaultGlobalConfigSettingHelper.Locale) : localeId;

            IZnodeViewRepository<GlobalAttributeValuesModel> globalAttributeValues = new ZnodeViewRepository<GlobalAttributeValuesModel>();
            globalAttributeValues.SetParameter(ZnodeFormBuilderSubmitEnum.FormBuilderId.ToString(), 0, ParameterDirection.Input, DbType.Int32);
            globalAttributeValues.SetParameter(ZnodeFormBuilderSubmitEnum.UserId.ToString(), 0, ParameterDirection.Input, DbType.Int32);
            globalAttributeValues.SetParameter(ZnodeFormBuilderSubmitEnum.PortalId.ToString(), 0, ParameterDirection.Input, DbType.Int32);
            globalAttributeValues.SetParameter(ZnodeFormBuilderSubmitEnum.FormBuilderSubmitId.ToString(), formSubmitId, ParameterDirection.Input, DbType.Int32);
            globalAttributeValues.SetParameter(ZnodeFormBuilderSubmitEnum.LocaleId.ToString(), localeId, ParameterDirection.Input, DbType.Int32);
            model.Attributes = globalAttributeValues.ExecuteStoredProcedureList("Znode_GetFormBuilderGlobalAttributeValue @FormBuilderId,@UserId,@PortalId,@FormBuilderSubmitId,@LocaleId").ToList();
            model.Groups = GetFormAssociatedGroup(formSubmitId);
            ZnodeLogging.LogMessage("Groups count returned from GetFormAssociatedGroup :", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new object[] { model?.Groups?.Count });

            GetFormBuilderDetails(formSubmitId, model);

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

            return model;
        }

        #endregion

        #region Private Method

        //Get Form Associated Group
        private List<GlobalAttributeGroupModel> GetFormAssociatedGroup(int formSubmitId)
        {

            List<GlobalAttributeGroupModel> groupList = (from frmsub in _formSubmitRepository.Table
                                                         join frm in _formBuilderRepository.Table on frmsub.FormBuilderId equals frm.FormBuilderId
                                                         join map in _formMapperRepository.Table on frm.FormBuilderId equals map.FormBuilderId
                                                         join grp in _globalAttributeGroupRepository.Table on map.GlobalAttributeGroupId equals grp.GlobalAttributeGroupId
                                                         join loc in _attributeGroupLocaleRepository.Table on grp.GlobalAttributeGroupId equals loc.GlobalAttributeGroupId
                                                         where frmsub.FormBuilderSubmitId == formSubmitId
                                                         && loc.LocaleId == frmsub.LocaleId
                                                         orderby map.DisplayOrder
                                                         select new GlobalAttributeGroupModel
                                                         {
                                                             AttributeGroupName = loc.AttributeGroupName,
                                                             GroupCode = grp.GroupCode,
                                                             GlobalAttributeGroupId = grp.GlobalAttributeGroupId,
                                                             DisplayOrder = map.DisplayOrder

                                                         }).ToList();
            if (groupList?.Count < 1)
            {
                int localeId = Convert.ToInt32(DefaultGlobalConfigSettingHelper.Locale);
                groupList = (from frmsub in _formSubmitRepository.Table
                             join frm in _formBuilderRepository.Table on frmsub.FormBuilderId equals frm.FormBuilderId
                             join map in _formMapperRepository.Table on frm.FormBuilderId equals map.FormBuilderId
                             join grp in _globalAttributeGroupRepository.Table on map.GlobalAttributeGroupId equals grp.GlobalAttributeGroupId
                             join loc in _attributeGroupLocaleRepository.Table on grp.GlobalAttributeGroupId equals loc.GlobalAttributeGroupId
                             where frmsub.FormBuilderSubmitId == formSubmitId
                           && loc.LocaleId == localeId
                             orderby map.DisplayOrder
                             select new GlobalAttributeGroupModel
                             {
                                 AttributeGroupName = loc.AttributeGroupName,
                                 GroupCode = grp.GroupCode,
                                 GlobalAttributeGroupId = grp.GlobalAttributeGroupId,
                                 DisplayOrder = map.DisplayOrder

                             }).ToList();
            }
            return groupList ?? new List<GlobalAttributeGroupModel>();
        }

        //Get FormBuilder Details By formSubmissionId
        private void GetFormBuilderDetails(int formSubmissionId, FormBuilderAttributeGroupModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

            ZnodeLogging.LogMessage("Input parameters formSubmissionId ", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new object[] { formSubmissionId });

            FormBuilderModel formDetails = (from sub in _formSubmitRepository.Table
                                            join frm in _formBuilderRepository.Table on sub.FormBuilderId equals frm.FormBuilderId
                                            where sub.FormBuilderSubmitId == formSubmissionId
                                            select new FormBuilderModel
                                            {
                                                FormBuilderId = frm.FormBuilderId,
                                                FormCode = frm.FormCode

                                            })?.FirstOrDefault() ?? null;
            if (IsNotNull(formDetails))
            {
                model.FormBuilderId = formDetails.FormBuilderId;
                model.FormCode = formDetails.FormCode;
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

        }
        #endregion
    }
}
