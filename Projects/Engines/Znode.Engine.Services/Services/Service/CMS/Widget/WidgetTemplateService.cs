﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Utilities;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;
using Znode.Libraries.Framework.Business;
using Znode.Engine.Exceptions;
using Znode.Libraries.Resources;
using Znode.Libraries.Data.Helpers;
using System.Data;

namespace Znode.Engine.Services
{
    public class WidgetTemplateService : BaseService, IWidgetTemplateService
    {
        #region Private Variable
        protected readonly IZnodeRepository<ZnodeCMSWidgetTemplate> _widgetTemplateRepository;
        protected readonly IZnodeRepository<ZnodeMedia> _mediaRepository;
        #endregion

        #region Constructor
        public WidgetTemplateService()
        {
            _widgetTemplateRepository = new ZnodeRepository<ZnodeCMSWidgetTemplate>();
            _mediaRepository = new ZnodeRepository<ZnodeMedia>();
        }
        #endregion

        #region Public Methods

        //Get the List of Widget Template
        public virtual WidgetTemplateListModel GetWidgetTemplateList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
 
            //Bind the Filter, sorts & Paging details.
            PageListModel pageListModel = new PageListModel(filters, sorts, page);

            WidgetTemplateListModel listModel = new WidgetTemplateListModel();
            listModel.WidgetTemplates = WidgetTemplateList(pageListModel);

            listModel.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return listModel;
        }

        //Create Widget Template
        public virtual WidgetTemplateModel CreateWidgetTemplate(WidgetTemplateCreateModel widgetTemplate)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            if (Equals(widgetTemplate, null))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ModelNotNull);

            //Validate if the name already exists
            if (IsWidgetTemplateExists(widgetTemplate.Code))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.TemplateCodeExists);

            if(string.IsNullOrEmpty(widgetTemplate.FileName))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.InvalidData);

            ZnodeCMSWidgetTemplate model = _widgetTemplateRepository.Insert(widgetTemplate.ToEntity<ZnodeCMSWidgetTemplate>());

            return model.ToModel<WidgetTemplateModel>();
        }

        //Get Widget Template
        public virtual WidgetTemplateModel GetWidgetTemplate(string templateCode)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            if (string.IsNullOrEmpty(templateCode))
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.InvalidTemplateCode);

            WidgetTemplateModel model = _widgetTemplateRepository.Table.FirstOrDefault(x => x.Code.ToLower() == templateCode.ToLower()).ToModel<WidgetTemplateModel>();
            GetMediaDetails(model);
            return model;
        }
        
        //Update Widget Template
        public virtual WidgetTemplateModel UpdateWidgetTemplate(WidgetTemplateUpdateModel widgetTemplateModel)
        {

            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            int templateId = GetTemplateId(widgetTemplateModel.Code);

            ZnodeCMSWidgetTemplate model = _widgetTemplateRepository.Table.FirstOrDefault(x => x.CMSWidgetTemplateId == templateId);

            if (Equals(model, null))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorModelNull);

            model.FileName = widgetTemplateModel.FileName;
            model.MediaId = widgetTemplateModel.MediaId;
            model.Name = widgetTemplateModel.Name;

            bool isUpdated = _widgetTemplateRepository.Update(model);

            return model.ToModel<WidgetTemplateModel>();
        }

        //Delete Template by Code
        public virtual bool DeleteWidgetTemplateByCode(string templateCode)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            int widgetTemplateId = GetTemplateId(templateCode);
            return DeleteWidgetTemplate(widgetTemplateId.ToString());

        }

        //Delete Template By Ids
        public virtual bool DeleteWidgetTemplateById(ParameterModel WidgetTemplateIds)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return DeleteWidgetTemplate(WidgetTemplateIds.Ids);
        }


        //Validate if the Widget Template Exists
        public virtual bool IsWidgetTemplateExists(string templateCode)
        {
            if (string.IsNullOrEmpty(templateCode))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.InvalidTemplateCode);

            return _widgetTemplateRepository.Table.Any(x => x.Code.ToLower() == templateCode.ToLower());
        }


        #endregion

        #region Private Methods

        //Delete Widget Template
        protected virtual bool DeleteWidgetTemplate(string WidgetTemplateIds)
        {
            int status = 0;
            IZnodeViewRepository<View_ReturnBoolean> objStoredProc = new ZnodeViewRepository<View_ReturnBoolean>();
            objStoredProc.SetParameter("@WidgetTemplateIds", WidgetTemplateIds, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Status", null, ParameterDirection.Output, DbType.Int32);
            var deleteResult = objStoredProc.ExecuteStoredProcedureList("Znode_DeleteCMSWidgetTemplates @WidgetTemplateIds,@Status OUT", 1, out status);

            if (deleteResult.FirstOrDefault().Status.Value)
            {
                ZnodeLogging.LogMessage(Admin_Resources.DeleteMessage, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
                return true;
            }
            else
            {
                throw new ZnodeException(ErrorCodes.AssociationDeleteError, Admin_Resources.ErrorFailedToDelete);
            }

        }

        //Get media Details
        protected virtual void GetMediaDetails(WidgetTemplateModel model)
        {
            if (HelperUtility.IsNotNull(model) && HelperUtility.IsNotNull(model.MediaId))
            {
                ZnodeMedia media = _mediaRepository.Table.FirstOrDefault(x => x.MediaId == model.MediaId);
                if (HelperUtility.IsNotNull(media))
                {
                    model.MediaPath = media.Path;
                    model.Version = media.Version;
                    MediaConfigurationModel configurationModel = GetService<IMediaConfigurationService>().GetDefaultMediaConfiguration();
                    string serverPath = GetMediaServerUrl(configurationModel);

                    model.MediaPath = GetMediaPath(model, serverPath, configurationModel.ThumbnailFolderName).MediaThumbNailPath;
                }

            }

        }

        //Get widget template list
        protected virtual List<WidgetTemplateModel> WidgetTemplateList(PageListModel pageListModel)
        {
         List<WidgetTemplateModel> templateList = (from _template in _widgetTemplateRepository.GetPagedList(pageListModel.EntityWhereClause.WhereClause, pageListModel.OrderBy, null, pageListModel.EntityWhereClause.FilterValues, pageListModel.PagingStart, pageListModel.PagingLength, out pageListModel.TotalRowCount)
                                                    join _media in _mediaRepository.Table on _template.MediaId equals _media.MediaId
                                                    into templateMedia
                                                    from media in templateMedia.DefaultIfEmpty()
                                                    select new WidgetTemplateModel
                                                    {
                                                        WidgetTemplateId = _template.CMSWidgetTemplateId,
                                                        Name = _template.Name,
                                                        FileName = _template.FileName,
                                                        MediaId = _template.MediaId,
                                                        Code = _template.Code,
                                                        MediaPath = media == null ? string.Empty : (media.Path ?? string.Empty),
                                                    }).ToList();

            MediaConfigurationModel configurationModel = GetService<IMediaConfigurationService>().GetDefaultMediaConfiguration();
            string serverPath = GetMediaServerUrl(configurationModel);
          
            templateList?.ForEach(
                x =>
                {
                    x.MediaPath = GetMediaPath(x, serverPath, configurationModel.ThumbnailFolderName).MediaThumbNailPath;

                });
            return templateList;

        }

        //Get media path
        protected virtual WidgetTemplateModel GetMediaPath(WidgetTemplateModel templateModel, string serverPath, string thumbnailFolderName)
        {
            string mediaServerPath = string.IsNullOrEmpty(templateModel.MediaPath) ? string.Empty : $"{serverPath}{templateModel.MediaPath}?v={templateModel.Version}";
            string mediaServerThumbnailPath = string.IsNullOrEmpty(templateModel.MediaPath) ? string.Empty : $"{serverPath}{thumbnailFolderName}/{templateModel.MediaPath}?v={templateModel.Version}";
            templateModel.MediaPath = mediaServerPath;
            templateModel.MediaThumbNailPath = mediaServerThumbnailPath;
            return templateModel;
        }

        //Get Widget Id
        private int GetTemplateId(string code)
        {
            int widgetTemplateId = (from templateRepository in _widgetTemplateRepository.Table.Where(x => x.Code.ToLower() == code.ToLower()) select templateRepository.CMSWidgetTemplateId).FirstOrDefault();

            if (widgetTemplateId < 1)
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.InvalidTemplateCode);

            return widgetTemplateId;

        }
        #endregion
    }
}
