﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Threading;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Observer;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Engine.Services
{
    public class RMAReturnService : BaseService, IRMAReturnService
    {
        #region Private Variables
        private readonly IZnodeRepository<ZnodeRmaReturnDetail> _returnDetailsRepository;
        private readonly IZnodeRepository<ZnodeRmaReturnLineItem> _returnLineItemsRepository;
        private readonly IZnodeRepository<ZnodeRmaReturnState> _returnStateRepository;
        private readonly IZnodeRepository<ZnodeRmaReturnNote> _returnNoteRepository;
        #endregion

        #region Constructor
        public RMAReturnService()
        {
            _returnDetailsRepository = new ZnodeRepository<ZnodeRmaReturnDetail>();
            _returnLineItemsRepository = new ZnodeRepository<ZnodeRmaReturnLineItem>();
            _returnStateRepository = new ZnodeRepository<ZnodeRmaReturnState>();
            _returnNoteRepository = new ZnodeRepository<ZnodeRmaReturnNote>();
        }
        #endregion

        #region Public Methods
        //Get Return Eligible Order Number List.
        public virtual RMAReturnOrderNumberListModel GetEligibleOrderNumberListForReturn(int userId, int portalId)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            RMAReturnOrderNumberListModel rmaReturnOrderNumberList = new RMAReturnOrderNumberListModel();
            rmaReturnOrderNumberList.ReturnEligibleOrderNumberList = GetReturnEligibleOrderNumberList(userId, portalId, string.Empty)?.ToList();
            return rmaReturnOrderNumberList;
        }

        //Check Is Order Eligible for Return Items
        public virtual bool IsOrderEligibleForReturn(int userId, int portalId, string orderNumber)
        {
            if (string.IsNullOrEmpty(orderNumber))
                return false;

            return GetReturnEligibleOrderNumberList(userId, portalId, orderNumber)?.ToList()?.Count > 0 ? true : false;
        }

        //Get order details by order number for return.
        public virtual OrderModel GetOrderDetailsForReturn(int userId, string orderNumber)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (string.IsNullOrEmpty(orderNumber) || Equals(userId, 0))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorOrderNumberNull);

            OrderModel orderModel = GetOrderDetails(userId, orderNumber);

            if (orderModel == null)
                return null;

            if (IsNotNull(orderModel?.OrderLineItems) && orderModel.OrderLineItems.Count > 0)
                orderModel.OrderLineItems = GetService<IZnodeOrderHelper>()?.FormalizeOrderLineItems(orderModel);

            ValidOrderLineItemsForReturn(orderModel);

            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return orderModel;
        }

        //Get return list.
        public virtual RMAReturnListModel GetReturnList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            //Replace sort key name.
            ReplaceSortKeys(ref sorts);

            //Add date time value in filter collection against filter column name Order date.
            filters = ServiceHelper.AddDateTimeValueInFilterByName(filters, ZnodeRmaReturnDetailEnum.ReturnDate.ToString().ToLower());

            bool isAdminRequest = Convert.ToBoolean(filters?.Find(x => string.Equals(x.FilterName, Libraries.ECommerce.Utilities.FilterKeys.IsFromAdmin, StringComparison.CurrentCultureIgnoreCase))?.Item3);
            string returnDateRange = string.Empty;

            if (isAdminRequest)
            {
                BindUserPortalFilter(ref filters);
                returnDateRange = GetReturnDateRangeFromFilters(filters);
            }
            RemoveFilter(filters, Libraries.ECommerce.Utilities.FilterKeys.IsFromAdmin);

            PageListModel pageListModel = new PageListModel(filters, sorts, page);

            IList<RMAReturnModel> returnList = GetReturnList(pageListModel, isAdminRequest, returnDateRange);
            ZnodeLogging.LogMessage("Return list count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, returnList?.Count);
            RMAReturnListModel returnListModel = new RMAReturnListModel() { Returns = returnList?.ToList() };
            returnListModel.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            return returnListModel;
        }

        //Gets order return details by return number
        public virtual RMAReturnModel GetReturnDetails(string rmaReturnNumber, NameValueCollection expands = null)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (string.IsNullOrEmpty(rmaReturnNumber))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorReturnNumberNull);

            RMAReturnModel returnModel = _returnDetailsRepository.Table.FirstOrDefault(x => x.ReturnNumber.ToLower() == rmaReturnNumber.ToLower())?.ToModel<RMAReturnModel>();
            //Get expand details
            bool isReturnItemList = !string.IsNullOrEmpty(expands.Get(ExpandKeys.ReturnItemList));
            bool isReturnShippingDetails = !string.IsNullOrEmpty(expands.Get(ExpandKeys.ReturnShippingDetails));
            bool isReturnNotes = !string.IsNullOrEmpty(expands.Get(ExpandKeys.ReturnNotes));
            bool isReturnProductImages = !string.IsNullOrEmpty(expands.Get(ExpandKeys.ReturnProductImages));
            bool isReturnBarcode = !string.IsNullOrEmpty(expands.Get(ExpandKeys.ReturnBarcode));

            if (IsNotNull(returnModel))
            {
                returnModel.ReturnStatus = Enum.GetName(typeof(ZnodeReturnStateEnum), returnModel.RmaReturnStateId)?.Replace('_', ' ')?.ToLower()?.ToProperCase();

                if (isReturnItemList)
                {
                    returnModel.ReturnLineItems = GetReturnLineItems(returnModel.RmaReturnDetailsId, true);

                    if (returnModel?.ReturnLineItems?.Count > 0 && isReturnProductImages)
                        returnModel.ReturnLineItems = GetProductImagePath(returnModel.ReturnLineItems, returnModel.PortalId);

                    ZnodeLogging.LogMessage("Return line item list count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, returnModel.ReturnLineItems?.Count);
                }
                if (isReturnShippingDetails)
                {
                    WarehouseModel warehouseDetails = GetReturnWarehouseDetails(returnModel.PortalId);
                    returnModel.ShippingToAddressHtml = GetReturnShippingAddressHTML(warehouseDetails?.Address, warehouseDetails?.WarehouseName);

                    AddressModel customerAddress = GetCustomerAddressDetails(returnModel.AddressId);
                    returnModel.ShippingFromAddressHtml = GetReturnShippingAddressHTML(customerAddress, customerAddress?.FirstName + " " + customerAddress?.LastName);
                }
                if (isReturnNotes)
                    returnModel.ReturnNotes = GetReturnNotes(returnModel);

                if (isReturnBarcode)
                    returnModel.BarcodeImage = GetBarcodeImage(returnModel.ReturnNumber);
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return returnModel;
        }

        //Insert or update order return details.
        public virtual RMAReturnModel SaveOrderReturn(RMAReturnModel returnModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            if (IsNull(returnModel) || IsNull(returnModel?.ReturnLineItems) || returnModel?.ReturnLineItems?.Count == 0)
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorModelNull);
            if (returnModel.PortalId < 1)
                throw new ZnodeException(ErrorCodes.NotFound, Admin_Resources.PortalIdNotLessThanOne);
            if (returnModel.UserId < 1)
                throw new ZnodeException(ErrorCodes.NotFound, Admin_Resources.ErrorUserIdLessThanOne);
            if (string.IsNullOrEmpty(returnModel?.OrderNumber))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorOrderNumberEmptyOrNull);

            if (returnModel.IsAdminRequest)
                return UpdateOrderReturnForAdmin(returnModel);

            if (!IsRMAConfigurationValidForOrder(returnModel.OrderNumber, returnModel.UserId))
                throw new ZnodeException(ErrorCodes.NotOrderEligibleForReturn, Admin_Resources.ErrorOrderNotEligibleForReturn);

            ZnodeLogging.LogMessage("Input parameter RmaReturnDetailsId of returnModel: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { RmaReturnDetailsId = returnModel?.RmaReturnDetailsId });

            returnModel.ActionMode = string.IsNullOrEmpty(returnModel.ReturnNumber) ? ZnodeConstant.Create : ZnodeConstant.Edit;

            if (ValidateOrderReturn(returnModel?.ReturnLineItems, returnModel.ActionMode))
            {
                return string.IsNullOrEmpty(returnModel.ReturnNumber) ? InsertOrderReturn(returnModel) : UpdateOrderReturn(returnModel);
            }
            else
            {
                ZnodeLogging.LogMessage(Admin_Resources.InvalidData, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Warning);
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.InvalidData);
            }
        }

        //To generate unique return number on basis of current date.
        public virtual string GenerateReturnNumber(RMAReturnModel returnModel, ParameterModel portalId = null)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            var _erpInc = new ERPInitializer<RMAReturnModel>(returnModel, "GetReturnNumber");
            if (string.IsNullOrEmpty(Convert.ToString(_erpInc.Result)))
            {
                string returnNumber = "RO";
                string storeName = ZnodeConfigManager.SiteConfig.StoreName;
                if (!string.IsNullOrEmpty(storeName))
                    returnNumber += storeName.Trim().Length > 2 ? storeName.Substring(0, 2) : storeName.Substring(0, 1);

                DateTime date = GetDateWithTime();
                String strDate = date.ToString("yyMMdd-HHmmss-fff");
                returnNumber += $"-{strDate}";
                return returnNumber.ToUpper();
            }
            else
                return Convert.ToString(_erpInc?.Result);
        }

        //Delete order return
        public virtual bool DeleteOrderReturn(int returnDetailsId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (Equals(returnDetailsId, 0))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorFailedToDelete);

            FilterCollection filters = new FilterCollection { new FilterTuple("RmaReturnDetailsId", ProcedureFilterOperators.Equals, returnDetailsId.ToString()) };

            EntityWhereClauseModel whereClauseForDelete = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection());
            ZnodeLogging.LogMessage("WhereClause generated for delete: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, whereClauseForDelete?.WhereClause);
            _returnNoteRepository.Delete(whereClauseForDelete.WhereClause, whereClauseForDelete.FilterValues);
            _returnLineItemsRepository.Delete(whereClauseForDelete.WhereClause, whereClauseForDelete.FilterValues);
            return _returnDetailsRepository.Delete(whereClauseForDelete.WhereClause, whereClauseForDelete.FilterValues);
        }

        //Delete order return on the basis of return number.
        public virtual bool DeleteOrderReturnByReturnNumber(string rmaReturnNumber, int userId)
        {
            ZnodeLogging.LogMessage("Execution started", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            if (string.IsNullOrEmpty(rmaReturnNumber) || Equals(userId, 0))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorFailedToDelete);

            int rmaReturnDetailsId = _returnDetailsRepository.Table.FirstOrDefault(x => x.ReturnNumber.ToLower() == rmaReturnNumber.ToLower() && x.UserId == userId)?.RmaReturnDetailsId ?? 0;

            if (rmaReturnDetailsId > 0)
                return DeleteOrderReturn(rmaReturnDetailsId);

            return false;
        }

        //Perform calculations for a order return line item.
        public virtual RMAReturnCalculateModel CalculateOrderReturn(RMAReturnCalculateModel returnCalculateModel)
        {
            if (IsNull(returnCalculateModel) || IsNull(returnCalculateModel?.ReturnCalculateLineItemList) || returnCalculateModel?.ReturnCalculateLineItemList?.Count == 0)
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorModelNull);
            if (returnCalculateModel.PortalId < 1)
                throw new ZnodeException(ErrorCodes.NotFound, Admin_Resources.PortalIdNotLessThanOne);
            if (returnCalculateModel.UserId < 1)
                throw new ZnodeException(ErrorCodes.NotFound, Admin_Resources.ErrorUserIdLessThanOne);
            if (string.IsNullOrEmpty(returnCalculateModel?.OrderNumber))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorReturnNumberNull);


            if (ValidateCalculationOrderReturn(returnCalculateModel?.ReturnCalculateLineItemList))
            {
                int omsOrderId = GetOrderDetailsByOrderNumber(returnCalculateModel.OrderNumber, returnCalculateModel.UserId)?.OmsOrderId ?? 0;
                ShoppingCartModel shoppingCartModel = GetShoppingCartDetails(returnCalculateModel.UserId, returnCalculateModel.PortalId, omsOrderId);

                PerformCalculationForOrderReturnLineItem(returnCalculateModel, shoppingCartModel);
            }
            else
            {
                ZnodeLogging.LogMessage(Admin_Resources.InvalidData, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Warning);
            }
            return returnCalculateModel;
        }

        //Get List of Return States
        public virtual RMAReturnStateListModel GetReturnStatusList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Info);

            //Bind the Filter, sorts & Paging details.
            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            ZnodeLogging.LogMessage("pageListModel for GetPagedList", ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Verbose, new object[] { pageListModel?.ToDebugString() });

            IList<ZnodeRmaReturnState> returnStatusList = _returnStateRepository.GetPagedList(pageListModel.EntityWhereClause.WhereClause, pageListModel.OrderBy, null, pageListModel.EntityWhereClause.FilterValues, pageListModel.PagingStart, pageListModel.PagingLength, out pageListModel.TotalRowCount);
            ZnodeLogging.LogMessage("returnStatusList count", ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Verbose, new object[] { returnStatusList?.Count });

            RMAReturnStateListModel rmaReturnStateListModel = new RMAReturnStateListModel();
            rmaReturnStateListModel.ReturnStates = returnStatusList?.Count > 0 ? returnStatusList.ToModel<RMAReturnStateModel>().ToList() : new List<RMAReturnStateModel>();

            rmaReturnStateListModel.BindPageListModel(pageListModel);

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            return rmaReturnStateListModel;
        }

        //Gets order return details from admin by return number
        public virtual RMAReturnModel GetReturnDetailsForAdmin(string rmaReturnNumber)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (string.IsNullOrEmpty(rmaReturnNumber))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorReturnNumberNull);

            RMAReturnModel returnModel = _returnDetailsRepository.Table.FirstOrDefault(x => x.ReturnNumber.ToLower() == rmaReturnNumber.ToLower())?.ToModel<RMAReturnModel>();

            if (IsNull(returnModel))
                throw new ZnodeException(ErrorCodes.NotFound, Admin_Resources.ErrorReturnNumberNotFound);

            returnModel.ReturnStatus = _returnStateRepository.GetById(returnModel.RmaReturnStateId)?.ReturnStateName;
            returnModel.CreatedByName = returnModel.CreatedBy == returnModel.UserId ? returnModel.EmailId : new ZnodeRepository<ZnodeUser>().Table.FirstOrDefault(x => x.UserId == returnModel.CreatedBy)?.Email;
            returnModel.StoreName = (from portalRepository in new ZnodeRepository<ZnodePortal>().Table.Where(x => x.PortalId == returnModel.PortalId) select portalRepository.StoreName)?.FirstOrDefault();
            returnModel.ReturnLineItems = GetReturnLineItems(returnModel.RmaReturnDetailsId);
            returnModel.OldReturnLineItems = returnModel.ReturnLineItems;
            returnModel.ReturnHistoryAndNotesList = GetReturnHistoryAndNotes(returnModel.RmaReturnDetailsId);
            returnModel.RMAOrderModel = GetOrderDetails(returnModel.UserId, returnModel.OrderNumber, true);
            returnModel.BarcodeImage = GetBarcodeImage(returnModel.ReturnNumber);
            WarehouseModel warehouseDetails = GetReturnWarehouseDetails(returnModel.PortalId);
            returnModel.ShippingToAddressHtml = GetReturnShippingAddressHTML(warehouseDetails?.Address, warehouseDetails?.WarehouseName);
            AddressModel customerAddress = GetCustomerAddressDetails(returnModel.AddressId);
            returnModel.ShippingFromAddressHtml = GetReturnShippingAddressHTML(customerAddress, customerAddress?.FirstName + " " + customerAddress?.LastName);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return returnModel;
        }

        //Save return history
        public virtual bool CreateReturnHistory(List<RMAReturnHistoryModel> returnHistoryModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            if (IsNull(returnHistoryModel) && returnHistoryModel?.Count < 1)
            {
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ModelInvalid);
            }
            else
            {
                IEnumerable<ZnodeRmaReturnHistory> returnHistory = new ZnodeRepository<ZnodeRmaReturnHistory>().Insert(returnHistoryModel?.ToEntity<ZnodeRmaReturnHistory>()?.ToList());
                ZnodeLogging.LogMessage(IsNotNull(returnHistory) && returnHistory?.Count() > 0 ? Admin_Resources.SuccessCreateReturnHistory : Admin_Resources.ErrorCreateReturnHistory, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                return returnHistory?.Count() > 0;
            }
        }

        //Save additional notes for return.
        public virtual bool SaveReturnNotes(RMAReturnNotesModel rmaReturnNotesModel)
        {
            ZnodeLogging.LogMessage("Execution started", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            if (IsNull(rmaReturnNotesModel) && string.IsNullOrEmpty(rmaReturnNotesModel.Notes) && Equals(rmaReturnNotesModel.RmaReturnDetailsId, 0))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.InvalidData);

            ZnodeLogging.LogMessage("Input parameters:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { rmaReturnNotesModel = rmaReturnNotesModel });

            ZnodeRmaReturnNote returnNotes = _returnNoteRepository.Insert(rmaReturnNotesModel?.ToEntity<ZnodeRmaReturnNote>());

            return returnNotes?.RmaReturnNotesId > 0 ? true : false;
        }
        #endregion

        #region Protected Methods
        //Get Return Eligible Order Number List by using store procedure.
        protected virtual IList<string> GetReturnEligibleOrderNumberList(int userId, int portalId, string orderNumber)
        {
            if (string.IsNullOrEmpty(orderNumber) || Equals(userId, 0) || Equals(portalId, 0))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.InvalidData);

            //Get RMA Configuration
            RMAConfigurationModel rmaConfiguration = GetService<IRMAConfigurationService>().GetRMAConfiguration();

            IList<string> eligibleOrderNumberList = null;

            if (IsNull(rmaConfiguration) || rmaConfiguration?.MaxDays == 0)
                return eligibleOrderNumberList;

            ZnodeLogging.LogMessage("Input parameters to Get Eligible OrderNumber List For Return:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { userId = userId, portalId = portalId, orderNumber = orderNumber });
            IZnodeViewRepository<string> objStoredProc = new ZnodeViewRepository<string>();
            objStoredProc.SetParameter("@UserId", userId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PortalId", portalId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@MaxDays", rmaConfiguration.MaxDays, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@OrderNumber", orderNumber, ParameterDirection.Input, DbType.String);
            eligibleOrderNumberList = objStoredProc.ExecuteStoredProcedureList("Znode_GetEligibleOrderNumberListForReturn @UserId, @PortalId,@MaxDays,@OrderNumber");
            return eligibleOrderNumberList;
        }

        //get the order details by order number.
        protected virtual OrderModel GetOrderDetails(int userId, string orderNumber, bool IsAdminRequest = false)
        {
            if (string.IsNullOrEmpty(orderNumber) || Equals(userId, 0))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.InvalidData);

            ZnodeOmsOrderDetail orderDetail = GetOrderDetailsByOrderNumber(orderNumber, userId);

            if (IsNull(orderDetail))
                return null;

            OrderModel orderModel = new OrderModel();
            //null check for order detail object.

            ZnodeLogging.LogMessage("OmsOrderDetailsId: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { OmsOrderDetailsId = orderDetail?.OmsOrderDetailsId });
            //Map order detail object to OrderMOdel object.
            orderModel = orderDetail.ToModel<OrderModel>();
            orderModel.BillingAddress.PhoneNumber = orderDetail.BillingPhoneNumber;

            BindOrderLineItemData(orderDetail, orderModel);

            if (IsAdminRequest)
            {
                orderModel.PaymentType = (from paymentRepository in new ZnodeRepository<ZnodePaymentType>().Table.Where(x => x.PaymentTypeId == orderDetail.PaymentTypeId) select paymentRepository.Name)?.FirstOrDefault();
                orderModel.PaymentStatus = (from paymentStateRepository in new ZnodeRepository<ZnodeOmsPaymentState>().Table.Where(x => x.OmsPaymentStateId == orderDetail.OmsPaymentStateId) select paymentStateRepository.Name)?.FirstOrDefault();
                orderModel.OrderState = new ZnodeRepository<ZnodeOmsOrderState>().GetById(orderModel.OmsOrderStateId)?.OrderStateName;
            }

            //check for omsOrderDetailsId greater than 0.
            if (orderModel?.OmsOrderDetailsId > 0)
            {
                //Map orderNumber.
                orderModel.OrderNumber = orderNumber;

                //Get ShoppingCart By OrderId.
                orderModel.ShoppingCartModel = GetShoppingCartByOrderId(orderModel.OmsOrderId, orderModel.PortalId, orderModel.UserId);

                //set the amount of dissenter discount applied during order creation.
                GetService<IOrderService>().SetOrderDiscount(orderModel);
            }
            return orderModel;
        }

        //Bind order line item data
        protected virtual void BindOrderLineItemData(ZnodeOmsOrderDetail orderDetail, OrderModel orderModel)
        {
            if (IsNull(orderDetail) || IsNull(orderModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ModelNotNull);

            List<ZnodeOmsOrderLineItem> orderLineItems = GetService<IZnodeOrderHelper>()?.GetOrderLineItemByOrderId(orderDetail.OmsOrderDetailsId)?.ToList();
            orderModel.OrderLineItems = orderLineItems.Where(m => m.OrderLineItemRelationshipTypeId != Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Bundles))?
                                            .ToModel<OrderLineItemModel>()?.ToList();
            if (orderModel.OrderLineItems?.Count > 0)
            {
                orderModel.OrderLineItems.ForEach(x =>
                {
                    x.Description = orderLineItems.FirstOrDefault(m => m.OrderLineItemRelationshipTypeId == Convert.ToInt16(ZnodeCartItemRelationshipTypeEnum.AddOns)
                                                   && m.ParentOmsOrderLineItemsId == x.OmsOrderLineItemsId)?.Description ?? x.Description;
                    x.Quantity = Convert.ToDecimal(ServiceHelper.ToInventoryRoundOff(x.Quantity));
                    ZnodeOmsOrderLineItem lineItem = orderLineItems.FirstOrDefault(y => y.OmsOrderLineItemsId == x.OmsOrderLineItemsId);
                    x.Price += orderLineItems
                            .Where(z => z.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.AddOns
                            && z.ParentOmsOrderLineItemsId == x.OmsOrderLineItemsId
                            ).Sum(z => z.Price);
                    x.OrderWarehouse = orderLineItems.FirstOrDefault(y => y.OmsOrderLineItemsId == x.OmsOrderLineItemsId)?.ZnodeOmsOrderWarehouses.ToModel<OrderWarehouseModel>()?.ToList();
                    if (lineItem.ZnodeOmsOrderAttributes?.Count > 0)
                    {
                        x.Attributes = new List<OrderAttributeModel>();
                        foreach (ZnodeOmsOrderAttribute item in lineItem.ZnodeOmsOrderAttributes)
                            x.Attributes.Add(new OrderAttributeModel { AttributeCode = item.AttributeCode, AttributeValue = item.AttributeValue, AttributeValueCode = item.AttributeValueCode });
                    }
                    ZnodeOmsOrderLineItem parentLineItem = orderLineItems.FirstOrDefault(y => y.OmsOrderLineItemsId == x.ParentOmsOrderLineItemsId);
                    if (IsNotNull(parentLineItem))
                        x.GroupId = parentLineItem.GroupId;
                });

                //Bind Personalised Data
                BindPersonalisedData(orderModel);
            }
        }

        //Get order details as shopping cart by order id.
        protected virtual ShoppingCartModel GetShoppingCartByOrderId(int orderId, int portalId, int userId)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            if (Equals(orderId, 0) || Equals(portalId, 0) || Equals(userId, 0))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.InvalidData);

            ZnodeLogging.LogMessage(" Input parameters OrderId, portalId, userId, portalCatalogId :", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new object[] { orderId, portalId, userId });
            //Get catalog list by portal id.
            int portalCatalogId = GetPortalCatalogId(portalId).GetValueOrDefault();

            IShoppingCartService _shoppingCartService = GetService<IShoppingCartService>();

            //Get shopping cart model by using orderId.
            ShoppingCartModel shoppingCart = _shoppingCartService.GetShoppingCart(new CartParameterModel
            {
                LocaleId = GetLocaleIdFromHeader(),
                PortalId = portalId,
                UserId = userId,
                PublishedCatalogId = portalCatalogId > 0 ? portalCatalogId : 0,
                OmsOrderId = orderId
            });
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return shoppingCart;
        }

        //Valid order line items for return
        protected virtual void ValidOrderLineItemsForReturn(OrderModel orderModel)
        {
            //Set only shipped line items
            if (IsNotNull(orderModel) && orderModel?.OrderLineItems?.Count > 0)
            {
                orderModel.OrderLineItems = orderModel?.OrderLineItems?.Where(x => string.Equals(x.OrderLineItemState, ZnodeOrderStatusEnum.SHIPPED.ToString(), StringComparison.InvariantCultureIgnoreCase) && x.Quantity > 0)?.ToList();
            }

            if (IsNotNull(orderModel) && orderModel?.ShoppingCartModel?.ShoppingCartItems?.Count > 0)
            {
                orderModel.ShoppingCartModel.ShoppingCartItems = orderModel?.ShoppingCartModel?.ShoppingCartItems?.Where(x => string.Equals(x.OrderLineItemStatus, ZnodeOrderStatusEnum.SHIPPED.ToString(), StringComparison.InvariantCultureIgnoreCase) && x.Quantity > 0)?.ToList();
                if (orderModel?.ShoppingCartModel?.ShoppingCartItems?.Count > 0)
                {
                    List<RMAReturnLineItemModel> returnedQuantityList = GetReturnedQuantityForOrderReturn(orderModel.OrderNumber);
                    if (returnedQuantityList?.Count > 0)
                    {
                        foreach (ShoppingCartItemModel shoppingCartItem in orderModel.ShoppingCartModel.ShoppingCartItems)
                        {
                            decimal returnedQuantity = returnedQuantityList?.FirstOrDefault(x => x.OmsOrderLineItemsId == shoppingCartItem.OmsOrderLineItemsId)?.ReturnedQuantity ?? 0;
                            if (returnedQuantity > 0)
                            {
                                shoppingCartItem.Quantity = shoppingCartItem.Quantity - returnedQuantity;
                                foreach (AssociatedProductModel associatedProduct in shoppingCartItem?.GroupProducts)
                                {
                                    associatedProduct.Quantity = associatedProduct.Quantity - returnedQuantity;
                                }
                            }
                        }
                    }
                }
            }
        }

        //Get return list by SP.
        protected virtual IList<RMAReturnModel> GetReturnList(PageListModel pageListModel, bool isAdminRequest, string returnDateRange)
        {
            if (IsNull(pageListModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ModelNotNull);

            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("pagelist model to get return list:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { pageListModel = pageListModel?.ToDebugString() });
            IZnodeViewRepository<RMAReturnModel> objStoredProc = new ZnodeViewRepository<RMAReturnModel>();
            objStoredProc.SetParameter("@WhereClause", pageListModel.SPWhereClause, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@Order_By", pageListModel.OrderBy, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@RowCount", pageListModel.TotalRowCount, ParameterDirection.Output, DbType.Int32);
            objStoredProc.SetParameter("@IsAdmin", isAdminRequest, ParameterDirection.Input, DbType.Boolean);
            objStoredProc.SetParameter("@ReturnDate", returnDateRange, ParameterDirection.Input, DbType.String);

            return objStoredProc.ExecuteStoredProcedureList("Znode_GetRmaReturnHistoryList @WhereClause, @Rows,@PageNo,@Order_By,@IsAdmin,@RowCount OUT,@ReturnDate", 4, out pageListModel.TotalRowCount);
        }

        //Get warehouse details by portal id
        protected virtual WarehouseModel GetReturnWarehouseDetails(int portalId)
        {
            WarehouseModel warehouseDetails = new WarehouseModel();
            if (portalId > 0)
            {
                int warehouseId = new ZnodeRepository<ZnodePortalWarehouse>().Table.FirstOrDefault(x => x.PortalId == portalId)?.WarehouseId ?? 0;
                ZnodeLogging.LogMessage("warehouseId:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { warehouseId = warehouseId });
                if (warehouseId > 0)
                {
                    IWarehouseService _warehouseService = GetService<IWarehouseService>();
                    warehouseDetails = _warehouseService.GetWarehouse(warehouseId);
                }
            }
            return warehouseDetails;
        }

        //Get customer address details
        protected virtual AddressModel GetCustomerAddressDetails(int? addressId)
        {
            if (IsNotNull(addressId) && addressId > 0)
                return new ZnodeRepository<ZnodeAddress>().Table.FirstOrDefault(x => x.AddressId == addressId)?.ToModel<AddressModel>();
            else
                return null;
        }

        //Get shipping address HTML
        protected virtual string GetReturnShippingAddressHTML(AddressModel address, string addressName)
        {
            if (IsNotNull(address))
            {
                string street1 = string.IsNullOrEmpty(address.Address2) ? string.Empty : "<br />" + address.Address2;
                return $"{addressName}{"<br />"}{address.Address1}{street1}{"<br />"}{ address.CityName}{"<br />"}{(string.IsNullOrEmpty(address.StateCode) ? address.StateName : address.StateCode)}{"<br />"}{address.PostalCode}{"<br />"}{address.CountryName}{"<br />"}{Admin_Resources.LabelPhoneNumber}{" : "}{address.PhoneNumber}";
            }
            return string.Empty;
        }

        //Get return line items list
        protected virtual List<RMAReturnLineItemModel> GetReturnLineItems(int rmaReturnDetailsId, bool isBindPersonaliseData = false)
        {
            List<RMAReturnLineItemModel> returnLineItemsList = new List<RMAReturnLineItemModel>();
            if (rmaReturnDetailsId > 0)
            {
                List<ZnodeRmaReturnState> returnStates = _returnStateRepository.Table.ToList();
                returnLineItemsList = _returnLineItemsRepository.Table.Where(x => x.RmaReturnDetailsId == rmaReturnDetailsId)?.ToModel<RMAReturnLineItemModel>()?.ToList();
                List<ZnodeOmsPersonalizeItem> personalizeList = null;
                List<OrderLineItemModel> orderLineItemsList = null;
                if (isBindPersonaliseData)
                {
                    List<int> orderLineItemsIds = returnLineItemsList?.Select(x => x.OmsOrderLineItemsId)?.ToList();
                    if (orderLineItemsIds?.Count > 0)
                    {
                        orderLineItemsList = new ZnodeRepository<ZnodeOmsOrderLineItem>().Table.Where(x => orderLineItemsIds.Contains(x.OmsOrderLineItemsId))?.ToModel<OrderLineItemModel>()?.ToList();
                        personalizeList = GetPersonalisedValueOrderLineItemList(orderLineItemsList);
                    }
                }
                IZnodeOrderHelper orderHelper = GetService<IZnodeOrderHelper>();
                returnLineItemsList?.ForEach(returnLineItem =>
                {
                    returnLineItem.ReturnStatus = returnStates?.FirstOrDefault(y => y.RmaReturnStateId == returnLineItem?.RmaReturnStateId)?.ReturnStateName;
                    if (personalizeList?.Count > 0)
                    {
                        //get personalise attributes by omsorderlineitemid
                        returnLineItem.PersonaliseValueList = GetPersonalisedValueOrderLineItem(orderLineItemsList?.FirstOrDefault(x => x.OmsOrderLineItemsId == returnLineItem.OmsOrderLineItemsId)?.ParentOmsOrderLineItemsId ?? returnLineItem.OmsOrderLineItemsId, personalizeList);
                        returnLineItem.PersonaliseValuesDetail = orderHelper.GetPersonalizedAttributeLineItemDetails(returnLineItem.PersonaliseValueList, string.Empty);
                    }
                });
            }
            return returnLineItemsList;
        }

        //Get return notes list
        protected virtual List<RMAReturnNotesModel> GetReturnNotes(RMAReturnModel returnModel)
        {
            List<RMAReturnNotesModel> returnNotesList = new List<RMAReturnNotesModel>();
            if (IsNotNull(returnModel))
            {
                returnNotesList = _returnNoteRepository.Table.Where(x => x.RmaReturnDetailsId == returnModel.RmaReturnDetailsId)?.ToModel<RMAReturnNotesModel>()?.ToList();
                if (IsNotNull(returnNotesList) && returnNotesList?.Count > 0)
                {
                    List<int> userIds = returnNotesList?.Where(x => x.CreatedBy != returnModel.UserId)?.Select(x => x.CreatedBy)?.ToList();
                    List<ZnodeUser> znodeUserList = null;
                    if (userIds?.Count > 0)
                        znodeUserList = new ZnodeRepository<ZnodeUser>().Table.Where(x => userIds.Contains(x.UserId))?.ToList();
                    foreach (RMAReturnNotesModel returnNotes in returnNotesList)
                    {
                        if (returnNotes.CreatedBy == returnModel.UserId)
                            returnNotes.UserEmailId = returnModel.EmailId;
                        else
                            returnNotes.UserEmailId = znodeUserList?.FirstOrDefault(x => x.UserId == returnNotes.CreatedBy)?.Email;
                    }

                }
            }
            return returnNotesList;
        }

        //Save data for order return.
        protected virtual RMAReturnModel InsertOrderReturn(RMAReturnModel returnModel)
        {
            ZnodeLogging.LogMessage("Execution started", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            if (IsNull(returnModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorModelNull);

            //Get generated unique order number on basis of current date.           
            ParameterModel portalId = new ParameterModel() { Ids = Convert.ToString(returnModel.PortalId) };

            returnModel.ReturnNumber = !string.IsNullOrEmpty(returnModel.ReturnNumber) ? returnModel.ReturnNumber : GenerateReturnNumber(returnModel, portalId);

            ZnodeLogging.LogMessage("Generated return number: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { GenerateReturnNumber = returnModel?.ReturnNumber });

            //Bind Return Details Data
            BindReturnDetailsData(returnModel);

            //start transaction
            using (SqlConnection connection = new SqlConnection(HelperMethods.ConnectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();// Start a local transaction.

                try
                {
                    //Save return details
                    ZnodeRmaReturnDetail znodeRmaReturnDetail = _returnDetailsRepository.Insert(returnModel?.ToEntity<ZnodeRmaReturnDetail>());
                    ZnodeLogging.LogMessage(znodeRmaReturnDetail?.RmaReturnDetailsId > 0 ? Admin_Resources.RecordCreationSuccessMessage : Admin_Resources.ErrorFailedToCreate, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

                    if (znodeRmaReturnDetail?.RmaReturnDetailsId > 0)
                    {
                        returnModel.RmaReturnDetailsId = znodeRmaReturnDetail.RmaReturnDetailsId;

                        //Save return line item data
                        SaveOrderReturnLineItem(returnModel.RmaReturnDetailsId, returnModel.ReturnLineItems);

                        //Save additional notes for return.
                        if (!string.IsNullOrEmpty(returnModel?.Notes))
                        {
                            SaveReturnNotes(new RMAReturnNotesModel()
                            {
                                RmaReturnDetailsId = returnModel.RmaReturnDetailsId,
                                Notes = returnModel.Notes,
                                CreatedBy = returnModel.CreatedBy,
                                CreatedDate = returnModel.CreatedDate,
                                ModifiedBy = returnModel.ModifiedBy,
                                ModifiedDate = returnModel.ModifiedDate
                            });
                        }
                    }
                    transaction.Commit();
                    //Send return request notification email to customer and admin
                    if (returnModel.IsSubmitReturn)
                        SendReturnRequestEmailNotificationAsync(returnModel);

                    ZnodeLogging.LogMessage("Execution done", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                    return new RMAReturnModel() { ReturnNumber = returnModel.ReturnNumber, RmaReturnDetailsId = returnModel.RmaReturnDetailsId };
                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                    transaction.Rollback();
                    if (returnModel.RmaReturnDetailsId > 0)
                        DeleteOrderReturn(returnModel.RmaReturnDetailsId);
                    throw new ZnodeException(ErrorCodes.ExceptionalError, ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        //Bind user data
        protected virtual void BindUserData(RMAReturnModel returnModel)
        {
            if (IsNull(returnModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ModelNotNull);

            UserModel userModel = GetUserNameByUserId(returnModel.UserId);
            returnModel.FirstName = userModel?.FirstName;
            returnModel.LastName = userModel?.LastName;
            returnModel.EmailId = userModel?.Email;
        }

        //Save return line item data
        protected virtual void SaveOrderReturnLineItem(int rmaReturnDetailsId, List<RMAReturnLineItemModel> returnLineItems)
        {
            if (IsNotNull(returnLineItems) && returnLineItems.Count > 0)
            {
                returnLineItems.ForEach(x => x.RmaReturnDetailsId = rmaReturnDetailsId);
                _returnLineItemsRepository.Insert(returnLineItems?.ToEntity<ZnodeRmaReturnLineItem>()?.ToList());
            }
        }

        //Update return order
        protected virtual RMAReturnModel UpdateOrderReturn(RMAReturnModel returnModel)
        {
            if (IsNull(returnModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorModelNull);

            ZnodeLogging.LogMessage("Execution started", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            List<int> deleteReturnLineItemIds = returnModel.ReturnLineItems.Where(x => x.RmaReturnLineItemsId > 0 && x.ExpectedReturnQuantity == 0)?.Select(x => x.RmaReturnLineItemsId)?.ToList();
            returnModel.ReturnLineItems.RemoveAll(x => x.ExpectedReturnQuantity == 0);

            RMAReturnModel updateReturnModel = BindReturnDetailsDataForUpdate(returnModel);

            //start transaction
            using (SqlConnection connection = new SqlConnection(HelperMethods.ConnectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();// Start a local transaction.

                try
                {
                    //Save return line item data
                    List<RMAReturnLineItemModel> insertReturnLineItemsList = updateReturnModel.ReturnLineItems.Where(x => x.RmaReturnLineItemsId == 0 && x.ExpectedReturnQuantity > 0)?.ToList();
                    SaveOrderReturnLineItem(updateReturnModel.RmaReturnDetailsId, insertReturnLineItemsList);

                    //Update return line item data
                    List<RMAReturnLineItemModel> updateReturnLineItemsList = updateReturnModel.ReturnLineItems.Where(x => x.RmaReturnLineItemsId > 0 && x.ExpectedReturnQuantity > 0)?.ToList();
                    updateReturnLineItemsList.ForEach(x => x.RmaReturnDetailsId = updateReturnModel.RmaReturnDetailsId);
                    UpdateReturnLineItems(updateReturnLineItemsList, returnModel.IsSubmitReturn);

                    //Delete return line item data
                    if (deleteReturnLineItemIds.Count > 0)
                        DeleteReturnLineItems(new ParameterModel() { Ids = String.Join(",", deleteReturnLineItemIds) });

                    bool isUpdateReturnDetails = _returnDetailsRepository.Update(updateReturnModel?.ToEntity<ZnodeRmaReturnDetail>());
                    ZnodeLogging.LogMessage("Return details updated:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { isUpdateReturnDetails = isUpdateReturnDetails });

                    InsertUpdateReturnNotes(updateReturnModel.Notes, updateReturnModel.RmaReturnDetailsId, updateReturnModel.UserId);

                    transaction.Commit();

                    //Send return request notification email to customer and admin
                    if (returnModel.IsSubmitReturn)
                        SendReturnRequestEmailNotificationAsync(returnModel);

                    ZnodeLogging.LogMessage("Execution done", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                    return new RMAReturnModel() { ReturnNumber = updateReturnModel.ReturnNumber, RmaReturnDetailsId = updateReturnModel.RmaReturnDetailsId };
                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                    transaction.Rollback();
                    throw new ZnodeException(ErrorCodes.ExceptionalError, ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        //Bind return details data for update
        protected virtual RMAReturnModel BindReturnDetailsDataForUpdate(RMAReturnModel returnModel)
        {
            if (IsNull(returnModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ModelNotNull);

            ZnodeRmaReturnDetail znodeReturnDetails = _returnDetailsRepository.Table.FirstOrDefault(x => x.ReturnNumber.ToLower() == returnModel.ReturnNumber.ToLower() && x.UserId == returnModel.UserId);
            if (IsNotNull(znodeReturnDetails))
            {
                RMAReturnModel updateReturnDetails = znodeReturnDetails.ToModel<RMAReturnModel>();

                updateReturnDetails.TotalExpectedReturnQuantity = returnModel.ReturnLineItems.Sum(x => x.ExpectedReturnQuantity);
                DateTime returnDateTime = GetDateWithTime();
                if (returnModel.IsSubmitReturn)
                    updateReturnDetails.ReturnDate = returnDateTime;
                else
                    updateReturnDetails.ReturnDate = null;

                updateReturnDetails.RmaReturnStateId = returnModel.IsSubmitReturn ? (int)ZnodeReturnStateEnum.SUBMITTED : (int)ZnodeReturnStateEnum.NOT_SUBMITTED;
                updateReturnDetails.ReturnStatus = returnModel.IsSubmitReturn ? ZnodeConstant.ReturnStateSubmitted : ZnodeConstant.ReturnStateNotSubmitted;
                updateReturnDetails.ModifiedDate = returnDateTime;
                updateReturnDetails.ModifiedBy = returnModel.UserId;
                updateReturnDetails.Notes = returnModel.Notes;

                //Bind Order Return Line Item Data
                BindOrderReturnLineItemData(returnModel, GetShoppingCartDetails(updateReturnDetails.UserId, updateReturnDetails.PortalId, updateReturnDetails.OmsOrderId));
                updateReturnDetails.ReturnLineItems = returnModel.ReturnLineItems;

                //Bind Order Summary
                BindOrderReturnSummary(updateReturnDetails);
                return updateReturnDetails;
            }
            else
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.InvalidData);
        }

        //Update return line items
        protected virtual bool UpdateReturnLineItems(List<RMAReturnLineItemModel> updateReturnLineItems, bool isSubmit)
        {
            if (IsNotNull(updateReturnLineItems) && updateReturnLineItems.Count > 0)
            {
                if (isSubmit)
                    return _returnLineItemsRepository.BatchUpdate(updateReturnLineItems.ToEntity<ZnodeRmaReturnLineItem>()?.ToList());
                else
                {
                    List<RMAReturnLineItemModel> updateReturnLineItemsList = new List<RMAReturnLineItemModel>();
                    List<int> updateReturnLineItemIds = updateReturnLineItems.Where(y => IsNotNull(y)).Select(x => x.RmaReturnLineItemsId)?.ToList();
                    List<ZnodeRmaReturnLineItem> znodeReturnLineItemList = _returnLineItemsRepository.Table.Where(x => updateReturnLineItemIds.Contains(x.RmaReturnLineItemsId))?.ToList();

                    foreach (RMAReturnLineItemModel returnlineitem in updateReturnLineItems)
                    {
                        ZnodeRmaReturnLineItem znodeReturnLineItem = znodeReturnLineItemList.FirstOrDefault(x => x.RmaReturnLineItemsId == returnlineitem.RmaReturnLineItemsId);
                        if (returnlineitem.ExpectedReturnQuantity != znodeReturnLineItem.ExpectedReturnQuantity || returnlineitem.RmaReasonForReturnId != znodeReturnLineItem.RmaReasonForReturnId)
                            updateReturnLineItemsList.Add(returnlineitem);
                    }

                    if (updateReturnLineItemsList.Count > 0)
                        return _returnLineItemsRepository.BatchUpdate(updateReturnLineItemsList.ToEntity<ZnodeRmaReturnLineItem>()?.ToList());
                }
            }
            return false;
        }

        //Delete return line items
        protected virtual bool DeleteReturnLineItems(ParameterModel returnLineItemIds)
        {
            if (IsNotNull(returnLineItemIds) || !string.IsNullOrEmpty(returnLineItemIds.Ids))
            {
                FilterCollection filters = new FilterCollection { new FilterTuple(ZnodeRmaReturnLineItemEnum.RmaReturnLineItemsId.ToString(), ProcedureFilterOperators.In, returnLineItemIds.Ids) };
                EntityWhereClauseModel whereClauseForDelete = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection());
                return _returnLineItemsRepository.Delete(whereClauseForDelete.WhereClause);
            }
            return false;
        }

        //Get order details by ordernumber and userid
        protected virtual ZnodeOmsOrderDetail GetOrderDetailsByOrderNumber(string orderNumber, int userId)
        {
            if (!string.IsNullOrEmpty(orderNumber) && userId > 0)
            {
                IZnodeRepository<ZnodeOmsOrder> _omsOrderRepository = new ZnodeRepository<ZnodeOmsOrder>();
                IZnodeRepository<ZnodeOmsOrderDetail> _orderDetailsRepository = new ZnodeRepository<ZnodeOmsOrderDetail>();
                return (from znodeorderDetails in _orderDetailsRepository.Table
                        join znodeOrder in _omsOrderRepository.Table on znodeorderDetails.OmsOrderId equals znodeOrder.OmsOrderId
                        where znodeOrder.OrderNumber.ToLower() == orderNumber.ToLower() && znodeorderDetails.IsActive && znodeorderDetails.UserId == userId
                        select znodeorderDetails)?.FirstOrDefault();
            }
            else
                return null;
        }

        //Validate calculation order return data
        protected virtual bool ValidateCalculationOrderReturn(List<RMAReturnCalculateLineItemModel> returnCalulateLineItemModelList)
        {
            bool status = true;
            foreach (RMAReturnCalculateLineItemModel returnCalculateLineItemModel in returnCalulateLineItemModelList)
            {
                if (returnCalculateLineItemModel.ExpectedReturnQuantity < 1)
                {
                    status = false;
                    returnCalculateLineItemModel.HasError = true;
                    returnCalculateLineItemModel.ErrorMessage = Admin_Resources.ErrorReturnQuantity;
                    break;
                }
                else if (returnCalculateLineItemModel.ExpectedReturnQuantity > returnCalculateLineItemModel.ShippedQuantity)
                {
                    status = false;
                    returnCalculateLineItemModel.HasError = true;
                    returnCalculateLineItemModel.ErrorMessage = Admin_Resources.ErrorReturnQuantityLessThanMessage;
                    break;
                }
            }
            ZnodeLogging.LogMessage("Validate calculation order return data status", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { status = status });
            return status;
        }

        //Perform calculation for an order return line item
        protected virtual void PerformCalculationForOrderReturnLineItem(RMAReturnCalculateModel returnCalculateModel, ShoppingCartModel shoppingCartModel)
        {
            if (IsNotNull(shoppingCartModel) && shoppingCartModel?.ShoppingCartItems?.Count > 0)
            {
                foreach (RMAReturnCalculateLineItemModel returnCalulateLineItem in returnCalculateModel.ReturnCalculateLineItemList)
                {
                    ShoppingCartItemModel shoppingCartItem = shoppingCartModel?.ShoppingCartItems?.FirstOrDefault(x => x.OmsOrderLineItemsId == returnCalulateLineItem.OmsOrderLineItemsId);
                    if (IsNotNull(shoppingCartItem))
                    {
                        returnCalulateLineItem.TotalLineItemPrice = shoppingCartItem.UnitPrice * returnCalulateLineItem.ExpectedReturnQuantity;
                        returnCalulateLineItem.UnitPrice = shoppingCartItem.UnitPrice;
                        returnCalulateLineItem.TaxCost = shoppingCartItem.TaxCost > 0 ? (shoppingCartItem.TaxCost / shoppingCartItem.Quantity) * returnCalulateLineItem.ExpectedReturnQuantity : shoppingCartItem.TaxCost;
                    }
                }
                returnCalculateModel.ReturnSubTotal = returnCalculateModel.ReturnCalculateLineItemList.Sum(x => x.TotalLineItemPrice);
                returnCalculateModel.ReturnTaxCost = returnCalculateModel.ReturnCalculateLineItemList.Sum(x => x.TaxCost);             
                returnCalculateModel.ReturnTotal = (returnCalculateModel.ReturnSubTotal + returnCalculateModel.ReturnTaxCost);
            }
        }

        //Validate order return data
        protected virtual bool ValidateOrderReturn(List<RMAReturnLineItemModel> returnLineItemList, string actionMode)
        {
            bool status = true;
            if (returnLineItemList?.Count > 0)
            {
                foreach (RMAReturnLineItemModel returnLineItem in returnLineItemList)
                {
                    if (returnLineItem.ExpectedReturnQuantity > returnLineItem.ShippedQuantity)
                    {
                        status = false;
                        returnLineItem.HasError = true;
                        returnLineItem.ErrorMessage = Admin_Resources.ErrorReturnQuantityLessThanMessage;
                    }
                    else if (actionMode == ZnodeConstant.Create && returnLineItem.ExpectedReturnQuantity < 1)
                    {
                        status = false;
                        returnLineItem.HasError = true;
                        returnLineItem.ErrorMessage = Admin_Resources.ErrorReturnQuantity;

                    }
                    else if (actionMode == ZnodeConstant.Edit && returnLineItem.RmaReturnLineItemsId < 1 && returnLineItem.ExpectedReturnQuantity < 1)
                    {
                        status = false;
                        returnLineItem.HasError = true;
                        returnLineItem.ErrorMessage = Admin_Resources.ErrorReturnQuantity;
                    }
                }
            }
            else
                status = false;

            ZnodeLogging.LogMessage("Validate order return data status", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { status = status });
            return status;
        }

        //Bind Return Details Data
        protected virtual void BindReturnDetailsData(RMAReturnModel returnModel)
        {
            if (IsNull(returnModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ModelNotNull);

            ZnodeOmsOrderDetail orderDetail = GetOrderDetailsByOrderNumber(returnModel.OrderNumber, returnModel.UserId);
            if (IsNotNull(orderDetail))
            {
                returnModel.OmsOrderId = orderDetail.OmsOrderId;
                returnModel.OmsOrderDetailsId = orderDetail.OmsOrderDetailsId;

                DateTime returnDateTime = GetDateWithTime();

                if (returnModel.IsSubmitReturn)
                    returnModel.ReturnDate = returnDateTime;
                else
                    returnModel.ReturnDate = null;

                returnModel.RmaReturnStateId = returnModel.IsSubmitReturn ? (int)ZnodeReturnStateEnum.SUBMITTED : (int)ZnodeReturnStateEnum.NOT_SUBMITTED;
                returnModel.ReturnStatus = returnModel.IsSubmitReturn ? ZnodeConstant.ReturnStateSubmitted : ZnodeConstant.ReturnStateNotSubmitted;
                returnModel.TotalExpectedReturnQuantity = returnModel.ReturnLineItems.Sum(x => x.ExpectedReturnQuantity);
                returnModel.AddressId = orderDetail.AddressId;
                returnModel.ShippingId = orderDetail.ShippingId;
                returnModel.ShippingNumber = orderDetail.ShippingNumber;
                returnModel.IsTaxCostEdited = (bool)orderDetail.IsTaxCostEdited;
                returnModel.IsActive = true;
                returnModel.CurrencyCode = orderDetail.CurrencyCode;
                returnModel.CultureCode = orderDetail.CultureCode;
                returnModel.ModifiedDate = returnDateTime;
                returnModel.ModifiedBy = returnModel.UserId;

                //Bind User Data
                BindUserData(returnModel);
                //BindOrderReturnLineItemData
                BindOrderReturnLineItemData(returnModel, GetShoppingCartDetails(returnModel.UserId, returnModel.PortalId, returnModel.OmsOrderId));
                //Bind Order Return Summary
                BindOrderReturnSummary(returnModel);
            }
        }

        //Get shopping cart details
        protected virtual ShoppingCartModel GetShoppingCartDetails(int userId, int portalId, int orderId)
        {
            if (Equals(userId, 0) || Equals(portalId, 0) || Equals(orderId, 0))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.InvalidData);

            //Get catalog list by portal id.
            int portalCatalogId = (new ZnodeRepository<ZnodePortalCatalog>().Table.FirstOrDefault(x => x.PortalId == portalId)?.PublishCatalogId).GetValueOrDefault();
            CartParameterModel cartParameterModel = new CartParameterModel
            {
                LocaleId = GetLocaleIdFromHeader(),
                PortalId = portalId,
                UserId = userId,
                PublishedCatalogId = portalCatalogId > 0 ? portalCatalogId : 0,
                OmsOrderId = orderId
            };
            return GetService<IZnodeShoppingCart>()?.LoadCartFromOrder(cartParameterModel, GetCatalogVersionId(cartParameterModel.PublishedCatalogId));
        }

        //Bind Order Return Line Item Data
        protected virtual void BindOrderReturnLineItemData(RMAReturnModel returnModel, ShoppingCartModel shoppingCartModel)
        {
            if (IsNotNull(returnModel) && IsNotNull(shoppingCartModel) && shoppingCartModel?.ShoppingCartItems?.Count > 0)
            {
                foreach (RMAReturnLineItemModel returnLineItem in returnModel?.ReturnLineItems)
                {
                    ShoppingCartItemModel shoppingCartItem = shoppingCartModel?.ShoppingCartItems?.FirstOrDefault(x => x.OmsOrderLineItemsId == returnLineItem.OmsOrderLineItemsId);
                    if (IsNotNull(shoppingCartItem))
                    {
                        returnLineItem.OmsOrderLineItemsId = shoppingCartItem.OmsOrderLineItemsId;
                        if (IsNull(shoppingCartItem.OrderLineItemRelationshipTypeId) && IsNotNull(shoppingCartItem.BundleProductSKUs))
                            returnLineItem.OrderLineItemRelationshipTypeId = (int)ZnodeCartItemRelationshipTypeEnum.Bundles;
                        else
                            returnLineItem.OrderLineItemRelationshipTypeId = (int)shoppingCartItem.OrderLineItemRelationshipTypeId;

                        if (IsNotNull(shoppingCartItem.OrderLineItemRelationshipTypeId) && shoppingCartItem.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.Group)
                        {
                            returnLineItem.Sku = shoppingCartItem?.GroupProducts.FirstOrDefault().Sku;
                            returnLineItem.Description = shoppingCartItem?.GroupProducts?.FirstOrDefault().ProductName;
                        }
                        else
                        {
                            returnLineItem.Sku = shoppingCartItem.SKU;
                            returnLineItem.Description = shoppingCartItem.Description;
                        }
                        returnLineItem.ProductName = shoppingCartItem.ProductName;
                        returnLineItem.Price = shoppingCartItem.UnitPrice;
                        returnLineItem.DiscountAmount = shoppingCartItem.ProductDiscountAmount > 0 ? (shoppingCartItem.ProductDiscountAmount / shoppingCartItem.Quantity) * returnLineItem.ExpectedReturnQuantity : shoppingCartItem.ProductDiscountAmount;
                        returnLineItem.ShipSeparately = shoppingCartItem.ShipSeperately;
                        returnLineItem.ReturnDate = GetDateWithTime();
                        returnLineItem.RmaReturnStateId = returnModel.IsSubmitReturn ? (int)ZnodeReturnStateEnum.SUBMITTED : (int)ZnodeReturnStateEnum.NOT_SUBMITTED;
                        returnModel.ModifiedDate = GetDateWithTime();
                        returnModel.ModifiedBy = returnModel.UserId;
                        returnLineItem.IsActive = true;
                        returnLineItem.TaxCost = shoppingCartItem.TaxCost > 0 ? (shoppingCartItem.TaxCost / shoppingCartItem.Quantity) * returnLineItem.ExpectedReturnQuantity : shoppingCartItem.TaxCost;
                    }
                }
            }
        }

        //Bind Order Return Summary
        protected virtual void BindOrderReturnSummary(RMAReturnModel returnModel, bool isAdminRequest = false)
        {
            if (IsNotNull(returnModel) && IsNotNull(returnModel?.ReturnLineItems))
            {
                returnModel.SubTotal = !isAdminRequest ? returnModel.ReturnLineItems.Sum(x => x.ExpectedReturnQuantity * x.Price) : (decimal)returnModel.ReturnLineItems.Sum(x => x.ReturnedQuantity * x.Price);
                returnModel.ReturnShippingCost = returnModel.ReturnLineItems.Sum(x => x.ShippingCost);
                returnModel.DiscountAmount = returnModel.ReturnLineItems.Sum(x => x.DiscountAmount) + (decimal)returnModel.ReturnLineItems.Sum(x => x.RefundAmount);
                returnModel.ReturnTaxCost = returnModel.ReturnLineItems.Sum(x => x.TaxCost);
                returnModel.TotalReturnAmount = (returnModel.SubTotal + returnModel.ReturnTaxCost + (decimal)returnModel.ReturnShippingCost) - returnModel.DiscountAmount;
            }
        }

        //Get Returned Quantity For Order Return
        protected virtual List<RMAReturnLineItemModel> GetReturnedQuantityForOrderReturn(string orderNumber)
        {
            List<RMAReturnLineItemModel> returnedQuantityList = (from returnDetail in _returnDetailsRepository.Table
                                                                 join returnLineItem in _returnLineItemsRepository.Table
                                                                 on returnDetail.RmaReturnDetailsId equals returnLineItem.RmaReturnDetailsId
                                                                 where returnDetail.OrderNumber == orderNumber
                                                                 && returnDetail.RmaReturnStateId != (int)ZnodeReturnStateEnum.APPROVED
                                                                 && returnDetail.RmaReturnStateId != (int)ZnodeReturnStateEnum.PARTIALLY_APPROVED
                                                                 && returnDetail.RmaReturnStateId != (int)ZnodeReturnStateEnum.REFUND_PROCESSED
                                                                 && returnDetail.RmaReturnStateId != (int)ZnodeReturnStateEnum.REJECTED
                                                                 group returnLineItem by returnLineItem.OmsOrderLineItemsId into returnLineItemGroup
                                                                 select new RMAReturnLineItemModel
                                                                 {
                                                                     OmsOrderLineItemsId = returnLineItemGroup.Key,
                                                                     ReturnedQuantity = returnLineItemGroup.Sum(x => x.ExpectedReturnQuantity),
                                                                 })?.ToList();

            return returnedQuantityList;
        }

        //Get product image path for return line items
        protected virtual List<RMAReturnLineItemModel> GetProductImagePath(List<RMAReturnLineItemModel> returnLineItems, int portalId)
        {
            if (IsNotNull(returnLineItems) && returnLineItems?.Count > 0 && portalId > 0)
            {
                int publishCatalogId = GetPortalCatalogId(portalId).GetValueOrDefault();
                int localeId = GetDefaultLocaleId();
                List<PublishedProductEntityModel> productList = GetService<IPublishedProductDataService>().GetPublishProductBySKUs(returnLineItems?.Select(x => x.Sku)?.ToList(), publishCatalogId, localeId, GetCatalogVersionId(publishCatalogId, localeId, portalId))?.ToModel<PublishedProductEntityModel>()?.ToList();

                ZnodeLogging.LogMessage("Product list count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { productList = productList?.Count });

                IImageHelper objImage = GetService<IImageHelper>();
                if (IsNotNull(productList) && productList.Count > 0)
                {
                    foreach (RMAReturnLineItemModel returnItem in returnLineItems)
                    {
                        string imagePath = productList.FirstOrDefault(x => x.SKU.ToLower() == returnItem.Sku.ToLower())?.Attributes?.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues;
                        returnItem.ProductImagePath = BindImagePath(imagePath, portalId, objImage);
                    }
                }
            }
            return returnLineItems ?? new List<RMAReturnLineItemModel>();
        }

        //Bind product image full path
        protected virtual string BindImagePath(string imagePath, int portalId, IImageHelper objImage)
        {
            if (IsNull(objImage))
                objImage = GetService<IImageHelper>();

            return GetService<IShoppingCartItemMap>()?.GetImagePath(imagePath, portalId, objImage);
        }

        //Generate barcode image
        protected virtual string GetBarcodeImage(string returnNumber)
        {
            if (IsNotNull(returnNumber))
            {
                BarcodeModel barcodeModel = new BarcodeModel()
                {
                    BarcodeText = returnNumber,
                    FontName = "code128",
                    FontSize = 48,
                    Length = 20,
                    Height = 80,
                    PointX = 2f,
                    PointY = 2f,
                    BarcodeLineColor = Color.Black,
                    BarcodeBackgroundColor = Color.White,
                    BarcodeImageFormat = ImageFormat.Jpeg,
                };
                return GetService<IBarcodeHelper>().GenerateBarcode(barcodeModel);
            }
            else
            {
                return null;
            }
        }

        //Insert Update Return Notes
        protected virtual void InsertUpdateReturnNotes(string notes, int returnDetailsId, int userId)
        {
            ZnodeLogging.LogMessage("Input parameters:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { notes = notes, returnDetailsId = returnDetailsId, userId = userId });
            if (returnDetailsId > 0 && userId > 0)
            {
                int returnNotesId = _returnNoteRepository.Table.FirstOrDefault(x => x.RmaReturnDetailsId == returnDetailsId && x.CreatedBy == userId)?.RmaReturnNotesId ?? 0;

                RMAReturnNotesModel returnNotesModel = new RMAReturnNotesModel()
                {
                    RmaReturnDetailsId = returnDetailsId,
                    Notes = notes,
                    ModifiedBy = userId,
                    ModifiedDate = GetDateWithTime()
                };

                if (returnNotesId > 0 && !string.IsNullOrEmpty(notes))
                {
                    returnNotesModel.RmaReturnNotesId = returnNotesId;
                    UpdateReturnNotes(returnNotesModel);
                }
                else if (returnNotesId < 1 && !string.IsNullOrEmpty(notes))
                    SaveReturnNotes(returnNotesModel);
                else if (returnNotesId > 0 && string.IsNullOrEmpty(notes))
                    DeleteReturnNotes(new ParameterModel() { Ids = returnNotesId.ToString() });
            }
        }

        //Update return notes
        protected virtual bool UpdateReturnNotes(RMAReturnNotesModel rmaReturnNotesModel)
        {
            if (!string.IsNullOrEmpty(rmaReturnNotesModel?.Notes) && rmaReturnNotesModel?.RmaReturnDetailsId > 0)
                return _returnNoteRepository.Update(rmaReturnNotesModel?.ToEntity<ZnodeRmaReturnNote>());
            return false;
        }

        //Delete return notes
        protected virtual bool DeleteReturnNotes(ParameterModel deleteReturnNoteIds)
        {
            if (IsNotNull(deleteReturnNoteIds) || !string.IsNullOrEmpty(deleteReturnNoteIds.Ids))
            {
                FilterCollection filters = new FilterCollection { new FilterTuple(ZnodeRmaReturnNoteEnum.RmaReturnNotesId.ToString(), ProcedureFilterOperators.In, deleteReturnNoteIds.Ids) };
                EntityWhereClauseModel whereClauseForDelete = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection());
                return _returnNoteRepository.Delete(whereClauseForDelete.WhereClause);
            }
            return false;
        }

        //This method will send email notification to customer       
        protected virtual void SendReturnRequestNotificationEmailToCustomer(RMAEmailDetailsModel emailDetails)
        {
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { emailDetails = emailDetails });
            if (IsNotNull(emailDetails))
            {
                EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode(emailDetails.EmailTemplateCode, emailDetails.PortalId);
                PortalModel portalModel = GetCustomPortalDetails(emailDetails.PortalId);
                if (IsNotNull(emailTemplateMapperModel))
                {
                    emailDetails.UserFullName = string.IsNullOrEmpty(emailDetails.UserFullName) ? emailDetails.EmailId : emailDetails.UserFullName;
                    if (!string.IsNullOrEmpty(emailDetails.ReturnStatus))
                    {
                        string webstoreDomain = GetWebstoreDomainByPortalId(emailDetails.PortalId, ZnodeConstant.Webstore);
                        emailDetails.ReturnDetailsUrl = $"{HttpContext.Current.Request.Url.Scheme}://{webstoreDomain}/RMAReturn/GetReturnDetails?returnNumber={emailDetails.ReturnNumber}";
                    }
                    string messageText = GetEmailMessageText(emailDetails, emailTemplateMapperModel.Descriptions, portalModel?.StoreName);

                    emailTemplateMapperModel.Subject = GetEmailMessageText(emailDetails, emailTemplateMapperModel.Subject);

                    ZnodeLogging.LogMessage("Message text to send email notification to customer :", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { messageText = messageText });

                    string bcc = string.Empty;
                    try
                    {
                        if (IsNotNull(portalModel) && emailDetails.PortalId > 0)
                            ZnodeEmail.SendEmail(emailDetails.PortalId, emailDetails.EmailId, portalModel.AdministratorEmail, ZnodeEmail.GetBccEmail(emailTemplateMapperModel.IsEnableBcc, emailDetails.PortalId, bcc), $"{portalModel.StoreName} - {emailTemplateMapperModel.Subject}", messageText, true);
                        else
                            ZnodeEmail.SendEmail(emailDetails.EmailId, ZnodeConfigManager.SiteConfig.AdminEmail, ZnodeEmail.GetBccEmail(emailTemplateMapperModel.IsEnableBcc, emailDetails.PortalId, bcc), $"{ZnodeConfigManager.SiteConfig.StoreName} - {emailTemplateMapperModel.Subject}", messageText, true);
                    }
                    catch (Exception ex)
                    {
                        ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginCreateSuccess, emailDetails.UserFullName, string.Empty, null, ex.Message, null);
                    }
                }
            }
        }

        //Get email message text
        protected virtual string GetEmailMessageText(RMAEmailDetailsModel emailDetails, string emailDescription, string storeName = null)
        {
            string messageText = string.Empty;
            if (IsNotNull(emailDetails) && !string.IsNullOrEmpty(emailDescription))
            {
                messageText = ReplaceTokenWithMessageText("#UserFullName#", emailDetails.UserFullName, emailDescription);
                messageText = ReplaceTokenWithMessageText("#ReturnNumber#", emailDetails.ReturnNumber, messageText);
                messageText = ReplaceTokenWithMessageText("#ReturnStatus#", emailDetails.ReturnStatus, messageText);
                messageText = ReplaceTokenWithMessageText("#StoreName#", storeName, messageText);
                messageText = ReplaceTokenWithMessageText("#Url#", emailDetails.ReturnDetailsUrl, messageText);
                messageText = EmailTemplateHelper.ReplaceTemplateTokens(messageText);
            }
            return messageText;
        }

        //Get domain name on the basis of portal id.
        protected virtual string GetWebstoreDomainByPortalId(int portalId, string applicationType)
        {
            string domainName = string.Empty;
            if (portalId > 0 && !string.IsNullOrEmpty(applicationType))
                domainName = (from znodeDomainRepository in new ZnodeRepository<ZnodeDomain>().Table.Where(x => x.PortalId == portalId && x.ApplicationType.ToLower() == applicationType.ToLower() && x.IsActive && x.IsDefault) select znodeDomainRepository.DomainName)?.FirstOrDefault();
            return domainName;
        }

        //This method will send return order request notification email to admin.        
        protected virtual void SendReturnRequestNotificationEmailToAdmin(string returnNumber, int portalId, string emailTemplateCode)
        {
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { returnNumber = returnNumber, portalId = portalId });

            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode(emailTemplateCode, portalId);
            PortalModel portalModel = GetCustomPortalDetails(portalId);
            if (IsNotNull(emailTemplateMapperModel))
            {
                string messageText = ReplaceTokenWithMessageText("#ReturnNumber#", returnNumber, emailTemplateMapperModel.Descriptions);
                messageText = ReplaceTokenWithMessageText("#StoreName#", portalModel?.StoreName, messageText);
                messageText = EmailTemplateHelper.ReplaceTemplateTokens(messageText);
                ZnodeLogging.LogMessage("Message text for return request notification email to admin:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { messageText = messageText });

                string bcc = string.Empty;
                try
                {
                    if (IsNotNull(portalModel) && portalId > 0)
                        ZnodeEmail.SendEmail(portalId, portalModel?.AdministratorEmail, portalModel?.AdministratorEmail, ZnodeEmail.GetBccEmail(emailTemplateMapperModel.IsEnableBcc, portalId, bcc), $"{portalModel?.StoreName} - {emailTemplateMapperModel?.Subject}", messageText, true);
                    else
                        ZnodeEmail.SendEmail(portalModel?.AdministratorEmail, ZnodeConfigManager.SiteConfig.AdminEmail, ZnodeEmail.GetBccEmail(emailTemplateMapperModel.IsEnableBcc, portalId, bcc), $"{ZnodeConfigManager.SiteConfig.StoreName} - {emailTemplateMapperModel?.Subject}", messageText, true);
                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginCreateSuccess, returnNumber, string.Empty, null, ex.Message, null);
                }
            }
        }

        //Send return email notification
        protected virtual bool SendReturnRequestEmailNotificationAsync(RMAReturnModel returnModel, bool isAdminRequest = false)
        {
            bool emailNotificationStatus = false;
            if (IsNotNull(returnModel))
            {
                System.Web.HttpContext current = System.Web.HttpContext.Current;

                Thread thread = new Thread(new ThreadStart(() =>
                {
                    System.Web.HttpContext.Current = current;
                    try
                    {
                        RMAEmailDetailsModel emailDetails = new RMAEmailDetailsModel()
                        {
                            EmailId = returnModel.EmailId,
                            PortalId = returnModel.PortalId,
                            ReturnNumber = returnModel.ReturnNumber,
                            UserFullName = string.Concat(returnModel.FirstName, " ", returnModel.LastName)
                        };

                        if (isAdminRequest)
                        {
                            emailDetails.ReturnStatus = returnModel.ReturnStatus;
                            if (returnModel.RmaReturnStateId == (int)ZnodeReturnStateEnum.REFUND_PROCESSED)
                            {
                                //refund email notification
                                emailDetails.EmailTemplateCode = ZnodeConstant.EmailTemplateRefundProcessedNotificationForCustomer;
                                SendReturnRequestNotificationEmailToCustomer(emailDetails);
                            }
                            else
                            {
                                //Updated return status email notification
                                emailDetails.EmailTemplateCode = ZnodeConstant.EmailTemplateReturnStatusNotificationForCustomer;
                                SendReturnRequestNotificationEmailToCustomer(emailDetails);
                            }
                        }
                        else
                        {
                            emailDetails.EmailTemplateCode = ZnodeConstant.EmailTemplateReturnRequestNotificationForCustomer;
                            SendReturnRequestNotificationEmailToCustomer(emailDetails);
                            SendReturnRequestNotificationEmailToAdmin(returnModel.ReturnNumber, returnModel.PortalId, ZnodeConstant.EmailTemplateReturnRequestNotificationForAdmin);
                            emailNotificationStatus = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                        emailNotificationStatus = false;
                    }
                }));
                thread.Start();
            }
            ZnodeLogging.LogMessage("Return request email notification status", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { emailNotificationStatus = emailNotificationStatus });
            return emailNotificationStatus;
        }

        //Map return history from history as well as notes.
        protected virtual List<RMAReturnHistoryModel> GetReturnHistoryAndNotes(int rmaReturnDetailsId)
        {
            if (rmaReturnDetailsId > 0)
            {
                IZnodeViewRepository<RMAReturnHistoryModel> objStoredProc = new ZnodeViewRepository<RMAReturnHistoryModel>();
                objStoredProc.SetParameter("@RmaReturnDetailsId", rmaReturnDetailsId, ParameterDirection.Input, DbType.Int32);
                IList<RMAReturnHistoryModel> returnHistoryAndNotesList = objStoredProc.ExecuteStoredProcedureList("Znode_GetReturnHistory @RmaReturnDetailsId");
                ZnodeLogging.LogMessage("Return history list count and RmaReturnDetailsId to get Return history: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { ReturnHistoryListCount = returnHistoryAndNotesList?.Count, RmaReturnDetailsId = rmaReturnDetailsId });
                return returnHistoryAndNotesList?.ToList();
            }
            else
            {
                return new List<RMAReturnHistoryModel>();
            }
        }

        //Get Personalised Value Order Line Item List
        protected virtual List<ZnodeOmsPersonalizeItem> GetPersonalisedValueOrderLineItemList(List<OrderLineItemModel> lineItems)
        {
            List<ZnodeOmsPersonalizeItem> orderPersonlizeItemList = null;
            if (lineItems?.Count > 0)
            {
                List<int?> lineItemIds = lineItems.Select(x => Convert.ToInt32(x.ParentOmsOrderLineItemsId) > 0 ? x.ParentOmsOrderLineItemsId : x.OmsOrderLineItemsId)?.Distinct()?.ToList();
                if (lineItemIds?.Count > 0)
                    orderPersonlizeItemList = new ZnodeRepository<ZnodeOmsPersonalizeItem>().Table.Where(x => lineItemIds.Contains(x.OmsOrderLineItemsId))?.ToList();
            }
            return orderPersonlizeItemList;
        }

        //Get Personalised Value Order Line Item
        protected virtual Dictionary<string, object> GetPersonalisedValueOrderLineItem(int orderLineItemId, List<ZnodeOmsPersonalizeItem> personalizeItems)
        {
            Dictionary<string, object> personaliseItem = new Dictionary<string, object>();
            if (orderLineItemId > 0 && personalizeItems?.Count > 0)
            {
                foreach (KeyValuePair<string, string> personaliseAttr in personalizeItems.Where(x => x.OmsOrderLineItemsId == orderLineItemId)?.ToDictionary(x => x.PersonalizeCode, x => x.PersonalizeValue))
                    personaliseItem.Add(personaliseAttr.Key, (object)personaliseAttr.Value);
            }

            return personaliseItem;
        }

        //Bind Personalised Data
        protected virtual void BindPersonalisedData(OrderModel orderModel)
        {
            //check for omsOrderDetailsId greater than 0.
            if (orderModel?.OmsOrderDetailsId > 0 && IsNotNull(orderModel?.OrderLineItems))
            {
                List<ZnodeOmsPersonalizeItem> personalizeList = GetPersonalisedValueOrderLineItemList(orderModel.OrderLineItems);
                IZnodeOrderHelper orderHelper = GetService<IZnodeOrderHelper>();
                foreach (OrderLineItemModel lineItem in orderModel.OrderLineItems)
                {
                    //get personalise attributes by omsorderlineitemid
                    lineItem.PersonaliseValueList = GetPersonalisedValueOrderLineItem(Convert.ToInt32(lineItem.ParentOmsOrderLineItemsId) > 0 ? Convert.ToInt32(lineItem.ParentOmsOrderLineItemsId) : lineItem.OmsOrderLineItemsId, personalizeList);
                    lineItem.PersonaliseValuesDetail = orderHelper.GetPersonalizedAttributeLineItemDetails(lineItem.PersonaliseValueList, string.Empty);
                }
            }
        }

        //Replace sort key name
        protected virtual void ReplaceSortKeys(ref NameValueCollection sorts)
        {
            if (IsNotNull(sorts))
            {
                foreach (string key in sorts.Keys)
                {
                    if (string.Equals(key, ZnodeConstant.ReturnTotalWithCurrency, StringComparison.OrdinalIgnoreCase)) { ReplaceSortKeyName(ref sorts, ZnodeConstant.ReturnTotalWithCurrency.ToLower(), ZnodeRmaReturnDetailEnum.TotalReturnAmount.ToString()); }
                    if (string.Equals(key, ZnodeConstant.ReturnDateWithTime, StringComparison.OrdinalIgnoreCase)) { ReplaceSortKeyName(ref sorts, ZnodeConstant.ReturnDateWithTime.ToLower(), ZnodeRmaReturnDetailEnum.ReturnDate.ToString()); }
                }
            }
        }

        #region Admin

        //Update return order for admin
        protected virtual RMAReturnModel UpdateOrderReturnForAdmin(RMAReturnModel returnModel)
        {
            if (IsNull(returnModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorModelNull);

            ZnodeLogging.LogMessage("Execution started", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            RMAReturnModel updateReturnModel = BindReturnDetailsDataForAdmin(returnModel);

            //start transaction
            using (SqlConnection connection = new SqlConnection(HelperMethods.ConnectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();// Start a local transaction.

                try
                {
                    if (updateReturnModel?.ReturnLineItems?.Count > 0)
                    {
                        _returnLineItemsRepository.BatchUpdate(updateReturnModel.ReturnLineItems.ToEntity<ZnodeRmaReturnLineItem>()?.ToList());

                        if (returnModel.ReturnLineItems.All(x => x.RmaReturnStateId == (int)ZnodeReturnStateEnum.REJECTED))
                            updateReturnModel.RmaReturnStateId = (int)ZnodeReturnStateEnum.REJECTED;

                        bool isUpdateReturnDetails = _returnDetailsRepository.Update(updateReturnModel?.ToEntity<ZnodeRmaReturnDetail>());
                        ZnodeLogging.LogMessage("Return details updated:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { isUpdateReturnDetails = isUpdateReturnDetails });

                        //Save additional notes for return.
                        if (isUpdateReturnDetails && !string.IsNullOrEmpty(returnModel?.Notes))
                        {
                            int userId = HelperMethods.GetLoginAdminUserId();
                            SaveReturnNotes(new RMAReturnNotesModel()
                            {
                                RmaReturnDetailsId = returnModel.RmaReturnDetailsId,
                                Notes = returnModel.Notes,
                                CreatedBy = userId,
                                CreatedDate = returnModel.ModifiedDate,
                                ModifiedBy = userId,
                                ModifiedDate = returnModel.ModifiedDate
                            });
                        }
                        SaveReturnHistory(returnModel);
                        transaction.Commit();

                        if (!string.Equals(returnModel.OldReturnStatus, returnModel.ReturnStatus, StringComparison.InvariantCultureIgnoreCase))
                            SendReturnRequestEmailNotificationAsync(returnModel, true);

                        ZnodeLogging.LogMessage("Execution done", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                        return new RMAReturnModel() { ReturnNumber = updateReturnModel.ReturnNumber, RmaReturnDetailsId = updateReturnModel.RmaReturnDetailsId };
                    }
                    else
                    {
                        ZnodeLogging.LogMessage("Line Item not found", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                        throw new ZnodeException(ErrorCodes.InvalidData, "Line Item not found");
                    }
                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                    transaction.Rollback();
                    throw new ZnodeException(ErrorCodes.ExceptionalError, ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        //Bind return details data for admin
        protected virtual RMAReturnModel BindReturnDetailsDataForAdmin(RMAReturnModel returnModel)
        {
            if (IsNull(returnModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ModelNotNull);

            ZnodeRmaReturnDetail znodeReturnDetails = _returnDetailsRepository.Table.FirstOrDefault(x => x.ReturnNumber.ToLower() == returnModel.ReturnNumber.ToLower() && x.UserId == returnModel.UserId && x.OrderNumber == returnModel.OrderNumber);
            if (IsNotNull(znodeReturnDetails))
            {
                returnModel.RmaReturnDetailsId = znodeReturnDetails.RmaReturnDetailsId;
                RMAReturnModel updateReturnDetails = znodeReturnDetails.ToModel<RMAReturnModel>();

                updateReturnDetails.RmaReturnStateId = returnModel.RmaReturnStateId;
                if (returnModel.RmaReturnStateId == (int)ZnodeReturnStateEnum.APPROVED || returnModel.RmaReturnStateId == (int)ZnodeReturnStateEnum.PARTIALLY_APPROVED)
                    updateReturnDetails.OverDueAmount = returnModel.OverDueAmount;
                if (returnModel.RmaReturnStateId == (int)ZnodeReturnStateEnum.REFUND_PROCESSED)
                    updateReturnDetails.IsRefundProcess = returnModel.IsRefundProcess;
                updateReturnDetails.ModifiedDate = GetDateTime();
                updateReturnDetails.ModifiedBy = HelperMethods.GetLoginAdminUserId();

                //Bind Order Return Line Item Data for admin
                updateReturnDetails.ReturnLineItems = BindOrderReturnLineItemDataForAdmin(returnModel);

                //Bind Order Summary
                BindOrderReturnSummary(updateReturnDetails, true);
                return updateReturnDetails;
            }
            else
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.InvalidData);
        }

        //Bind Order Return Line Item Data for admin
        protected virtual List<RMAReturnLineItemModel> BindOrderReturnLineItemDataForAdmin(RMAReturnModel returnModel)
        {
            if (IsNull(returnModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ModelNotNull);

            List<RMAReturnLineItemModel> returnLineItemsList = _returnLineItemsRepository.Table.Where(x => x.RmaReturnDetailsId == returnModel.RmaReturnDetailsId)?.ToModel<RMAReturnLineItemModel>()?.ToList();

            if (IsNotNull(returnLineItemsList) && returnLineItemsList?.Count > 0)
            {
                foreach (RMAReturnLineItemModel returnLineItem in returnLineItemsList)
                {
                    RMAReturnLineItemModel lineItem = returnModel?.ReturnLineItems?.FirstOrDefault(x => x.RmaReturnLineItemsId == returnLineItem.RmaReturnLineItemsId);
                    if (IsNotNull(lineItem))
                    {
                        returnLineItem.ReturnedQuantity = lineItem.ReturnedQuantity;
                        returnLineItem.DiscountAmount = lineItem.DiscountAmount;
                        returnLineItem.ShipSeparately = lineItem.ShipSeparately;
                        returnLineItem.RefundAmount = lineItem.RefundAmount;
                        returnLineItem.IsShippingReturn = lineItem.IsShippingReturn;
                        if (lineItem.IsShippingReturn)
                            returnLineItem.ShippingCost = lineItem.ShippingCost;
                        else
                            returnLineItem.ShippingCost = null;
                        returnLineItem.TaxCost = lineItem.TaxCost;
                        returnLineItem.RmaReturnStateId = lineItem.RmaReturnStateId;
                        returnLineItem.ModifiedDate = GetDateTime();
                        returnLineItem.ModifiedBy = HelperMethods.GetLoginAdminUserId();
                        if (returnLineItem?.OmsReturnOrderLineItemsId != 0 && (returnModel.RmaReturnStateId == (int)ZnodeReturnStateEnum.APPROVED || returnModel.RmaReturnStateId == (int)ZnodeReturnStateEnum.PARTIALLY_APPROVED))
                            returnLineItem.OmsReturnOrderLineItemsId = lineItem.OmsOrderLineItemsId;
                    }
                    else
                    {
                        return new List<RMAReturnLineItemModel>();
                    }
                }
            }
            return returnLineItemsList;
        }

        //To save return history
        protected virtual void SaveReturnHistory(RMAReturnModel returnModel)
        {
            ZnodeLogging.LogMessage("Execution started", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (IsNotNull(returnModel))
            {
                List<RMAReturnHistoryModel> returnHistoryModel = new List<RMAReturnHistoryModel>();
                //to check return status is updated
                if (ExistUpdateHistory(ZnodeConstant.ReturnUpdatedStatus, returnModel))
                {
                    string returnHistoryMessage = GetHistoryMessageByKey(ZnodeConstant.ReturnUpdatedStatus, returnModel);
                    returnHistoryModel.Add(new RMAReturnHistoryModel { Message = returnHistoryMessage });
                }

                //to refund processed status is updated 
                if (ExistUpdateHistory(ZnodeConstant.ReturnRefundProcessed, returnModel))
                {
                    string returnHistoryMessage = GetHistoryMessageByKey(ZnodeConstant.ReturnRefundProcessed, returnModel);
                    decimal amount = returnModel.TotalReturnAmount;
                    returnHistoryModel.Add(new RMAReturnHistoryModel { Message = returnHistoryMessage, ReturnAmount = amount });
                }

                if (HelperUtility.IsNotNull(returnModel.ReturnLineItemHistory) && returnModel.ReturnLineItemHistory?.Count > 0)
                {
                    string returnHistoryMessage = string.Empty;
                    string returnShippingMessage = string.Empty;
                    decimal amount = 0;
                    decimal shippingAmount = 0;
                    foreach (var returnLineItemHistory in returnModel.ReturnLineItemHistory)
                    {
                        if (IsNotNull(returnLineItemHistory.Value))
                        {
                            returnHistoryMessage = GenerateReturnLineItemHistory(returnHistoryMessage, returnLineItemHistory.Value, returnModel.OldReturnLineItems, returnModel.CultureCode, ref returnShippingMessage);

                            if (!string.IsNullOrEmpty(returnLineItemHistory.Value.ReturnedQuantity) || !Equals(returnLineItemHistory.Value?.ReturnedQuantity, "0") || !string.IsNullOrEmpty(returnLineItemHistory.Value.ReturnUpdatedStatus))
                                amount = amount + returnLineItemHistory.Value.Total;

                            string oldIsShippingReturn = returnModel.OldReturnLineItems.FirstOrDefault(w => w.RmaReturnLineItemsId == returnLineItemHistory.Value.RmaReturnLineItemsId).IsShippingReturn ? ZnodeConstant.True : ZnodeConstant.False;
                            string updatedShippingReturn = returnLineItemHistory.Value.IsShippingReturn;

                            if (!string.Equals(updatedShippingReturn, oldIsShippingReturn, StringComparison.InvariantCultureIgnoreCase))
                                shippingAmount = string.Equals(updatedShippingReturn, ZnodeConstant.True, StringComparison.InvariantCultureIgnoreCase) ? shippingAmount + returnLineItemHistory.Value.ReturnShippingAmount : shippingAmount;

                            ZnodeLogging.LogMessage("return history amount and shipping amount value: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { amount = amount, shippingAmount = shippingAmount });
                        }
                    }
                    if (!string.IsNullOrEmpty(returnHistoryMessage))
                        returnHistoryModel.Add(new RMAReturnHistoryModel { Message = returnHistoryMessage, ReturnAmount = amount });

                    if (!string.IsNullOrEmpty(returnShippingMessage))
                        returnHistoryModel.Add(new RMAReturnHistoryModel { Message = returnShippingMessage, ReturnAmount = shippingAmount });
                }

                ZnodeLogging.LogMessage("returnHistoryModel count: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { returnHistoryModel = returnHistoryModel?.Count });

                if (IsNotNull(returnHistoryModel) && returnHistoryModel?.Count > 0)
                {
                    returnHistoryModel.ForEach(x => { x.RmaReturnDetailsId = returnModel.RmaReturnDetailsId; x.CreatedBy = returnModel.CreatedBy; x.ModifiedBy = returnModel.ModifiedBy; });
                    CreateReturnHistory(returnHistoryModel);
                }
            }
            ZnodeLogging.LogMessage("Execution done", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }

        //to save return line item changes
        protected virtual string GenerateReturnLineItemHistory(string returnHistoryMessage, RMAReturnLineItemHistoryModel skuHistory, List<RMAReturnLineItemModel> oldReturnLineItems, string cultureCode, ref string returnShippingMessage)
        {
            ZnodeLogging.LogMessage("Execution started", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (IsNotNull(skuHistory) && IsNotNull(oldReturnLineItems))
            {
                string sku = skuHistory.SKU;
                int returnLineItemId = skuHistory.RmaReturnLineItemsId;
                string oldIsShippingReturn = oldReturnLineItems.FirstOrDefault(w => w.RmaReturnLineItemsId == returnLineItemId).IsShippingReturn ? ZnodeConstant.True : ZnodeConstant.False;
                string productName = skuHistory.ProductName;
                string oldQuantity = Convert.ToString(oldReturnLineItems.FirstOrDefault(w => w.RmaReturnLineItemsId == returnLineItemId)?.ReturnedQuantity);
                oldQuantity = string.IsNullOrEmpty(oldQuantity) ? Convert.ToString(oldReturnLineItems.FirstOrDefault(w => w.RmaReturnLineItemsId == returnLineItemId)?.ExpectedReturnQuantity) : oldQuantity;
                oldQuantity = oldQuantity?.Split('.')?.FirstOrDefault();
                string confirmedQty = skuHistory.ReturnedQuantity;

                if (!string.IsNullOrEmpty(skuHistory.ReturnUpdatedStatus))
                {
                    string oldStatus = Convert.ToString(oldReturnLineItems.FirstOrDefault(w => w.RmaReturnLineItemsId == returnLineItemId)?.ReturnStatus);
                    confirmedQty = !string.IsNullOrEmpty(confirmedQty) && !Equals(confirmedQty, "0") ? confirmedQty : oldQuantity;
                    returnHistoryMessage = GetConsolidatedHistoryMessage(string.Format(Admin_Resources.ReturnHistoryUpdatedLineItemStatus, sku, productName, confirmedQty, oldStatus, skuHistory.ReturnUpdatedStatus), returnHistoryMessage);
                }

                if (!string.IsNullOrEmpty(skuHistory.ReturnedQuantity) && !Equals(skuHistory.ReturnedQuantity, "0"))
                    returnHistoryMessage = GetConsolidatedHistoryMessage(string.Format(Admin_Resources.ReturnHistoryUpdatedLineItemQuantity, sku, productName, oldQuantity, confirmedQty), returnHistoryMessage);

                ZnodeLogging.LogMessage("returnHistoryMessage: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { returnHistoryMessage = returnHistoryMessage });

                if (!string.IsNullOrEmpty(skuHistory.IsShippingReturn) && !string.Equals(skuHistory.IsShippingReturn, oldIsShippingReturn, StringComparison.InvariantCultureIgnoreCase))
                    returnShippingMessage = GetConsolidatedHistoryMessage(string.Format(Admin_Resources.ReturnHistoryUpdatedLineItemShipping, sku, productName, skuHistory.IsShippingReturn?.ToLower()), returnShippingMessage);
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return returnHistoryMessage;
        }

        //to get consolidated history message
        protected virtual string GetConsolidatedHistoryMessage(string message, string mergedMessage)
        {
            if (!string.IsNullOrEmpty(mergedMessage))
                mergedMessage += $"<br/>{ message}";
            else
                mergedMessage = message;

            return mergedMessage;
        }

        //to check get history for provided key
        protected virtual string GetHistoryMessageByKey(string key, RMAReturnModel returnModel)
        {
            string value = string.Empty;
            if (IsNotNull(returnModel) && !string.IsNullOrEmpty(key))
            {
                returnModel.ReturnHistory?.TryGetValue(key, out value);

                switch (key)
                {
                    case ZnodeConstant.ReturnUpdatedStatus:
                        return string.Format(Admin_Resources.ReturnHistoryUpdatedStatus, returnModel.OldReturnStatus, value);

                    case ZnodeConstant.ReturnRefundProcessed:
                        return Admin_Resources.ReturnHistoryRefundProcessed;

                    default:
                        return string.Format(Admin_Resources.ReturnHistory, key, value);
                }
            }
            return value;
        }

        //to check update history for provided key
        protected virtual bool ExistUpdateHistory(string key, RMAReturnModel returnModel)
            => returnModel?.ReturnHistory?.Keys?.Contains(key) ?? false;

        //Get return date filter value from filters
        protected virtual string GetReturnDateRangeFromFilters(FilterCollection filters)
        {
            string returnDateRange = string.Empty;
            FilterCollection returnDateFilter = new FilterCollection();
            FilterTuple returnDateFilterTuple = filters?.FirstOrDefault(x => string.Equals(x.FilterName, ZnodeRmaReturnDetailEnum.ReturnDate.ToString(), StringComparison.CurrentCultureIgnoreCase));
            if (IsNotNull(returnDateFilterTuple))
            {
                returnDateFilter.Add(returnDateFilterTuple);
                RemoveFilter(filters, ZnodeRmaReturnDetailEnum.ReturnDate.ToString());
                returnDateRange = DynamicClauseHelper.GenerateDynamicWhereClauseForSP(returnDateFilter.ToFilterDataCollection());
            }
            return returnDateRange;
        }

        //Remove filter from FilterCollection by name
        protected virtual void RemoveFilter(FilterCollection filters, string filterName)
        {
            if (IsNotNull(filters) && !string.IsNullOrEmpty(filterName))
                filters?.RemoveAll(x => string.Equals(x.FilterName, filterName, StringComparison.CurrentCultureIgnoreCase));
        }

        //Check RMA configuration for order
        protected virtual bool IsRMAConfigurationValidForOrder(string orderNumber, int userId)
        {
            if (string.IsNullOrEmpty(orderNumber) || userId < 1)
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.InvalidData);

            RMAConfigurationModel rmaConfiguration = GetService<IRMAConfigurationService>().GetRMAConfiguration();
            DateTime? orderDate = GetOrderDetailsByOrderNumber(orderNumber, userId)?.OrderDate;
            if (IsNotNull(rmaConfiguration) && IsNotNull(orderDate))
            {
                int rmaPeriod = 0;
                if (rmaConfiguration.MaxDays.HasValue)
                    rmaPeriod = rmaConfiguration.MaxDays.Value;

                DateTime validRmaConfigOrderDate = Convert.ToDateTime(orderDate).AddDays(rmaPeriod);
                return !(validRmaConfigOrderDate.Date < DateTime.Now.Date);
            }
            return false;
        }
        #endregion

        #endregion
    }
}
