﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Services
{
    public interface IContentWidgetService
    {

        /// <summary>
        /// Get the List of Content Widget
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="page">page</param>
        /// <returns>ContentWidgetListModel model</returns>
        ContentWidgetListModel List(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Create Content Widget
        /// </summary>
        /// <param name="model">ContentWidgetCreateModel model</param>
        /// <returns>ContentWidgetResponseModel model</returns>
        ContentWidgetResponseModel Create(ContentWidgetCreateModel model);

        /// <summary>
        /// Update Content Widget
        /// </summary>
        /// <param name="model">ContentWidgetUpdateModel model</param>
        /// <returns>ContentWidgetResponseModel</returns>
        ContentWidgetResponseModel Update(ContentWidgetUpdateModel model);

        /// <summary>
        /// Get Content Widget
        /// </summary>
        /// <param name="widgetKey">widgetKey</param>
        /// <returns>ContentWidgetResponseModel</returns>
        ContentWidgetResponseModel GetContentWidget(string widgetKey);

        /// <summary>
        /// Get Associated Variants to a Content Widget
        /// </summary>
        /// <param name="widgetKey">widgetKey</param>
        /// <returns>List of AssociatedVariantModel</returns>
        List<AssociatedVariantModel> GetAssociatedVariants(string widgetKey);

        /// <summary>
        ///  Associate Variant to a Content Widget
        /// </summary>
        /// <param name="variant">AssociatedVariantModel model</param>
        /// <returns>List of AssociatedVariantModel</returns>
        List<AssociatedVariantModel> AssociateVariant(AssociatedVariantModel variant);


        /// <summary>
        /// Delete Associated Variant
        /// </summary>
        /// <param name="variantId">variantId</param>
        /// <returns>Status</returns>
        bool DeleteAssociatedVariant(int variantId);

        /// <summary>
        /// Delete Content Widget
        /// </summary>
        /// <param name="contentWidgetIds">ParameterModel model</param>
        /// <returns>Status</returns>
        bool DeleteContentWidget(ParameterModel contentWidgetIds);

        /// <summary>
        /// Delete Content Widget by Widget Key
        /// </summary>
        /// <param name="widgetKey">widgetKey</param>
        /// <returns>Status</returns>
        bool DeleteContentWidgetByWidgetKey(string widgetKey);

        /// <summary>
        /// Verify if the Widget Key Exist
        /// </summary>
        /// <param name="widgetKey">widgetKey</param>
        /// <returns>Status</returns>
        bool IsWidgetKeyExists(string widgetKey);

        /// <summary>
        /// Associate Widget Template
        /// </summary>
        /// <param name="variantId">variantId</param>
        /// <param name="WidgetTemplateId">WidgetTemplateId</param>
        /// <returns>Status</returns>
        bool AssociateWidgetTemplate(int variantId, int widgetTemplateId);


    }
}
