﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Services.Maps
{
    public static class ImpersonationMap
    {
        // This method will map the ZnodeUserAddress Entity to AddressModel.
        public static ImpersonationActivityLogModel ToModel(ZnodeImpersonificationLog entity)
        {
            if (HelperUtility.IsNotNull(entity))
            {
                ImpersonationActivityLogModel impersonificationLog = entity.ToModel<ImpersonationActivityLogModel>();
                if (HelperUtility.IsNotNull(impersonificationLog))
                {
                 
                    impersonificationLog.CSRId = entity.CSRId.GetValueOrDefault();
                    impersonificationLog.OperationType = entity.OperationType;
                    impersonificationLog.ActivityType = entity.ActivityType;
                    impersonificationLog.ShopperId = entity.WebstoreuserId.GetValueOrDefault();                
                    return impersonificationLog;
                }
            }
            return null;
        }

        // This method will map the list of ZnodeUserAddress Entities to AddressListModel.
        public static ImpersonationActivityListModel ToImpersonationListModel(IList<ZnodeImpersonificationLog> entities, PageListModel pageListModel)
        {
            ImpersonationActivityListModel impersonationActivityListModel = new ImpersonationActivityListModel();

            if (entities?.Count > 0)
            {
                impersonationActivityListModel.LogActivityList = new List<ImpersonationActivityLogModel>();
                foreach (ZnodeImpersonificationLog logList in entities)
                {
                    ImpersonationActivityLogModel znodeImpersonificationLog = ToModel(logList);
                    //Skip the null entries
                    if (HelperUtility.IsNotNull(logList))
                        impersonationActivityListModel.LogActivityList.Add(znodeImpersonificationLog);
                }
            }
            impersonationActivityListModel.BindPageListModel(pageListModel);
            return impersonationActivityListModel;
        }
    }
}
