﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Znode.Engine.Api.Models
{
    public class RMAReturnCalculateModel : BaseModel
    {
        [Required]
        public List<RMAReturnCalculateLineItemModel> ReturnCalculateLineItemList { get; set; }
        [Required]
        public string OrderNumber { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public int PortalId { get; set; }
        public decimal ReturnSubTotal { get; set; }
        public decimal ReturnTaxCost { get; set; }
        public decimal ReturnShippingCost { get; set; }
        public decimal ReturnTotal { get; set; }
        public string CultureCode { get; set; }
    }
}