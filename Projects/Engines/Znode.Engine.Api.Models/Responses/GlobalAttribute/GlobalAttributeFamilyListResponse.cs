﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models.Responses
{
    public class GlobalAttributeFamilyListResponse : BaseListResponse
    {
        public List<GlobalAttributeFamilyModel> AttributeFamily { get; set; }

        public GlobalAttributeFamilyLocaleListModel AttributeFamilyLocales { get; set; }
    }
}
