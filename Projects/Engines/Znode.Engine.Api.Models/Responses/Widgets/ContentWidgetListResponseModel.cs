﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models.Responses
{
    public class ContentWidgetListResponseModel :  BaseListResponse
    {
        public List<AssociatedVariantModel> AssociatedVariants { get; set; }
        public List<ContentWidgetResponseModel> WidgetList { get; set; }
    }
}
