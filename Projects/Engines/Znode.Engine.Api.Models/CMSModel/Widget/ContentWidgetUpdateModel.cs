﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Libraries.Resources;

namespace Znode.Engine.Api.Models
{
    public class ContentWidgetUpdateModel : BaseModel
    {

        [Required(AllowEmptyStrings = false, ErrorMessageResourceName = ZnodeApi_Resources.ErrorContentWidgetKey, ErrorMessageResourceType = typeof(ZnodeApi_Resources))]
        [RegularExpression(@"^[A-Za-z][a-zA-Z0-9]*$", ErrorMessageResourceName = ZnodeApi_Resources.WidgetKeyError, ErrorMessageResourceType = typeof(Api_Resources))]
        [MaxLength(100, ErrorMessageResourceName = ZnodeApi_Resources.WidgetKeyLimit, ErrorMessageResourceType = typeof(Api_Resources))]
        public string WidgetKey { get; set; }

        [MaxLength(1000)]
        public string Tags { get; set; }

        [Required(ErrorMessageResourceType = typeof(Api_Resources), ErrorMessageResourceName = ZnodeApi_Resources.RequiredContentWidgetName)]
        [MaxLength(100, ErrorMessageResourceName = ZnodeApi_Resources.WidgetNameLimit, ErrorMessageResourceType = typeof(Api_Resources))]
        public string Name { get; set; }
    }
}
