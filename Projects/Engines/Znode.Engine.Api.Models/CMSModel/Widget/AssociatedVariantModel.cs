﻿using System.ComponentModel.DataAnnotations;
using Znode.Libraries.Resources;

namespace Znode.Engine.Api.Models
{
    public class AssociatedVariantModel : BaseModel
    {
        [Required(ErrorMessageResourceType = typeof(Api_Resources), ErrorMessageResourceName = ZnodeApi_Resources.ErrorRequiredLocaleId)]
        public int LocaleId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Api_Resources), ErrorMessageResourceName = ZnodeApi_Resources.ErrorRequiredProfileId)]
        public int? ProfileId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Api_Resources), ErrorMessageResourceName = ZnodeApi_Resources.ErrorContentWidgetId)]
        public int ContentWidgetId { get; set; }
        public string LocaleName { get; set; }
        public string ProfileName { get; set; }
        public string WidgetKey { get; set; }
        public int WidgetProfileVariantId { get; set; }
        public int? WidgetTemplateId { get; set; }
    }
}
