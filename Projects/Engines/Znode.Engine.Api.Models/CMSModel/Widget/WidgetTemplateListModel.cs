﻿using System.Collections.Generic;

namespace Znode.Engine.Api.Models
{
    public class WidgetTemplateListModel : BaseListModel
    {
        public List<WidgetTemplateModel> WidgetTemplates { get; set; }
    }
}
