﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Libraries.Resources;

namespace Znode.Engine.Api.Models
{
    public class ContentWidgetCreateModel : BaseModel
    {

        [Required(AllowEmptyStrings = false, ErrorMessageResourceName = ZnodeApi_Resources.ErrorContentWidgetKey, ErrorMessageResourceType = typeof(ZnodeApi_Resources))]
        [RegularExpression(@"^[A-Za-z][a-zA-Z0-9]*$", ErrorMessageResourceName = ZnodeApi_Resources.WidgetKeyError, ErrorMessageResourceType = typeof(Api_Resources))]
        [MaxLength(100, ErrorMessageResourceName = ZnodeApi_Resources.WidgetKeyLimit, ErrorMessageResourceType = typeof(Api_Resources))]
        public string WidgetKey { get; set; }

        [Required(ErrorMessageResourceType = typeof(Api_Resources), ErrorMessageResourceName = ZnodeApi_Resources.ErrorIsGlobalContentWidget)]
        public bool IsGlobalContentWidget { get; set; }

        [MaxLength(1000)]
        public string Tags { get; set; }

        [Required(ErrorMessageResourceType = typeof(Api_Resources), ErrorMessageResourceName = ZnodeApi_Resources.RequiredFamilyCode)]
        public string FamilyCode { get; set; }

        [Required(ErrorMessageResourceType = typeof(Api_Resources), ErrorMessageResourceName = ZnodeApi_Resources.RequiredContentWidgetName)]
        [MaxLength(100, ErrorMessageResourceName = ZnodeApi_Resources.WidgetNameLimit, ErrorMessageResourceType = typeof(Api_Resources))]
        public string Name { get; set; }
        public string TemplateName { get; set; }

        public int? PortalId { get; set; }
        public int WidgetTemplateId { get; set; }
        public int FamilyId { get; set; }
        public int GlobalEntityId { get; set; }
        public string StoreCode { get; set; }
    }
}
