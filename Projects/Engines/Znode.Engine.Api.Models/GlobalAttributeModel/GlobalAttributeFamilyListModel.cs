﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class GlobalAttributeFamilyListModel : BaseListModel
    {
        public List<GlobalAttributeFamilyModel> AttributeFamilyList { get; set; }
    }
}
