﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class GlobalAttributeFamilyLocaleListModel : BaseListModel
    {
        public string FamilyCode { get; set; }
        public List<GlobalAttributeFamilyLocaleModel> AttributeFamilyLocales { get; set; }
    }
}
