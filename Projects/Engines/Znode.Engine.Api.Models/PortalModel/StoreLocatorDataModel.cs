﻿using System.ComponentModel.DataAnnotations;

namespace Znode.Engine.Api.Models
{
    public class StoreLocatorDataModel : BaseModel
    {
        public int PortalId { get; set; }
        public int? DisplayOrder { get; set; }
        public string StoreName { get; set; }
        public string StoreLocationCode { get; set; }
        public int AddressId { get; set; }
        public int PortalAddressId { get; set; }
        public bool IsActive { get; set; }
        public string MapQuestURL { get; set; }
        public string MediaPath { get; set; }
        public decimal? Longitude { get; set; }
        public decimal? Latitude { get; set; }
        public int MediaId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        [MaxLength(600)]
        public string DisplayName { get; set; }
        [MaxLength(300)]
        public string CountryName { get; set; }
        public string StateName { get; set; }
        public string StateCode { get; set; }
        public string CityName { get; set; }
        [MaxLength(50)]
        public string PostalCode { get; set; }
        [MaxLength(50)]
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string ExternalId { get; set; }
        public string Portalname { get; set; }
        [MaxLength(300)]
        public string CompanyName { get; set; }
    }
}
