﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class PortalApprovalLevelListModel : BaseListModel
    {
        public List<PortalApprovalLevelModel> PortalApprovalLevels { get; set; }

        public PortalApprovalLevelListModel()
        {
            PortalApprovalLevels = new List<PortalApprovalLevelModel>();
        }
    }
}
