﻿CREATE TYPE [dbo].[SKUPriceForQuote] AS TABLE(
	[SKU] [varchar](600) NULL,
	[OmsSavedCartLineItemId] [int] NULL,
	[Price] [numeric](28, 6) NULL,
	[ShippingCost]  NUMERIC (28, 6) NULL
)
GO