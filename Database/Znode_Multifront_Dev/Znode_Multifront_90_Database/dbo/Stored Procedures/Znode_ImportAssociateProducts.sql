﻿CREATE PROCEDURE [dbo].[Znode_ImportAssociateProducts](
	  @TableName nvarchar(100), @Status bit OUT, @UserId int, @ImportProcessLogId int, @NewGUId nvarchar(200), @PimCatalogId int= 0)
AS
	--------------------------------------------------------------------------------------
	-- Summary :  Import Product Association 
	
	-- Unit Testing : 
	--BEGIN TRANSACTION;
	--update ZnodeGlobalSetting set FeatureValues = '5' WHERE FeatureName = 'InventoryRoundOff' 
	--    DECLARE @status INT;
	--    EXEC [Znode_ImportInventory] @InventoryXML = '<ArrayOfImportInventoryModel>
	-- <ImportInventoryModel>
	--   <SKU>S1002</SKU>
	--   <Quantity>999998.33</Quantity>
	--   <ReOrderLevel>10</ReOrderLevel>
	--   <RowNumber>1</RowNumber>
	--   <ListCode>TestInventory</ListCode>
	--   <ListName>TestInventory</ListName>
	-- </ImportInventoryModel>
	--</ArrayOfImportInventoryModel>' , @status = @status OUT , @UserId = 2;
	--    SELECT @status;
	--    ROLLBACK TRANSACTION;
	--------------------------------------------------------------------------------------

BEGIN
	
	BEGIN TRY
	BEGIN TRAN A;
		DECLARE @MessageDisplay nvarchar(100), @SSQL nvarchar(max);
		DECLARE @GetDate datetime= dbo.Fn_GetDate();

		IF OBJECT_ID('TEMPDB..#InsertProductAssociation') IS NOT NULL 
			DROP TABLE #InsertProductAssociation

		IF OBJECT_ID('TEMPDB..#InsertProduct') IS NOT NULL 
			DROP TABLE #InsertProduct

		IF OBJECT_ID('TEMPDB..#SKU') IS NOT NULL 
			DROP TABLE #SKU

		IF OBJECT_ID('TEMPDB..#InsertProductAssociation_Parent_type') IS NOT NULL 
			DROP TABLE #InsertProductAssociation_Parent_type
		-- Retrive RoundOff Value from global setting 

		CREATE TABLE #InsertProductAssociation 
		( 
			RowId int IDENTITY(1, 1) PRIMARY KEY, RowNumber int, ParentSKU varchar(300), ChildSKU varchar(200), DisplayOrder int,IsDefault varchar(10), GUID nvarchar(400)
		);

		CREATE TABLE #InsertProductAssociation_Parent_type
		( 
			RowId int  PRIMARY KEY, RowNumber int, ParentSKU varchar(300), ChildSKU varchar(200), DisplayOrder int,IsDefault varchar(10), GUID nvarchar(400)
			,PT_ParentProductId varchar(300), PT_ProductType nvarchar(100)
		);
		
		CREATE TABLE #InsertProduct 
		( 
			RowId int IDENTITY(1, 1) PRIMARY KEY, RowNumber int, ParentProductId varchar(300), ChildProductId varchar(200), DisplayOrder int,IsDefault varchar(10), GUID nvarchar(400), ProductType nvarchar(100)
		);


		DECLARE @CategoryAttributId int;

		DECLARE @InventoryListId int;

		SET @SSQL = 'Select RowNumber,ParentSKU,ChildSKU,DisplayOrder,IsDefault,GUID FROM '+@TableName;
		INSERT INTO #InsertProductAssociation( RowNumber, ParentSKU,ChildSKU,DisplayOrder,IsDefault, GUID )
		EXEC sys.sp_sqlexec @SSQL;


		--@MessageDisplay will use to display validate message for input inventory value  
		CREATE TABLE #SKU 
		( 
						   SKU nvarchar(300), PimProductId int
		);
		INSERT INTO #SKU
			   SELECT b.AttributeValue, a.PimProductId
			   FROM ZnodePimAttributeValue AS a
					INNER JOIN
					ZnodePimAttributeValueLocale AS b
					ON a.PimAttributeId = dbo.Fn_GetProductSKUAttributeId() AND 
					   a.PimAttributeValueId = b.PimAttributeValueId;

		DECLARE @ProductType TABLE
		( 
			ProductType nvarchar(100) ,PimProductId int
		);
		INSERT INTO @ProductType
			   SELECT  ZPADV.AttributeDefaultValueCode, a.PimProductId
			   FROM ZnodePimAttributeValue AS a
					INNER JOIN
					ZnodePimProductAttributeDefaultValue AS b
					ON a.PimAttributeId = dbo.Fn_GetProductTypeAttributeId() AND 
					   a.PimAttributeValueId = b.PimAttributeValueId
					   Inner join ZnodePimAttributeDefaultValue ZPADV On b.PimAttributeDefaultValueId = ZPADV.PimAttributeDefaultValueId
					   where  ZPADV.AttributeDefaultValueCode in ('GroupedProduct','BundleProduct','ConfigurableProduct');

		INSERT INTO #InsertProductAssociation_Parent_type
			SELECT IPAC.*,SKUParent.PimProductId, PT.ProductType
					FROM #InsertProductAssociation AS IPAC INNER JOIN #SKU AS SKUParent ON IPAC.ParentSKU = SKUParent.SKU 
					inner join @ProductType PT on PT.PimProductId = SKUParent.PimProductId


		-- start Functional Validation 
			INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '84', 'IsDefault', IsDefault, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM #InsertProductAssociation_Parent_type AS ii
			   WHERE isnull(ii.IsDefault,'') = '' and ii.PT_ProductType ='ConfigurableProduct'

			INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '68', 'IsDefault', IsDefault, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM #InsertProductAssociation_Parent_type AS ii
			   WHERE isnull(ii.IsDefault,'') not in ('true','1','false','0') and isnull(ii.IsDefault,'') <> '' and ii.PT_ProductType ='ConfigurableProduct'

			INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '19', 'ChildSKU', ChildSKU, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM #InsertProductAssociation_Parent_type AS ii
			   WHERE NOT EXISTS( SELECT SKU FROM #SKU SKU WHERE ii.ChildSKU = SKU.SKU)

			INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '53', 'ParentSKU / ChildSKU', ParentSKU+' / '+ChildSKU, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM #InsertProductAssociation AS ii
			   WHERE ii.ParentSKU IN
			   (
				   select ParentSKU from #InsertProductAssociation_Parent_type
					group by ParentSKU,ChildSKU
					having count(1)>1
			   );

			INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '49', 'ParentSKU',   ParentSKU , @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM #InsertProductAssociation_Parent_type AS ii
			   WHERE not exists
			   (
				   SELECT SKU  FROM #SKU SKU inner join @ProductType  PT ON SKU.PimProductId = PT.PimProductId and ii.ParentSKU = SKU.SKU
	
			   );

			INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '51', 'ChildSKU',   ChildSKU, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM #InsertProductAssociation_Parent_type AS ii
			   WHERE exists 
			   (
				   SELECT SKU  FROM #SKU SKU inner join @ProductType  PT ON SKU.PimProductId = PT.PimProductId and ii.ChildSKU = SKU.SKU
	
			   );

			INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '35', 'ParentSKU',  'Configure Attribute Missing: '+ Convert(nvarchar(400),isnull(ParentSKU,'')), @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM #InsertProductAssociation_Parent_type AS ii Inner join #SKU PS ON 
			   ii.ParentSKU = PS.SKU 
			   Inner join @ProductType  PT ON PS.PimProductId = PT.PimProductId  AND PT.ProductType  in ('ConfigurableProduct')
			   where  NOT exists 
			   (select PimProductId  from ZnodePimConfigureProductAttribute d where PS.PimProductId = d.PimProductId)
			   -- End Function Validation 	

			INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			SELECT '17', 'DisplayOrder', DisplayOrder, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			FROM #InsertProductAssociation_Parent_type AS ii
			WHERE ((isnull(ii.DisplayOrder,'') = '' ) and ii.PT_ProductType !='ConfigurableProduct' ) --or  ii.DisplayOrder = 0

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			SELECT '64', 'DisplayOrder', DisplayOrder, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			FROM #InsertProductAssociation_Parent_type AS ii
			WHERE ((isnull(ii.DisplayOrder,'') = '' ) and ii.PT_ProductType !='ConfigurableProduct' ) --or  ii.DisplayOrder > 999

			   UPDATE ZIL
			   SET ZIL.ColumnName =   ZIL.ColumnName + ' [ SKU - ' + isnull(ParentSKU,'') + ' ] '
			   FROM ZnodeImportLog ZIL 
			   INNER JOIN #InsertProductAssociation_Parent_type IPA ON (ZIL.RowNumber = IPA.RowNumber)
			   WHERE  ZIL.ImportProcessLogId = @ImportProcessLogId AND ZIL.RowNumber IS NOT NULL

		--- Delete Invalid Data after functional validatin  
		DELETE FROM #InsertProductAssociation_Parent_type
		WHERE RowNumber IN
		(
			SELECT DISTINCT 
				   RowNumber
			FROM ZnodeImportLog
			WHERE ImportProcessLogId = @ImportProcessLogId AND 
				  GUID = @NewGUID
		);


		insert into #InsertProduct (RowNumber,  ParentProductId , ChildProductId , DisplayOrder, IsDefault, ProductType)
			SELECT RowNumber , SKUParent.PimProductId SKUParentId , 
				   ( Select TOP 1 SKUChild.PimProductId from #SKU AS SKUChild where  SKUChild.SKU = IPAC.ChildSKU ) SKUChildId,
				    case when isnull(DisplayOrder,'')= '' and IPAC.PT_ProductType ='ConfigurableProduct'  then 99 else DisplayOrder end DisplayOrder ,
					 case when (isnull(IsDefault,'')= '' or IsDefault =0 or IsDefault > 999) and IPAC.PT_ProductType !='ConfigurableProduct'  then '0' else IsDefault end IsDefault, IPAC.PT_ProductType
					FROM #InsertProductAssociation_Parent_type AS IPAC INNER JOIN #SKU AS SKUParent ON IPAC.ParentSKU = SKUParent.SKU 

	-- Update Record count in log 
        DECLARE @FailedRecordCount BIGINT
		DECLARE @SuccessRecordCount BIGINT
		SELECT @FailedRecordCount = COUNT(DISTINCT RowNumber) FROM ZnodeImportLog WHERE RowNumber IS NOT NULL AND  ImportProcessLogId = @ImportProcessLogId;
		Select @SuccessRecordCount = count(DISTINCT RowNumber) FROM #InsertProduct
		UPDATE ZnodeImportProcessLog SET FailedRecordcount = @FailedRecordCount , SuccessRecordCount = @SuccessRecordCount, TotalProcessedRecords = (ISNULL(@FailedRecordCount,0) + ISNULL(@SuccessRecordCount,0))
		WHERE ImportProcessLogId = @ImportProcessLogId;
		-- End

		update #InsertProduct
		set IsDefault =0
		where RowNumber < (select max(RowNumber) from #InsertProduct where IsDefault = '1' or  IsDefault = 'True' and ProductType ='ConfigurableProduct')
		and  ProductType ='ConfigurableProduct';


		update ZnodePimProductTypeAssociation
		set  IsDefault =0
		where  exists (select top 1 1  from #InsertProduct IP where IsDefault = '1' or  IsDefault = 'true' and PimParentProductId = IP.ParentProductId and ProductType ='ConfigurableProduct')
		

		UPDATE B set b.ModifiedDate = @GetDate, b.ModifiedBy = @UserId, b.DisplayOrder = case when a.DisplayOrder is not null then a.DisplayOrder else b.DisplayOrder end
				,b.IsDefault = A.IsDefault
		from #InsertProduct A
		INNER JOIN ZnodePimProductTypeAssociation B ON a.ParentProductId = b.PimParentProductId and a.ChildProductId = b.PimProductId

		INSERT INTO ZnodePimProductTypeAssociation (PimParentProductId, PimProductId, DisplayOrder, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, IsDefault) 
		select  ParentProductId , ChildProductId , DisplayOrder, @UserId, @GetDate, @UserId, @GetDate, IsDefault  
		from #InsertProduct 
		where  NOT Exists (Select TOP 1 1 from ZnodePimProductTypeAssociation where PimParentProductId =  #InsertProduct.ParentProductId
		AND PimProductId = #InsertProduct.ChildProductId )

								 
		--select 'End'
		--      SET @Status = 1;
		UPDATE ZnodeImportProcessLog
		  SET Status = dbo.Fn_GetImportStatus( 2 ), ProcessCompletedDate = @GetDate
		WHERE ImportProcessLogId = @ImportProcessLogId;

		COMMIT TRAN A;
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN A;
		UPDATE ZnodeImportProcessLog
		  SET Status = dbo.Fn_GetImportStatus( 3 ), ProcessCompletedDate = @GetDate
		WHERE ImportProcessLogId = @ImportProcessLogId;

		SET @Status = 0;
		SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE();
	
	END CATCH;
END;