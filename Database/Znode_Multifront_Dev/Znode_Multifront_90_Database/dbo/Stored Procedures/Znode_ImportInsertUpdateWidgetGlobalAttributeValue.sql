﻿CREATE PROCEDURE [dbo].[Znode_ImportInsertUpdateWidgetGlobalAttributeValue]
(
    @GlobalEntityValueDetail  [GlobalEntityValueDetail] READONLY,
    @UserId            INT       ,
    @status            BIT    OUT,
    @IsNotReturnOutput BIT    = 0 )
AS
     BEGIN
         BEGIN TRAN A;
         BEGIN TRY
			 DECLARE @GlobalEntityId INT,
			  @MultiSelectGroupAttributeTypeName nvarchar(200)='Select'
			 ,@MediaGroupAttributeTypeName nvarchar(200)='Media'
             DECLARE @GetDate DATETIME = dbo.Fn_GetDate();
			 DECLARE @LocaleId INT 
			 declare  @CMSContentWidgetId int
			 declare  @CMSWidgetProfileVariantId int
			 DECLARE @TBL_Widget TABLE (CMSWidgetProfileVariantId [int] NULL)
			 DECLARE @TBL_DeleteUser TABLE (CMSWidgetProfileVariantId [int] NULL,WidgetGlobalAttributeValueId int)
			 DECLARE @TBL_AttributeDefaultValueList TABLE 
			   (NewWidgetGlobalAttributeValueId int,WidgetGlobalAttributeValueId int,[AttributeValue] [varchar](300),[GlobalAttributeDefaultValueId] int,
			   [GlobalAttributeId] int,MediaId int,WidgetGlobalAttributeValueLocaleId int)


			 DECLARE @TBL_MediaValueList TABLE 
			   (NewWidgetGlobalAttributeValueId int,WidgetGlobalAttributeValueId int,GlobalAttributeId int,
			   MediaId int,MediaPath nvarchar(300),WidgetGlobalAttributeValueLocaleId int)
			 DECLARE @TBL_InsertGlobalEntityValue TABLE 
				([GlobalAttributeId] [int] NULL,GlobalAttributeDefaultValueId [int] NULL,CMSWidgetProfileVariantId [int] NULL,
					WidgetGlobalAttributeValueId int null)
		 	 DECLARE @TBL_GlobalEntityValueDetail TABLE ([GlobalAttributeId] [int] NULL,
				[AttributeCode] [varchar](300),[GlobalAttributeDefaultValueId] [int],[GlobalAttributeValueId] [int],
				[LocaleId] [int],CMSWidgetProfileVariantId [int], [AttributeValue] [nvarchar](max),WidgetGlobalAttributeValueId int,
				NewWidgetGlobalAttributeValueId int,GroupAttributeTypeName [varchar](300))
				
				
				SELECT TOP 1 @LocaleId = LocaleId FROM @GlobalEntityValueDetail;
				SELECT TOP 1 @CMSWidgetProfileVariantId = GlobalEntityValueId FROM @GlobalEntityValueDetail;


				
				 Select @CMSContentWidgetId = CCW.CMSContentWidgetId from ZnodeCMSContentWidget CCW
				 inner join ZnodeCMSWidgetProfileVariant CWPV  on CCW.CMSContentWidgetId = CWPV.CMSContentWidgetId
				 Where CWPV.CMSWidgetProfileVariantId = @CMSWidgetProfileVariantId

				Insert into @TBL_GlobalEntityValueDetail
				([GlobalAttributeId],[AttributeCode],[GlobalAttributeDefaultValueId],
				[GlobalAttributeValueId],[LocaleId],CMSWidgetProfileVariantId,[AttributeValue],GroupAttributeTypeName)
				Select dd.[GlobalAttributeId],dd.[AttributeCode],case when [GlobalAttributeDefaultValueId]=0 then null else 
				[GlobalAttributeDefaultValueId] end [GlobalAttributeDefaultValueId],
				case when [GlobalAttributeValueId]=0 then null else 
				[GlobalAttributeValueId] end [GlobalAttributeValueId],[LocaleId],[GlobalEntityValueId],[AttributeValue],ss.GroupAttributeType
				From @GlobalEntityValueDetail dd
				inner join [View_ZnodeGlobalAttribute] ss on ss.GlobalAttributeId=dd.GlobalAttributeId

				Update ss
				Set ss.WidgetGlobalAttributeValueId=dd.WidgetGlobalAttributeValueId
				From @TBL_GlobalEntityValueDetail ss
				inner join ZnodeWidgetGlobalAttributeValue dd on dd.CMSWidgetProfileVariantId=ss.CMSWidgetProfileVariantId
				and dd.GlobalAttributeId=ss.GlobalAttributeId
				
				insert into @TBL_Widget(CMSWidgetProfileVariantId)
				Select distinct  CMSWidgetProfileVariantId from @TBL_GlobalEntityValueDetail;

                insert into @TBL_DeleteUser
				Select p.CMSWidgetProfileVariantId,a.WidgetGlobalAttributeValueId
				from ZnodeWidgetGlobalAttributeValue a
				inner join @TBL_Widget p on p.CMSWidgetProfileVariantId=a.CMSWidgetProfileVariantId
				Where not exists(select 1 from @TBL_GlobalEntityValueDetail dd 
				where dd.CMSWidgetProfileVariantId=a.CMSWidgetProfileVariantId and dd.GlobalAttributeId=a.GlobalAttributeId)
				
				               
				Delete From ZnodeWidgetGlobalAttributeValueLocale
				WHere exists (select 1 from @TBL_DeleteUser dd 
				Where dd.WidgetGlobalAttributeValueId=ZnodeWidgetGlobalAttributeValueLocale.WidgetGlobalAttributeValueId)

				Delete From ZnodeWidgetGlobalAttributeValue
				WHere exists (select 1 from @TBL_DeleteUser dd 
				Where dd.WidgetGlobalAttributeValueId=ZnodeWidgetGlobalAttributeValue.WidgetGlobalAttributeValueId)
							

				INSERT INTO [dbo].[ZnodeWidgetGlobalAttributeValue]
				([CMSContentWidgetId],[CMSWidgetProfileVariantId],[GlobalAttributeId],[GlobalAttributeDefaultValueId],[CreatedBy],[CreatedDate],
				[ModifiedBy],[ModifiedDate])
				 output Inserted.GlobalAttributeId,inserted.[GlobalAttributeDefaultValueId],inserted.CMSWidgetProfileVariantId,
				 inserted.WidgetGlobalAttributeValueId into @TBL_InsertGlobalEntityValue
				Select @CMSContentWidgetId,[CMSWidgetProfileVariantId],[GlobalAttributeId],[GlobalAttributeDefaultValueId]
				,@UserId [CreatedBy],@GetDate [CreatedDate],@UserId [ModifiedBy],@GetDate [ModifiedDate]
				From @TBL_GlobalEntityValueDetail dd
				WHERE WidgetGlobalAttributeValueId IS NULL				

            
				Update dd
				Set dd.NewWidgetGlobalAttributeValueId=ss.WidgetGlobalAttributeValueId
				From @TBL_GlobalEntityValueDetail dd
				inner join @TBL_InsertGlobalEntityValue ss on dd.[CMSWidgetProfileVariantId]=ss.[CMSWidgetProfileVariantId]
				and dd.GlobalAttributeId=ss.GlobalAttributeId				

				INSERT INTO [dbo].[ZnodeWidgetGlobalAttributeValueLocale]
			   ([WidgetGlobalAttributeValueId],[LocaleId],[AttributeValue],[CreatedBy],[CreatedDate],[ModifiedBy]
			   ,[ModifiedDate])
				Select NewWidgetGlobalAttributeValueId,[LocaleId],[AttributeValue],@UserId [CreatedBy],@GetDate [CreatedDate],
				@UserId [ModifiedBy],@GetDate [ModifiedDate]
				From @TBL_GlobalEntityValueDetail dd
				WHERE NewWidgetGlobalAttributeValueId IS not NULL
				and isnull([AttributeValue],'') <>''    
				and isnull(GroupAttributeTypeName,'') != @MultiSelectGroupAttributeTypeName
				and isnull(GroupAttributeTypeName,'') != @MediaGroupAttributeTypeName		
				
				Update ss
				Set ss.AttributeValue=dd.AttributeValue,ss.ModifiedDate=@GetDate,ss.ModifiedBy=@UserId
				From @TBL_GlobalEntityValueDetail dd
				inner join [dbo].[ZnodeWidgetGlobalAttributeValueLocale] ss on ss.WidgetGlobalAttributeValueId =dd.WidgetGlobalAttributeValueId
				Where isnull(GroupAttributeTypeName,'') != @MultiSelectGroupAttributeTypeName
				and isnull(GroupAttributeTypeName,'') != @MediaGroupAttributeTypeName	

				insert into @TBL_AttributeDefaultValueList
				(NewWidgetGlobalAttributeValueId,WidgetGlobalAttributeValueId,dd.AttributeValue,GlobalAttributeId)
				Select dd.NewWidgetGlobalAttributeValueId, dd.WidgetGlobalAttributeValueId,ss.Item,dd.GlobalAttributeId
				From @TBL_GlobalEntityValueDetail dd
				cross apply dbo.Split(dd.AttributeValue,',') ss
				Where isnull(GroupAttributeTypeName,'') = @MultiSelectGroupAttributeTypeName

				Update dd
				Set dd.GlobalAttributeDefaultValueId=ss.GlobalAttributeDefaultValueId
				from  @TBL_AttributeDefaultValueList DD
				inner join [ZnodeGlobalAttributeDefaultValue] ss on dd.GlobalAttributeId=ss.GlobalAttributeId
				and dd.AttributeValue=ss.AttributeDefaultValueCode

				Update dd
				Set dd.WidgetGlobalAttributeValueLocaleId=ss.WidgetGlobalAttributeValueLocaleId
				from  @TBL_AttributeDefaultValueList DD
				inner join [ZnodeWidgetGlobalAttributeValueLocale] ss on dd.WidgetGlobalAttributeValueId=ss.WidgetGlobalAttributeValueId
				and ss.GlobalAttributeDefaultValueId=dd.GlobalAttributeDefaultValueId

				delete ss
				From @TBL_GlobalEntityValueDetail dd
				inner join [ZnodeWidgetGlobalAttributeValueLocale] ss on dd.WidgetGlobalAttributeValueId=ss.WidgetGlobalAttributeValueId
				Where isnull(GroupAttributeTypeName,'') = @MultiSelectGroupAttributeTypeName
				and not exists (Select 1 from @TBL_AttributeDefaultValueList cc 
				where cc.WidgetGlobalAttributeValueLocaleId=ss.WidgetGlobalAttributeValueLocaleId )

				INSERT INTO [dbo].[ZnodeWidgetGlobalAttributeValueLocale]
			   ([WidgetGlobalAttributeValueId],[LocaleId],GlobalAttributeDefaultValueId,[CreatedBy],[CreatedDate],[ModifiedBy]
			   ,[ModifiedDate])
				Select ss.NewWidgetGlobalAttributeValueId,dd.[LocaleId],ss.GlobalAttributeDefaultValueId,@UserId [CreatedBy],@GetDate [CreatedDate],
				@UserId [ModifiedBy],@GetDate [ModifiedDate]
				From @TBL_GlobalEntityValueDetail dd
				inner join @TBL_AttributeDefaultValueList ss on dd.GlobalAttributeId=ss.GlobalAttributeId
				and ss.NewWidgetGlobalAttributeValueId=dd.NewWidgetGlobalAttributeValueId
				WHERE isnull(dd.GroupAttributeTypeName,'') = @MultiSelectGroupAttributeTypeName

				INSERT INTO [dbo].[ZnodeWidgetGlobalAttributeValueLocale]
			   ([WidgetGlobalAttributeValueId],[LocaleId],GlobalAttributeDefaultValueId,[CreatedBy],[CreatedDate],[ModifiedBy]
			   ,[ModifiedDate])
				Select ss.WidgetGlobalAttributeValueId,dd.[LocaleId],ss.GlobalAttributeDefaultValueId,@UserId [CreatedBy],@GetDate [CreatedDate],
				@UserId [ModifiedBy],@GetDate [ModifiedDate]
				From @TBL_GlobalEntityValueDetail dd
				inner join @TBL_AttributeDefaultValueList ss on dd.GlobalAttributeId=ss.GlobalAttributeId
				and ss.WidgetGlobalAttributeValueId=dd.WidgetGlobalAttributeValueId				
				WHERE isnull(dd.GroupAttributeTypeName,'') = @MultiSelectGroupAttributeTypeName
				and ss.WidgetGlobalAttributeValueLocaleId is null 


				insert into @TBL_MediaValueList
				(NewWidgetGlobalAttributeValueId,WidgetGlobalAttributeValueId,GlobalAttributeId,MediaId)
				Select dd.NewWidgetGlobalAttributeValueId, dd.WidgetGlobalAttributeValueId,GlobalAttributeId,ss.Item 
				From @TBL_GlobalEntityValueDetail dd
				cross apply dbo.Split(dd.AttributeValue,',') ss
				Where isnull(GroupAttributeTypeName,'') = @MediaGroupAttributeTypeName

				Update dd
				Set dd.MediaPath=ss.Path
				from  @TBL_MediaValueList DD
				inner join ZnodeMedia ss on dd.MediaId=ss.MediaId

				Update dd
				Set dd.WidgetGlobalAttributeValueLocaleId=ss.WidgetGlobalAttributeValueLocaleId
				from  @TBL_MediaValueList DD
				inner join [ZnodeWidgetGlobalAttributeValueLocale] ss on dd.WidgetGlobalAttributeValueId=ss.WidgetGlobalAttributeValueId
				and ss.MediaId=dd.MediaId

				delete ss
				From @TBL_GlobalEntityValueDetail dd
				inner join [ZnodeWidgetGlobalAttributeValueLocale] ss on dd.WidgetGlobalAttributeValueId=ss.WidgetGlobalAttributeValueId
				Where isnull(GroupAttributeTypeName,'') = @MediaGroupAttributeTypeName
				and not exists (Select 1 from @TBL_MediaValueList cc 
				where cc.MediaId=ss.MediaId
				and cc.WidgetGlobalAttributeValueId=dd.WidgetGlobalAttributeValueId )

				INSERT INTO [dbo].[ZnodeWidgetGlobalAttributeValueLocale]
			   ([WidgetGlobalAttributeValueId],[LocaleId],MediaId,MediaPath,[CreatedBy],[CreatedDate],[ModifiedBy]
			   ,[ModifiedDate])
				Select ss.NewWidgetGlobalAttributeValueId,dd.[LocaleId],ss.MediaId,ss.MediaPath,@UserId [CreatedBy],@GetDate [CreatedDate],
				@UserId [ModifiedBy],@GetDate [ModifiedDate]
				From @TBL_GlobalEntityValueDetail dd
				inner join @TBL_MediaValueList ss on dd.GlobalAttributeId=ss.GlobalAttributeId
				and ss.NewWidgetGlobalAttributeValueId=dd.NewWidgetGlobalAttributeValueId
				WHERE isnull(dd.GroupAttributeTypeName,'') = @MediaGroupAttributeTypeName

				INSERT INTO [dbo].[ZnodeWidgetGlobalAttributeValueLocale]
			   ([WidgetGlobalAttributeValueId],[LocaleId],MediaId,MediaPath,[CreatedBy],[CreatedDate],[ModifiedBy]
			   ,[ModifiedDate])
				Select ss.WidgetGlobalAttributeValueId,dd.[LocaleId],ss.MediaId,ss.MediaPath,@UserId [CreatedBy],@GetDate [CreatedDate],
				@UserId [ModifiedBy],@GetDate [ModifiedDate]
				From @TBL_GlobalEntityValueDetail dd
				inner join @TBL_MediaValueList ss on dd.GlobalAttributeId=ss.GlobalAttributeId
				and ss.WidgetGlobalAttributeValueId=dd.WidgetGlobalAttributeValueId				
				WHERE isnull(dd.GroupAttributeTypeName,'') = @MediaGroupAttributeTypeName
				and ss.WidgetGlobalAttributeValueLocaleId is null 

				Update dd 
				Set dd.MediaPath=ss.MediaPath
				from [ZnodeWidgetGlobalAttributeValueLocale] dd
                inner join @TBL_MediaValueList ss on 
				ss.WidgetGlobalAttributeValueLocaleId =dd.WidgetGlobalAttributeValueLocaleId										    
		
		     SELECT 0 AS ID,CAST(1 AS BIT) AS Status;    
			   
             COMMIT TRAN A;
         END TRY
         BEGIN CATCH
             SELECT ERROR_MESSAGE()
		     SET @Status = 0;
		     DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(), 
			 @ErrorCall NVARCHAR(MAX)= 'EXEC Znode_ImportInsertUpdateGlobalEntity @UserId = '+CAST(@UserId AS VARCHAR(50))+',@IsNotReturnOutput='+CAST(@IsNotReturnOutput AS VARCHAR(50))+',@Status='+CAST(@Status AS VARCHAR(10));
              			 
             SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                    
			ROLLBACK TRAN A;
             EXEC Znode_InsertProcedureErrorLog
				@ProcedureName = 'Znode_ImportInsertUpdateWidgetGlobalAttributeValue',
				@ErrorInProcedure = @Error_procedure,
				@ErrorMessage = @ErrorMessage,
				@ErrorLine = @ErrorLine,
				@ErrorCall = @ErrorCall;
         END CATCH;
     END;
