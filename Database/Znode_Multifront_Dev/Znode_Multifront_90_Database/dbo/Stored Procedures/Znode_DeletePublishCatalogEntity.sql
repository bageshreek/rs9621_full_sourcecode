﻿CREATE PROCEDURE [dbo].[Znode_DeletePublishCatalogEntity]
(
    @PublishCatalogId  INT = 0 
   ,@UserId int = 0
   ,@IsRevertPublish bit = 0 
   ,@NewGUID nvarchar(500) 

)
AS
/*
	To Remove all publish catalog details from related entities
	Unit Testing : 
	Declare @Status bit 
	Exec [dbo].[ZnodePublishPortalEntity]
     @PortalId  = 1 
	,@LocaleId  = 0 
	,@RevisionState = 'PRODUCTION' 
	,@UserId = 2
	,@Status = @Status 
	--Select @Status 
*/
BEGIN
SET NOCOUNT ON
BEGIN TRY 
Begin Transaction 
--SET NOCOUNT ON
		Declare @Status Bit =0, @IsPreviewEnable  int,  @Batch INT;
		IF object_id('tempdb..[#Tbl_VersionEntity]') IS NOT NULL
			drop table tempdb..#Tbl_VersionEntity
		
		Create Table #Tbl_VersionEntity(PublishCatalogId int , VersionId int , LocaleId int , PublishType varchar(50))
		Insert into #Tbl_VersionEntity (PublishCatalogId , VersionId , LocaleId , PublishType )
			select ZnodeCatalogId , VersionId , LocaleId , RevisionType  from ZnodePublishVersionEntity   where ZnodeCatalogId = @PublishCatalogId 
			and  IsPublishSuccess =0  

		IF object_id('tempdb..[#Tbl_VersionEntityforCatalog]') IS NOT NULL
			drop table tempdb..#Tbl_VersionEntityforCatalog
		
		Create Table #Tbl_VersionEntityforCatalog(PublishCatalogId int , VersionId int , LocaleId int , PublishType varchar(50))
		Insert into #Tbl_VersionEntityforCatalog(PublishCatalogId , VersionId , LocaleId , PublishType )
		select ZnodeCatalogId , VersionId , LocaleId , RevisionType  from ZnodePublishVersionEntity   where ZnodeCatalogId = @PublishCatalogId 
		and  IsPublishSuccess = 0  

		IF object_id('tempdb..[#Tbl_OldVersionEntityforCatalog]') IS NOT NULL
			drop table tempdb..#Tbl_OldVersionEntityforCatalog
		
		Create Table #Tbl_OldVersionEntityforCatalog(PublishCatalogId int , VersionId int , LocaleId int , PublishType varchar(50),IsPublishSuccess bit )
		Insert into #Tbl_OldVersionEntityforCatalog(PublishCatalogId , VersionId , LocaleId , PublishType ,IsPublishSuccess)
		select ZnodeCatalogId , VersionId , LocaleId, RevisionType  ,IsPublishSuccess from ZnodePublishVersionEntity   where ZnodeCatalogId = @PublishCatalogId 
		and  IsPublishSuccess = 1  

		IF object_id('tempdb..[#Tbl_OldVersionEntityforDeletion]') IS NOT NULL
		drop table tempdb..#Tbl_OldVersionEntityforDeletion

		Create Table #Tbl_OldVersionEntityforDeletion(PublishCatalogId int , VersionId int , LocaleId int , PublishType varchar(50),IsPublishSuccess bit )
		Insert into #Tbl_OldVersionEntityforDeletion(PublishCatalogId , VersionId , LocaleId , PublishType ,IsPublishSuccess)
		select ZnodeCatalogId , VersionId , LocaleId, RevisionType  ,IsPublishSuccess from ZnodePublishVersionEntity   where ZnodeCatalogId = @PublishCatalogId 
		and  IsPublishSuccess = 1  and RevisionType  in 
		(Select distinct PublishType from #Tbl_VersionEntity where ZnodeCatalogId = #Tbl_VersionEntity.PublishCatalogId  )

		If @IsRevertPublish = 0 AND @PublishCatalogId > 0 AND Exists (Select Top 1 1 from #Tbl_VersionEntity ) 
		Begin 
			Update ZnodePublishProgressNotifierEntity SET 
			ProgressMark =90, 
			IsCompleted  = 1,
			IsFailed =0
			where  JobId = @NewGUID

			Delete  From ZnodePublishCatalogEntity Where  VersionId NOT IN  (Select VersionId from #Tbl_VersionEntity) AND ZnodeCatalogId = @PublishCatalogId  
				AND VersionId IN  (SELECT VersionId from #Tbl_OldVersionEntityforDeletion) 
			SET @Batch= 1;
			WHILE @Batch > 0
			BEGIN 
				Delete  TOP (10000) From ZnodePublishProductEntity Where  VersionId NOT IN (Select VersionId from #Tbl_VersionEntity)  AND ZnodeCatalogId = @PublishCatalogId 
				AND VersionId IN  (SELECT VersionId from #Tbl_OldVersionEntityforDeletion) 
				SET @Batch= @@ROWCOUNT;
				-- CHECKPOINT;    -- if simple
				-- BACKUP LOG ... -- if full
			END

			Delete  From ZnodePublishAddOnEntity Where  VersionId NOT IN (Select VersionId from #Tbl_VersionEntity)  AND ZnodeCatalogId = @PublishCatalogId 
			AND VersionId IN  (SELECT VersionId from #Tbl_OldVersionEntityforDeletion) 

			
			Delete  From ZnodePublishCategoryEntity Where  VersionId NOT IN  (Select VersionId from #Tbl_VersionEntity) AND ZnodeCatalogId = @PublishCatalogId 
			AND VersionId IN  (SELECT VersionId from #Tbl_OldVersionEntityforDeletion) 

			Delete  From ZnodePublishBundleProductEntity Where  VersionId NOT IN (Select VersionId from #Tbl_VersionEntity) AND ZnodeCatalogId = @PublishCatalogId 
			AND VersionId IN  (SELECT VersionId from #Tbl_OldVersionEntityforDeletion) 

			Delete  From ZnodePublishGroupProductEntity Where  VersionId NOT IN (Select VersionId from #Tbl_VersionEntity) AND ZnodeCatalogId = @PublishCatalogId 
			AND VersionId IN  (SELECT VersionId from #Tbl_OldVersionEntityforDeletion) 

			Delete  From ZnodePublishConfigurableProductEntity Where  VersionId NOT IN (Select VersionId from #Tbl_VersionEntity) AND ZnodeCatalogId = @PublishCatalogId 
			AND VersionId IN  (SELECT VersionId from #Tbl_OldVersionEntityforDeletion) 

			Delete  From ZnodePublishSEOEntity Where CMSSEOTypeId in (1,2,4) and 
			VersionId NOT IN  (Select VersionId from #Tbl_VersionEntity) 
			AND VersionId IN  (SELECT VersionId from #Tbl_OldVersionEntityforDeletion) 

			Delete  From ZnodePublishVersionEntity Where  VersionId NOT IN (Select VersionId from #Tbl_VersionEntity) AND ZnodeCatalogId = @PublishCatalogId 
			AND VersionId IN  (SELECT VersionId from #Tbl_OldVersionEntityforDeletion) 

			Update ZnodePublishVersionEntity  SET IsPublishSuccess= 1 where IsPublishSuccess = 0
			and ZnodeCatalogId  = @PublishCatalogId   

			
			Update ZnodePublishCatalogLog SET IsCatalogPublished =1,  PublishStateId = DBO.Fn_GetPublishStateIdForPreview(), ModifiedDate = Getdate()  where  PublishCatalogLogId in 
			(Select VersionId from #Tbl_VersionEntity Where PublishType = 'PREVIEW' )
			Update ZnodePublishCatalogLog SET IsCatalogPublished =1,  PublishStateId = DBO.Fn_GetPublishStateIdForPublish()  , ModifiedDate = Getdate() where  PublishCatalogLogId in
			(Select VersionId from #Tbl_VersionEntity Where PublishType = 'PRODUCTION' )

			Insert into ZnodePublishPreviewLogEntity
			(VersionId,PublishStartTime,IsDisposed,SourcePublishState,EntityId,EntityType,LogMessage,LogCreatedDate,PreviousVersionId,LocaleId,LocaleDisplayValue)
				Select A.VersionId,NULL,NULL,A.PublishType,@PublishCatalogId,'catalog','catalog has been published successfully' , Getdate(),  
				(select TOP 1 VersionId   from #Tbl_OldVersionEntityforCatalog where LocaleId = A.LocaleId AND PublishType = A.PublishType
				AND  PublishCatalogId = A.PublishCatalogId and  IsPublishSuccess =1 ),
				A.LocaleId,B.Name
				from #Tbl_VersionEntity  A  Inner join ZnodeLocale B on A.LocaleId = B.LocaleId 

			Update ZnodePublishProgressNotifierEntity SET 
			ProgressMark =100, 
			IsCompleted  = 1,
			IsFailed =0
			where  JobId = @NewGUID

			--UPDATE ZnodePimProduct SET IsProductPublish = 1,PublishStateId =  DBO.Fn_GetPublishStateIdForPublish()  
			--WHERE EXISTS (SELECT TOP 1 1 FROM ZnodePublishProduct ZPP WHERE ZPP.PimProductId = ZnodePimProduct.PimProductId AND ZPP.PublishCatalogId = @PublishCatalogId)

			--UPDATE ZnodePublishCatalogLog 
			--SET IsProductPublished = 1 
			--,PublishProductId = (SELECT  COUNT(DISTINCT PublishProductId) FROM ZnodePublishCategoryProduct ZPP WHERE ZPP.PublishCatalogId = ZnodePublishCatalogLog.PublishCatalogId AND ZPP.PublishCategoryId IS NOT NULL) 
			--WHERE EXISTS (SELECT TOP 1 1 FROM #Tbl_VersionEntity TY  WHERE  TY.VersionId =ZnodePublishCatalogLog.PublishCatalogLogId )  

		End
		Else If @IsRevertPublish = 1 AND @PublishCatalogId > 0 
		Begin 
			
			Delete  From ZnodePublishCatalogEntity Where  VersionId in (Select VersionId from #Tbl_VersionEntityforCatalog) AND ZnodeCatalogId = @PublishCatalogId 
			SET @Batch= 1;
			WHILE @Batch > 0
			BEGIN 
				Delete  TOP (10000) From ZnodePublishProductEntity Where  VersionId in (Select VersionId from #Tbl_VersionEntityforCatalog) AND ZnodeCatalogId = @PublishCatalogId 
				SET @Batch= @@ROWCOUNT;
				-- CHECKPOINT;    -- if simple
				-- BACKUP LOG ... -- if full
			END
			Delete  From ZnodePublishAddOnEntity Where  VersionId in (Select VersionId from #Tbl_VersionEntityforCatalog) AND ZnodeCatalogId = @PublishCatalogId 
			Delete  From ZnodePublishCategoryEntity Where  VersionId in (Select VersionId from #Tbl_VersionEntityforCatalog) AND ZnodeCatalogId = @PublishCatalogId 
			Delete  From ZnodePublishBundleProductEntity Where  VersionId in (Select VersionId from #Tbl_VersionEntityforCatalog) AND ZnodeCatalogId = @PublishCatalogId 
			Delete  From ZnodePublishGroupProductEntity Where  VersionId in (Select VersionId from #Tbl_VersionEntityforCatalog) AND ZnodeCatalogId = @PublishCatalogId 
			Delete  From ZnodePublishConfigurableProductEntity Where  VersionId in (Select VersionId from #Tbl_VersionEntityforCatalog) AND ZnodeCatalogId = @PublishCatalogId 
			Delete  From ZnodePublishSEOEntity Where CMSSEOTypeId in (1,2,4) and  VersionId in (Select VersionId from #Tbl_VersionEntityforCatalog) 
			--AND PortalId in 
			--	(Select ZPC.PortalId  from ZnodePublishVersionEntity  ZPVE inner join ZnodePortalCatalog  
			--		ZPC ON ZPVE.ZnodeCatalogId = ZPC.PublishCatalogId where  (ZPC.PublishCatalogId = @PublishCatalogId ))

			Delete  From ZnodePublishVersionEntity Where  VersionId in (Select VersionId from #Tbl_VersionEntityforCatalog) AND ZnodeCatalogId = @PublishCatalogId 

			INSERT INTO ZnodePublishCatalogErrorLogEntity
			(EntityName,ErrorDescription,ProcessStatus,CreatedDate,CreatedBy,VersionId)
			SELECT 'Elastic-index genration get failed : ', '' , 'Fail' , Getdate(), 
			@UserId , '' 
			
			Update ZnodePublishCatalogLog SET PublishStateId = DBO.Fn_GetPublishStateIdForPublishFailed() ,
			IsCatalogPublished = 0  , ModifiedDate = Getdate()
			where PublishCatalogLogId in  (Select VersionId from #Tbl_VersionEntityforCatalog)
			Update ZnodePublishProgressNotifierEntity SET 
			ProgressMark =100, 
			IsCompleted  = 1,
			IsFailed =1
			where  JobId = @NewGUID

		END 
		Else If @IsRevertPublish = 1 AND Isnull(@PublishCatalogId,0) = 0 
		Begin 
			INSERT INTO ZnodePublishCatalogErrorLogEntity
			(EntityName,ErrorDescription,ProcessStatus,CreatedDate,CreatedBy,VersionId)
			SELECT 'Publish failed, Check connection failuer ', '' , 'Fail' , Getdate(), @UserId , '' 
			
			Update ZnodePublishCatalogLog SET PublishStateId = DBO.Fn_GetPublishStateIdForPublishFailed() ,
			IsCatalogPublished =0, IsCategoryPublished = 0 ,IsProductPublished = 0 , ModifiedDate = Getdate()
			 where 
			PublishStateId  = 6 
						
			Update ZnodePublishProgressNotifierEntity SET 
			ProgressMark =80, 
			IsCompleted  = 0,
			IsFailed =1
			where  JobId = @NewGUID

		end

		SET @Status = 1
		Commit Transaction 
		SELECT @PublishCatalogId AS id,@Status AS Status;   
END TRY 
BEGIN CATCH 
	SET @Status =0  
	 SELECT 1 AS ID,@Status AS Status;   
	 Rollback transaction
	 DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), 
		@ErrorLine VARCHAR(100)= ERROR_LINE(),
		@ErrorCall NVARCHAR(MAX)= 'EXEC Znode_PublishCatalogEntity 
		@PublishCatalogId = '+CAST(@PublishCatalogId  AS VARCHAR	(max))+',@UserId='+CAST(@UserId AS VARCHAR(50))+',@IsRevertPublish='+CAST(@IsRevertPublish AS VARCHAR(10))
		+',@UserId = ' + CAST(@UserId AS varchar(20));	SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                    
	
	EXEC Znode_InsertProcedureErrorLog
		@ProcedureName = 'Znode_PublishCatalogEntity',
		@ErrorInProcedure = @Error_procedure,
		@ErrorMessage = @ErrorMessage,
		@ErrorLine = @ErrorLine,
		@ErrorCall = @ErrorCall;
END CATCH
END