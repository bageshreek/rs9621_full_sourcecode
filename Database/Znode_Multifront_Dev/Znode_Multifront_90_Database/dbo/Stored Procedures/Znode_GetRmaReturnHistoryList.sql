﻿CREATE PROCEDURE [dbo].[Znode_GetRmaReturnHistoryList]
(
	@WhereClause	varchar(max),
    @Rows			INT           = 100,
    @PageNo			INT           = 1,
    @Order_By		VARCHAR(1000) = '',
	@IsAdmin        BIT=0,
	@RowCount		INT        = 0 OUT,
	@ReturnDate     varchar(500) = ''
)
as
/* exec Znode_GetRmaReturnHistoryList @WhereClause = 'ReturnNumber = ''ROMA-200630-183358-954''',@IsAdmin=0,@Order_By='ReturnDate asc',@ReturnDate='returndate between ''06/05/2020 03:49 am'' and ''07/19/2020 03:49 pm''' */
begin
	SET NOCOUNT ON
	BEGIN TRY
		
		DECLARE @SQL NVarchar(max)
		Declare @PaginationWhereClause VARCHAR(300)= dbo.Fn_GetRowsForPagination(@PageNo, @Rows, ' WHERE RowId')
		Declare @PriceRoundOff varchar(10)
		select @PriceRoundOff = FeatureValues from Znodeglobalsetting where FeatureName = 'PriceRoundOff'

		if @IsAdmin = 0
		begin
		    ----Get all data for webstore
			set @SQL =
			'
			select ZRRD.RmaReturnDetailsId,ZRRD.ReturnNumber, ZRRS.ReturnStateName  as ReturnStatus,ZRRD.ReturnDate,ZRRD.CreatedDate,
			ZRRD.TotalExpectedReturnQuantity, isnull(ZU.FirstNAme,'''')+'' ''+isnull(ZU.LastName,'''') as UserName,ZU.Email as EmailId, ZP.StoreName, round(ZRRD.TotalReturnAmount,'+@PriceRoundOff+') TotalReturnAmount,
			ZRRD.PortalId , ZRRD.UserID,ZRRD.ModifiedDate,ZRRD.CurrencyCode, ZRRD.CultureCode
			into #Cte_RetuenOrder
			from ZnodeRmaReturnDetails ZRRD
			inner join ZnodeUser ZU ON ZRRD.UserId = ZU.UserId
			inner join ZnodePortal ZP ON ZRRD.PortalId = ZP.PortalId
			inner join ZnodeRmaReturnState ZRRS on ZRRD.RmaReturnStateId = ZRRS.RmaReturnStateId

			CREATE TABLE #Cte_RetuenOrder_WhereClause( RmaReturnDetailsId int, ReturnNumber nvarchar(200),  ReturnStatus nvarchar(200), ReturnDate datetime, TotalExpectedReturnQuantity numeric(28,6), UserName  varchar(300), EmailId  varchar(300), StoreName  varchar(300), TotalReturnAmount numeric(28,6), ModifiedDate datetime, CurrencyCode varchar(300), CultureCode varchar(300), RowId int)
			if ('''+@Order_By+''') = '''' 
			begin
				----To get the not subitted records at top for webstore
				insert into #Cte_RetuenOrder_WhereClause
				select RmaReturnDetailsId, ReturnNumber,  ReturnStatus, ReturnDate, TotalExpectedReturnQuantity, UserName, EmailId, StoreName, TotalReturnAmount, ModifiedDate, CurrencyCode, CultureCode
					   , Row_Number()Over('+dbo.Fn_GetOrderByClause(@Order_By, 'CreatedDate DESC')+',ReturnDate DESC) RowId 
				from #Cte_RetuenOrder
				where ReturnDate is null '+dbo.Fn_GetWhereClause(@WhereClause, ' AND ') +dbo.Fn_GetWhereClause(@ReturnDate, ' AND ')+'

				insert into #Cte_RetuenOrder_WhereClause
				select RmaReturnDetailsId, ReturnNumber,  ReturnStatus, ReturnDate, TotalExpectedReturnQuantity, UserName, EmailId, StoreName, TotalReturnAmount, ModifiedDate, CurrencyCode, CultureCode
					   , isnull((select max(RowId) from #Cte_RetuenOrder_WhereClause),0)+Row_Number()Over('+dbo.Fn_GetOrderByClause(@Order_By, 'ModifiedDate DESC')+',ReturnDate DESC) RowId
				from #Cte_RetuenOrder
				where ReturnDate is not null'+dbo.Fn_GetWhereClause(@WhereClause, ' AND ') +dbo.Fn_GetWhereClause(@ReturnDate, ' AND ')+'
			end
			else
			begin
				insert into #Cte_RetuenOrder_WhereClause
				select RmaReturnDetailsId, ReturnNumber,  ReturnStatus, ReturnDate, TotalExpectedReturnQuantity, UserName, EmailId, StoreName, TotalReturnAmount, ModifiedDate, CurrencyCode, CultureCode
					   , Row_Number()Over('+dbo.Fn_GetOrderByClause(@Order_By, 'ReturnDate DESC')+',ModifiedDate DESC) RowId
				from #Cte_RetuenOrder 
				where 1 = 1 '+dbo.Fn_GetWhereClause(@WhereClause, ' AND ') +dbo.Fn_GetWhereClause(@ReturnDate, ' AND ')+'
			end

			select RmaReturnDetailsId, ReturnNumber,  ReturnStatus, ReturnDate, TotalExpectedReturnQuantity, UserName, EmailId, StoreName, TotalReturnAmount, ModifiedDate, CurrencyCode, CultureCode, RowId 
			into #RetuenOrder
			from #Cte_RetuenOrder_WhereClause
			'+@PaginationWhereClause+' '+dbo.Fn_GetOrderByClause(@Order_By, 'ModifiedDate DESC' )
			+' select RmaReturnDetailsId, ReturnNumber,   dbo.Fn_CamelCase(ReturnStatus) as ReturnStatus, ReturnDate, TotalExpectedReturnQuantity, UserName, EmailId, StoreName, TotalReturnAmount, ModifiedDate, CurrencyCode, CultureCode
			from #RetuenOrder
			order by RowId 
			SET @Count= ISNULL((SELECT  Count(1) FROM #Cte_RetuenOrder_WhereClause ),0)';
		
		end
		else
		begin
		   ----Get all data for admin except Not submitted
			set @SQL =
			'
			select ZRRD.RmaReturnDetailsId,ZRRD.ReturnNumber, ZRRS.ReturnStateName  as ReturnStatus,ZRRD.ReturnDate,
			ZRRD.TotalExpectedReturnQuantity, isnull(ZU.FirstNAme,'''')+'' ''+isnull(ZU.LastName,'''') as UserName,ZU.Email as EmailId, ZP.StoreName, round(ZRRD.TotalReturnAmount,'+@PriceRoundOff+') TotalReturnAmount,
			ZRRD.PortalId , ZRRD.UserID,ZRRD.ModifiedDate, ZRRD.CurrencyCode, ZRRD.CultureCode
			into #Cte_RetuenOrder
			from ZnodeRmaReturnDetails ZRRD
			inner join ZnodeUser ZU ON ZRRD.UserId = ZU.UserId
			inner join ZnodePortal ZP ON ZRRD.PortalId = ZP.PortalId 
			inner join ZnodeRmaReturnState ZRRS on ZRRD.RmaReturnStateId = ZRRS.RmaReturnStateId
			where isnull(ZRRD.RmaReturnStateId,0) not in (select isnull(RmaReturnStateId,0) from ZnodeRmaReturnState where ReturnStateName = ''Not Submitted'')
		

			select RmaReturnDetailsId, ReturnNumber,  ReturnStatus, ReturnDate, TotalExpectedReturnQuantity, UserName, EmailId, StoreName, TotalReturnAmount,ModifiedDate, CurrencyCode, CultureCode
			, Row_Number()Over('+dbo.Fn_GetOrderByClause(@Order_By, 'ReturnDate DESC')+',ModifiedDate DESC) RowId
			into #Cte_RetuenOrder_WhereClause
			from #Cte_RetuenOrder
			where 1 = 1 '+dbo.Fn_GetWhereClause(@WhereClause, ' AND ') +dbo.Fn_GetWhereClause(@ReturnDate, ' AND ')+'

			select RmaReturnDetailsId, ReturnNumber,  ReturnStatus, ReturnDate, TotalExpectedReturnQuantity, UserName, EmailId, StoreName, TotalReturnAmount, ModifiedDate, CurrencyCode, CultureCode, RowId
			into #RetuenOrder
			from #Cte_RetuenOrder_WhereClause
			'+@PaginationWhereClause+' '+dbo.Fn_GetOrderByClause(@Order_By, 'ReturnDate DESC' )
			+' select RmaReturnDetailsId, ReturnNumber,  dbo.Fn_CamelCase(ReturnStatus) as ReturnStatus, ReturnDate, TotalExpectedReturnQuantity, UserName, EmailId, StoreName, TotalReturnAmount, ModifiedDate, CurrencyCode, CultureCode
			from #RetuenOrder
			order by RowId 
			
			SET @Count= ISNULL((SELECT  Count(1) FROM #Cte_RetuenOrder_WhereClause ),0)';
		end

		print @sql

		EXEC SP_executesql 
				@SQL,
				N'@Count INT OUT',
				@Count = @RowCount OUT;

	END TRY
         BEGIN CATCH
           
		     DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(), @ErrorCall NVARCHAR(MAX)= 'EXEC Znode_GetRmaReturnHistoryList @WhereClause = '+CAST(@WhereClause AS varchar(10))+',@Order_By='+cast(@Order_By as varchar(10));

             EXEC Znode_InsertProcedureErrorLog
				@ProcedureName = 'Znode_GetRmaReturnHistoryList',
				@ErrorInProcedure = @Error_procedure,
				@ErrorMessage = @ErrorMessage,
				@ErrorLine = @ErrorLine,
				@ErrorCall = @ErrorCall;
         END CATCH;
end