﻿CREATE PROCEDURE [dbo].[Znode_PublishSingleProductEntity]
(
    @PimProductId TransferId READONLY
   ,@RevisionType   Varchar(30) = ''
   ,@UserId int = 0
   ,@IsAutoPublish bit = 0 
   ,@ImportGUID Varchar(500) = '' 
)
AS
/*
	To publish all catalog product and their details
	Unit Testing : 
	*/
BEGIN
BEGIN TRY 
SET NOCOUNT ON
	Declare @Status Bit =0 
	Declare @Type varchar(50) = '',	@CMSSEOCode varchar(300);
	SET @Status = 1 

    DECLARE @PimProductId_Editable TransferId 
	DECLARE @PimAssociatedProductId TransferId  
			
	DECLARE @TBL_PublishIds TABLE (PublishProductId INT , PimProductId INT , PublishCatalogId INT)
	INSERT INTO @TBL_PublishIds 
	EXEC [dbo].[Znode_InsertPublishProductIds] @PublishCatalogId= 0 ,@userid =@UserId ,@PimProductId = @PimProductId 

	DECLARE @TBL_PublishCatalogId TABLE(PublishCatalogId INT,PublishProductId INT,PimProductId  INT , VersionId INT,LocaleId INT ,RevisionType Varchar(50) );
		If  (@RevisionType like '%Preview%'  OR @RevisionType like '%Production%'  ) 
		Begin

			INSERT INTO @TBL_PublishCatalogId 
			SELECT Distinct ZPP.PublishCatalogId , ZPP.PublishProductId,PimProductId,ZPCP.VersionId, ZPCP.LocaleId  ,ZPCP.RevisionType
					 FROM ZnodePublishProduct ZPP 
					 Inner join ZnodePublishVersionEntity ZPCP ON (ZPCP.ZnodeCatalogId  = ZPP.PublishCatalogId AND ZPCP.RevisionType = 'PREVIEW')
					 WHERE (EXISTS (SELECT TOP 1 1 FROM @TBL_PublishIds SP WHERE SP.PimProductId = ZPP.PimProductId  ))
					 AND ZPCP.IsPublishSuccess = 1 
		End
		If (@RevisionType like '%Production%' OR @RevisionType = 'None')
			INSERT INTO @TBL_PublishCatalogId 
			SELECT Distinct ZPP.PublishCatalogId , ZPP.PublishProductId,PimProductId,ZPCP.VersionId, ZPCP.LocaleId  ,ZPCP.RevisionType
					 FROM ZnodePublishProduct ZPP 
					 Inner join ZnodePublishVersionEntity ZPCP ON (ZPCP.ZnodeCatalogId  = ZPP.PublishCatalogId AND ZPCP.RevisionType = 'PRODUCTION')
					 WHERE (EXISTS (SELECT TOP 1 1 FROM @TBL_PublishIds SP WHERE SP.PimProductId = ZPP.PimProductId  ))
					 AND ZPCP.IsPublishSuccess = 1 

			Truncate table ZnodePublishSingleProductErrorLogEntity 
	Begin Transaction 
	If @Type = 'ZnodePublishSingleProductEntity' OR @Type = ''
	Begin
	    	
		INSERT INTO @PimProductId_Editable
		SELECT distinct PimProductId FROM @TBL_PublishIds
		-- initiate single product publish 
		EXEC Znode_GetPublishSingleProductJson
		 @PublishCatalogId = 0,
		 @VersionId = 0 ,
		 @PimProductId = @PimProductId_Editable,
		 @UserId =@UserId ,
		 @TokenId = null ,
		 @RevisionType = @RevisionType , 
		 @Status  = @Status out 
	

		If @Status = 0 
			Rollback Transaction 
		
		INSERT INTO ZnodePublishSingleProductErrorLogEntity(EntityName,ErrorDescription,ProcessStatus,CreatedDate,CreatedBy,VersionId)
		SELECT 'ZnodePublishProductEntity', '', Case when Isnull(@Status,0) = 0 then 'Fail' Else 'Success' end , Getdate(), 
		@UserId , '' 

		If @Status  = 0 
		Begin
			SELECT 1 AS ID,@Status AS Status;
			Return 0 
		End
	End
	If @Type = 'ZnodePublishAddOnEntity' OR @Type = ''
	Begin
		Exec [Znode_GetPublishAssociatedAddonsJson]
				@PublishCatalogId = 0 ,
				@PimProductId   = @PimProductId,
				@UserId =@UserId,														 
				@VersionIdString = '',
				@Status	 =@Status Out,
				@RevisionType= @RevisionType 

			If @Status  = 0 
				Rollback Transaction 
		
			INSERT INTO ZnodePublishSingleProductErrorLogEntity(EntityName,ErrorDescription,ProcessStatus,CreatedDate,CreatedBy,VersionId)
			SELECT 'ZnodePublishAddOnEntity', '', Case when Isnull(@Status,0) = 0 then 'Fail' Else 'Success' end , Getdate(), 
			@UserId ,''

			If @Status  = 0 
			Begin
				SELECT 1 AS ID,@Status AS Status;
				Return 0 
			End
	End 
				
	If @Type = 'BundleAssociatedProducts' OR @Type = ''
	Begin
			Exec [Znode_GetPublishAssociatedProductsJson]

				@PublishCatalogId = 0 ,
				@PimProductId     = @PimProductId,
				@UserId =@UserId,														 
				@VersionIdString = '',
				@Status	 =@Status Out,
				@RevisionType= @RevisionType ,
				@ProductType = 'BundleProduct'

			If @Status  = 0 
				Rollback Transaction 
		
			INSERT INTO ZnodePublishSingleProductErrorLogEntity(EntityName,ErrorDescription,ProcessStatus,CreatedDate,CreatedBy,VersionId)
			SELECT 'BundleAssociatedProducts', '', Case when Isnull(@Status,0) = 0 then 'Fail' Else 'Success' end , Getdate(), 
			@UserId ,''

			If @Status  = 0 
			Begin
				SELECT 1 AS ID,@Status AS Status;
				Return 0 
			End
	End 
	If @Type = 'GroupedAssociatedProducts' OR @Type = ''
	Begin
			Exec [Znode_GetPublishAssociatedProductsJson]

				@PublishCatalogId = 0 ,
				@PimProductId     = @PimProductId,
				@UserId =@UserId,														 
				@VersionIdString = '',
				@Status	 =@Status Out,
				@RevisionType= @RevisionType ,
				@ProductType = 'GroupedProduct'


			If @Status  = 0 
				Rollback Transaction 
		
			INSERT INTO ZnodePublishSingleProductErrorLogEntity(EntityName,ErrorDescription,ProcessStatus,CreatedDate,CreatedBy,VersionId)
			SELECT 'GroupedAssociatedProducts', '', Case when Isnull(@Status,0) = 0 then 'Fail' Else 'Success' end , Getdate(), 
			@UserId ,''

			If @Status  = 0 
			Begin
				SELECT 1 AS ID,@Status AS Status;
				Return 0 
			End
	End 
	If @Type = 'ConfigurableAssociatedProducts' OR @Type = ''
	Begin
			Exec [Znode_GetPublishAssociatedProductsJson]

				@PublishCatalogId = 0 ,
				@PimProductId     = @PimProductId,
				@UserId =@UserId,														 
				@VersionIdString = '',
				@Status	 =@Status Out,
				@RevisionType= @RevisionType ,
				@ProductType ='ConfigurableProduct'
			If @Status  = 0 
				Rollback Transaction 
		
			INSERT INTO ZnodePublishSingleProductErrorLogEntity(EntityName,ErrorDescription,ProcessStatus,CreatedDate,CreatedBy,VersionId)
			SELECT 'ConfigurableAssociatedProducts', '', Case when Isnull(@Status,0) = 0 then 'Fail' Else 'Success' end , Getdate(), 
			@UserId ,''

			If @Status  = 0 
			Begin
				SELECT 1 AS ID,@Status AS Status;
				Return 0 
			End
	End 
	If @Type = 'ZnodePublishSEOEntity' OR @Type = ''
	Begin
			SELECT TPC.PublishProductId  ,TPC.PublishCatalogId ,VersionId,TPC.LocaleId ,PP.SKU, ZCSD.SEOUrl,ZCSD.PortalId 
			INTO #SEODetails 
			FROM @TBL_PublishCatalogId TPC
			INNER JOIN ZnodePublishProductDetail PP ON (PP.PublishProductId = TPC.PublishProductId AND TPC.LocaleId = PP.LocaleId)
			inner join ZnodePublishCatalog ZPC ON TPC.PublishCatalogId = ZPC.PublishCatalogId
			left join ZnodePortalCatalog ZPCat on ZPC.PublishCatalogId = ZPCat.PublishCatalogId
			left join ZnodeCMSSEODetail ZCSD on PP.SKU = ZCSD.SEOCode and ZPCat.PortalId = ZCSD.PortalId 
				AND ZCSD.CMSSEOTypeId = (select top 1 CMSSEOTypeId from ZnodeCMSSEOType where Name = 'Product')
			--WHERE ZCSD.CMSSEODetailId is not null 
			GROUP BY TPC.PublishProductId  ,TPC.PublishCatalogId ,VersionId,TPC.LocaleId,PP.SKU, ZCSD.SEOUrl,ZCSD.PortalId 
			Declare @Seo_VersionId Varchar(10 )
			,@Seo_LocaleId int 
			,@Seo_SKU Varchar(300)
			,@Seo_PortalId int 

			DECLARE Cr_Attribute CURSOR LOCAL FAST_FORWARD
			FOR SELECT VersionId,LocaleId ,SKU,PortalId from #SEODetails    
			OPEN Cr_Attribute;
			FETCH NEXT FROM Cr_Attribute INTO @Seo_VersionId,@Seo_LocaleId ,@Seo_SKU,@Seo_PortalId ;
			WHILE @@FETCH_STATUS = 0
			BEGIN
				EXEC [Znode_SetPublishSEOEntity]
				  @CMSSEOTypeId = '1' 
				 ,@UserId  = @UserId 
				 ,@Status = @Status  OUTPUT 
				 ,@IsCatalogPublish = 0  
				 ,@IsSingleProductPublish = 1 
				 ,@VersionIdString = @Seo_VersionId  
				 ,@CMSSEOCode = @Seo_SKU
				 ,@PortalId  =@Seo_PortalId 
				 ,@LocaleId = @Seo_LocaleId
				 ,@RevisionState = @RevisionType 
				 
				 If @Status  = 0 
					Rollback Transaction 
		
				INSERT INTO ZnodePublishSingleProductErrorLogEntity(EntityName,ErrorDescription,ProcessStatus,CreatedDate,CreatedBy,VersionId)
				SELECT 'ZnodePublishSingleProductEntity', '', Case when Isnull(@Status,0) = 0 then 'Fail' Else 'Success' end , Getdate(), 
				@UserId ,''
				If @Status  = 0 
				Begin
					SELECT 1 AS ID,@Status AS Status;
					Return 0 
				End
			FETCH NEXT FROM Cr_Attribute INTO @Seo_VersionId,@Seo_LocaleId ,@Seo_SKU,@Seo_PortalId ;
			END;
			CLOSE Cr_Attribute;
			DEALLOCATE Cr_Attribute;
	End 

	IF Exists (select TOP 1 1  from ZnodePublishSingleProductErrorLogEntity where  ProcessStatus = 'Fail') 
		Begin
			Rollback transaction
			SET @Status  =0 
			SELECT 1 AS ID,@Status AS Status;
			INSERT INTO ZnodePublishSingleProductErrorLogEntity
			(EntityName,ErrorDescription,ProcessStatus,CreatedDate,CreatedBy,VersionId)
			SELECT 'ZnodePublishPortalEntity', '' , 'Fail' , Getdate(), 
			@UserId ,''
			Return 0 
		End
		

	SET @Status = 1
	Commit Transaction 
	Insert into ZnodePublishPreviewLogEntity
			(VersionId,PublishStartTime,IsDisposed,SourcePublishState,EntityId,EntityType,LogMessage,LogCreatedDate,PreviousVersionId,LocaleId,LocaleDisplayValue)
			Select A.VersionId,NULL,NULL,A.RevisionType,0,'product','product has been published successfully' , Getdate(),  
			0,
			A.LocaleId,B.Name
			from ZnodePublishVersionEntity A  Inner join ZnodeLocale B on A.LocaleId = B.LocaleId 
			where A.ZnodeCatalogId in (Select distinct PublishCatalogId from @TBL_PublishCatalogId)

	If @RevisionType = 'Preview'
		UPDATE ZnodePimProduct 
			SET PublishStateId =  DBO.Fn_GetPublishStateIdForPreview() 
			WHERE EXISTS (SELECT TOP 1 1 FROM @TBL_PublishCatalogId ZPP WHERE ZPP.PimProductId = ZnodePimProduct.PimProductId)
	else 	
		UPDATE ZnodePimProduct 
			SET PublishStateId =  DBO.Fn_GetPublishStateIdForPublish() 
			WHERE EXISTS (SELECT TOP 1 1 FROM @TBL_PublishCatalogId ZPP WHERE ZPP.PimProductId = ZnodePimProduct.PimProductId)
	
	If  @IsAutoPublish = 1 
	Begin
		Delete from ZnodePublishProductRecordset where ImportGUID =@ImportGUID  
		Insert into ZnodePublishProductRecordset	
		(PublishCatalogId,PublishProductId,PimProductId,VersionId,LocaleId,RevisionType,ImportGUID)		
		Select  PublishCatalogId,PublishProductId,PimProductId,VersionId,LocaleId,RevisionType,@ImportGUID FROM @TBL_PublishCatalogId  
	End
	Else
	If  @IsAutoPublish = 0 
	Begin
		Select B.* from @TBL_PublishCatalogId  A Inner join ZnodePublishProductEntity B on 
		A.PublishProductId = B.ZnodeProductId 
		and a.VersionId = b.VersionId
		and a.PublishCatalogId = b.ZnodeCatalogId 
		Select * from #SEODetails     		
	end 
	SELECT 0 AS id,@Status AS Status;   

END TRY 
BEGIN CATCH 
	SET @Status =0  
	 SELECT 1 AS ID,@Status AS Status;   
	 Rollback transaction
	 DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), 
		@ErrorLine VARCHAR(100)= ERROR_LINE(),
		@ErrorCall NVARCHAR(MAX)= 'EXEC Znode_PublishSingleProductEntity
		@PimProductIds = '',@UserId='+CAST(@UserId AS VARCHAR(50))+',@RevisionType='+CAST(@RevisionType AS VARCHAR(10))
			
	INSERT INTO ZnodePublishSingleProductErrorLogEntity
	(EntityName,ErrorDescription,ProcessStatus,CreatedDate,CreatedBy,VersionId)
	SELECT 'PublishSingleProductEntity', '' + isnull(@ErrorMessage,'') , 'Fail' , Getdate(), 
	@UserId ,''


	EXEC Znode_InsertProcedureErrorLog
		@ProcedureName = 'Znode_PublishSingleProductEntity',
		@ErrorInProcedure = @Error_procedure,
		@ErrorMessage = @ErrorMessage,
		@ErrorLine = @ErrorLine,
		@ErrorCall = @ErrorCall;
END CATCH
END