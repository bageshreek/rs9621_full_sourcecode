﻿insert into ZnodeMessage(MessageCode,MessageType,MessageName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 65,'Date','ExpirationDate should be greater than CurrentDate',2,getdate(),2,getdate()
where not exists(select * from ZnodeMessage where MessageCode = 65)

insert into ZnodeMessage(MessageCode,MessageType,MessageName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 66,'Date','Amount should be greater than RemainingAmount',2,getdate(),2,getdate()
where not exists(select * from ZnodeMessage where MessageCode = 66)

insert into ZnodeMessage(MessageCode,MessageType,MessageName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 67,'Other','InValid UserName',2,getdate(),2,getdate()
where not exists(select * from ZnodeMessage where MessageCode = 67)

update ZnodeMessage set messagename = 'The value should be a future date post Start Date'
where MessageCode = 65

insert into ZnodeMessage(MessageCode,MessageType,MessageName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 68,'Other','The value should be either True or False',2,getdate(),2,getdate()
where not exists(select * from ZnodeMessage where MessageCode = 68)

insert into ZnodeMessage(MessageCode,MessageType,MessageName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 69,'Number','The value should be the same as the Voucher Amount.',2,getdate(),2,getdate()
where not exists(select * from ZnodeMessage where MessageCode = 69)

insert into ZnodeMessage(MessageCode,MessageType,MessageName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 70,'Number','The value is not associated with any existing Store.',2,getdate(),2,getdate()
where not exists(select * from ZnodeMessage where MessageCode = 70)
go
insert into ZnodeMessage(MessageCode,MessageType,MessageName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 71,'Text','The value should consist of 10 digits in combinations of upper case alphabets and numbers.',2,getdate(),2,getdate()
where not exists(select * from ZnodeMessage where MessageCode = 71)

--dt 23-09-2020 ZPD-12366
INSERT INTO [dbo].[ZnodeMessage]
          ([MessageCode]
          ,[MessageType]
          ,[MessageName]
          ,[CreatedBy]
          ,[CreatedDate]
          ,[ModifiedBy]
          ,[ModifiedDate])
select 88,'Text','The Account cannot be changed.',2,getdate(),2,getdate()
where not exists(select * from [ZnodeMessage] where [MessageCode] = 88 )

INSERT INTO [dbo].[ZnodeMessage]
          ([MessageCode]
          ,[MessageType]
          ,[MessageName]
          ,[CreatedBy]
          ,[CreatedDate]
          ,[ModifiedBy]
          ,[ModifiedDate])
select 89,'Text','The added value should be the same as the existing value because this value cannot be updated.',2,getdate(),2,getdate()
where not exists(select * from [ZnodeMessage] where [MessageCode] = 89 )
	
INSERT INTO [dbo].[ZnodeMessage]
          ([MessageCode]
          ,[MessageType]
          ,[MessageName]
          ,[CreatedBy]
          ,[CreatedDate]
          ,[ModifiedBy]
          ,[ModifiedDate])
select 90,'Text','Country should belong to the selected Store.',2,getdate(),2,getdate()
where not exists(select * from [ZnodeMessage] where [MessageCode] = 90 )
	select * from znodeportal

INSERT INTO [dbo].[ZnodeMessage]
          ([MessageCode]
          ,[MessageType]
          ,[MessageName]
          ,[CreatedBy]
          ,[CreatedDate]
          ,[ModifiedBy]
          ,[ModifiedDate])
select 91,'Text','The default address cannot be marked as non-default.',2,getdate(),2,getdate()
where not exists(select * from [ZnodeMessage] where [MessageCode] = 91 )


delete from ZnodeImportUpdatableColumns
where ImportHeadId = (SELECT TOP 1 ImportHeadId from ZnodeImportHead where Name = 'Account')
and ColumnName in ('IsDefaultBilling','IsDefaultShipping')

--dt 30-09-2020 ZPD-12523
INSERT INTO [dbo].[ZnodeMessage]
          ([MessageCode]
          ,[MessageType]
          ,[MessageName]
          ,[CreatedBy]
          ,[CreatedDate]
          ,[ModifiedBy]
          ,[ModifiedDate])
select 92,'Text','Role Name cannot be updated.',2,getdate(),2,getdate()
where not exists(select * from [ZnodeMessage] where [MessageCode] = 92 )

Update ZnodeCMSMessage set Message = replace(Message,'2019','2020')
where Message like '%copyright%' and Message like '%2019%'

Update ZnodeCMSMessage set Message = replace(Message,'2020','2021')
where Message like '%copyright%' and Message like '%2020%'

Update ZnodeCMSMessage set Message = replace(Message,'2019','2021')
where Message like '%copyright%' and Message like '%2019%'