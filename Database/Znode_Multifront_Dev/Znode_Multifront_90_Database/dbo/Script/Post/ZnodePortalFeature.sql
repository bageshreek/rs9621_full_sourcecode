﻿
--dt 27-03-2020 ZPD-7626 --> ZPD-9585
insert into ZnodePortalFeature (PortalFeatureName,	CreatedBy,	CreatedDate,	ModifiedBy,	ModifiedDate)
select 'Enable_CMS_Page_Results',2,getdate(),2,getdate()
where not exists(select * from ZnodePortalFeature where PortalFeatureName = 'Enable_CMS_Page_Results')