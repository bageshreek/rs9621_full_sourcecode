﻿
--dt\09\10\2019 ZBT-369

INSERT INTO ZnodePimFamilyGroupMapper (PimAttributeFamilyId,PimAttributeGroupId,PimAttributeId,GroupDisplayOrder,IsSystemDefined
,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT PimAttributeFamilyId, (SELECT TOP 1 PimAttributeGroupId FROM ZnodePimAttributeGroup WHERE GroupCode = 'ProductInfo'),
(SELECT TOP 1 PimAttributeId FROM  ZnodePimAttribute WHERE AttributeCode = 'IsObsolete'),500,1,2,GETDATE(),2,GETDATE()
FROM  ZnodePimAttributeFamily PAF
WHERE NOT EXISTS (SELECT TOP 1 1 FROM ZnodePimFamilyGroupMapper PFG WHERE 
PimAttributeId = (SELECT TOP 1 PimAttributeId FROM  ZnodePimAttribute WHERE AttributeCode = 'IsObsolete')
AND PimAttributeGroupId = (SELECT TOP 1 PimAttributeGroupId FROM ZnodePimAttributeGroup WHERE GroupCode = 'ProductInfo') 
AND PFG.PimAttributeFamilyId = PAF.PimAttributeFamilyId)
AND PAF.IsCategory = 0
go

--dt \14\10\2019 ZBT-358

DECLARE @InsertedPimAttributeIds TABLE (PimAttributeId int ,AttributeTypeId int,AttributeCode nvarchar(300))
INSERT INTO ZnodePimAttribute (AttributeTypeId,AttributeCode,IsRequired,IsLocalizable,IsFilterable,IsSystemDefined
,IsConfigurable,IsPersonalizable,IsShowOnGrid,DisplayOrder,HelpDescription,IsCategory,IsHidden,IsSwatch,
CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)		
OUTPUT Inserted.PimAttributeId,Inserted.AttributeTypeId,Inserted.AttributeCode INTO @InsertedPimAttributeIds  		
SELECT (SELECT AttributeTypeId FROM ZnodeAttributeType WHERE AttributeTypeName = 'Text')
,'UPC',0,1,1,0,0,0,0,10,null,0,0,null,2,GETDATE(),2,GETDATE()
WHERE NOT EXISTS (SELECT TOP 1 1 FROM ZnodePimAttribute ZPA WHERE ZPA.AttributeCode = 'UPC')
		
INSERT INTO ZnodePimAttributeLocale (LocaleId,PimAttributeId,AttributeName,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT 1 ,IPAS.PimAttributeId, 'UPC Code', null, 2,GETDATE(),2,GETDATE()   
FROM @InsertedPimAttributeIds IPAS 
		
insert into ZnodePimFrontendProperties 
(PimAttributeId,IsComparable,IsUseInSearch,IsHtmlTags,IsFacets,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
Select IPA.PimAttributeId,1 IsComparable, 1 IsUseInSearch, 0 IsHtmlTags,0 IsFacets,2,getdate(),2,getdate()
from @InsertedPimAttributeIds IPA
		
INSERT INTO ZnodePimAttributeGroupMapper
(PimAttributeGroupId,PimAttributeId,AttributeDisplayOrder,IsSystemDefined,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select PimAttributeGroupId from ZnodePimAttributeGroup where GroupCode = 'ProductDetails'),(select PimAttributeId from znodePimattribute where AttributeCode = 'UPC'),null,1,2,getdate(),2,getdate()
WHERE NOT EXISTS (select * from ZnodePimAttributeGroupMapper where PimAttributeGroupId =(select PimAttributeGroupId from ZnodePimAttributeGroup where GroupCode = 'ProductDetails') AND
PimAttributeId = (select PimAttributeId from znodePimattribute where AttributeCode = 'UPC') )
GO
--dt 16/10/2019 ZPD-7674
update ZnodePimAttribute set IsSystemDefined = 1 where AttributeCode='UPC'

go
--dt 16/10/2019 ZPD-7673
INSERT INTO ZnodePimFamilyGroupMapper (PimAttributeFamilyId,PimAttributeGroupId,PimAttributeId,GroupDisplayOrder,IsSystemDefined
,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT PimAttributeFamilyId, (SELECT TOP 1 PimAttributeGroupId FROM ZnodePimAttributeGroup WHERE GroupCode = 'ProductDetails'),
(SELECT TOP 1 PimAttributeId FROM  ZnodePimAttribute WHERE AttributeCode = 'UPC'),500,1,2,GETDATE(),2,GETDATE()
FROM  ZnodePimAttributeFamily PAF
WHERE NOT EXISTS (SELECT TOP 1 1 FROM ZnodePimFamilyGroupMapper PFG WHERE 
PimAttributeId = (SELECT TOP 1 PimAttributeId FROM  ZnodePimAttribute WHERE AttributeCode = 'UPC')
AND PimAttributeGroupId = (SELECT TOP 1 PimAttributeGroupId FROM ZnodePimAttributeGroup WHERE GroupCode = 'ProductDetails') 
AND PFG.PimAttributeFamilyId = PAF.PimAttributeFamilyId)
AND PAF.IsCategory = 0

--dt 17/10/2019 ZPD-7674
update ZnodePimAttributeGroupMapper set IsSystemDefined = 1
where PimAttributeId = (select Top 1 PimAttributeId from ZnodePimAttribute where AttributeCode  = 'upc')

update  ZnodePimFamilyGroupMapper set IsSystemDefined = 1  
where PimAttributeId = (select Top 1 PimAttributeId from ZnodePimAttribute where AttributeCode  = 'upc')

--dt 08/11/2019 ZPD-7815 --> ZPD-7800
update ZnodePimAttribute set HelpDescription= 'Enter the minimum quantity that can be selected when adding an item to the cart.' where AttributeCode = 'MinimumQuantity'
GO
update ZnodePimAttribute set HelpDescription= 'Enter the maximum quantity that can be selected when adding an item to the cart.' where AttributeCode = 'MaximumQuantity'

--dt 26-03-2020 ZPD-7632 -- > ZPD-8048
DECLARE @InsertedPimAttributeIds TABLE (PimAttributeId int ,AttributeTypeId int,AttributeCode nvarchar(300))
INSERT INTO ZnodePimAttribute (AttributeTypeId,AttributeCode,IsRequired,IsLocalizable,IsFilterable,IsSystemDefined
,IsConfigurable,IsPersonalizable,IsShowOnGrid,DisplayOrder,HelpDescription,IsCategory,IsHidden,IsSwatch,
CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)		
OUTPUT Inserted.PimAttributeId,Inserted.AttributeTypeId,Inserted.AttributeCode INTO @InsertedPimAttributeIds  		
SELECT (SELECT AttributeTypeId FROM ZnodeAttributeType WHERE AttributeTypeName = 'Yes/No')
,'HideFromSearch',0,1,1,0,0,0,0,500,null,0,0,null,2,GETDATE(),2,GETDATE()
WHERE NOT EXISTS (SELECT TOP 1 1 FROM ZnodePimAttribute ZPA WHERE ZPA.AttributeCode = 'HideFromSearch')
		
INSERT INTO ZnodePimAttributeLocale (LocaleId,PimAttributeId,AttributeName,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT 1 ,IPAS.PimAttributeId, 'Hide From Search ', null, 2,GETDATE(),2,GETDATE()   
FROM @InsertedPimAttributeIds IPAS 
		
insert into ZnodePimFrontendProperties 
(PimAttributeId,IsComparable,IsUseInSearch,IsHtmlTags,IsFacets,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
Select IPA.PimAttributeId,1 IsComparable, 1 IsUseInSearch, 0 IsHtmlTags,0 IsFacets,2,getdate(),2,getdate()
from @InsertedPimAttributeIds IPA
		
INSERT INTO ZnodePimAttributeGroupMapper
(PimAttributeGroupId,PimAttributeId,AttributeDisplayOrder,IsSystemDefined,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select PimAttributeGroupId from ZnodePimAttributeGroup where GroupCode = 'ProductInfo'),(select PimAttributeId from znodePimattribute where AttributeCode = 'HideFromSearch'),null,0,2,getdate(),2,getdate()
WHERE NOT EXISTS (select * from ZnodePimAttributeGroupMapper where PimAttributeGroupId =(select PimAttributeGroupId from ZnodePimAttributeGroup where GroupCode = 'ProductInfo') AND
PimAttributeId = (select PimAttributeId from znodePimattribute where AttributeCode = 'HideFromSearch') )
GO

INSERT INTO ZnodePimFamilyGroupMapper (PimAttributeFamilyId,PimAttributeGroupId,PimAttributeId,GroupDisplayOrder,IsSystemDefined
,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT PimAttributeFamilyId, (SELECT TOP 1 PimAttributeGroupId FROM ZnodePimAttributeGroup WHERE GroupCode = 'ProductInfo'),
(SELECT TOP 1 PimAttributeId FROM  ZnodePimAttribute WHERE AttributeCode = 'HideFromSearch'),500,1,2,GETDATE(),2,GETDATE()
FROM  ZnodePimAttributeFamily PAF
WHERE NOT EXISTS (SELECT TOP 1 1 FROM ZnodePimFamilyGroupMapper PFG WHERE 
PimAttributeId = (SELECT TOP 1 PimAttributeId FROM  ZnodePimAttribute WHERE AttributeCode = 'HideFromSearch')
AND PimAttributeGroupId = (SELECT TOP 1 PimAttributeGroupId FROM ZnodePimAttributeGroup WHERE GroupCode = 'ProductInfo') 
AND PFG.PimAttributeFamilyId = PAF.PimAttributeFamilyId)
AND PAF.IsCategory = 0 and PAF.FamilyCode = 'Default'

--dt 15-06-2020 ZPD-10689 -- > ZPD-10255
DECLARE @InsertedPimAttributeIds TABLE (PimAttributeId int ,AttributeTypeId int,AttributeCode nvarchar(300))
INSERT INTO ZnodePimAttribute (AttributeTypeId,AttributeCode,IsRequired,IsLocalizable,IsFilterable,IsSystemDefined
,IsConfigurable,IsPersonalizable,IsShowOnGrid,DisplayOrder,HelpDescription,IsCategory,IsHidden,IsSwatch,
CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)		
OUTPUT Inserted.PimAttributeId,Inserted.AttributeTypeId,Inserted.AttributeCode INTO @InsertedPimAttributeIds  		
SELECT (SELECT AttributeTypeId FROM ZnodeAttributeType WHERE AttributeTypeName = 'Number')
,'TypicalLeadTime',0,0,1,1,0,0,0,5,null,0,0,null,2,GETDATE(),2,GETDATE()
WHERE NOT EXISTS (SELECT TOP 1 1 FROM ZnodePimAttribute ZPA WHERE ZPA.AttributeCode = 'TypicalLeadTime')
		
INSERT INTO ZnodePimAttributeLocale (LocaleId,PimAttributeId,AttributeName,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT 1 ,IPAS.PimAttributeId, 'Typical Lead Time', null, 2,GETDATE(),2,GETDATE()   
FROM @InsertedPimAttributeIds IPAS 
		
insert into ZnodePimFrontendProperties 
(PimAttributeId,IsComparable,IsUseInSearch,IsHtmlTags,IsFacets,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
Select IPA.PimAttributeId,0 IsComparable, 0 IsUseInSearch, 0 IsHtmlTags,0 IsFacets,2,getdate(),2,getdate()
from @InsertedPimAttributeIds IPA
		
INSERT INTO ZnodePimAttributeGroupMapper
(PimAttributeGroupId,PimAttributeId,AttributeDisplayOrder,IsSystemDefined,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select PimAttributeGroupId from ZnodePimAttributeGroup where GroupCode = 'ProductSetting'),(select PimAttributeId from znodePimattribute where AttributeCode = 'TypicalLeadTime'),null,0,2,getdate(),2,getdate()
WHERE NOT EXISTS (select * from ZnodePimAttributeGroupMapper where PimAttributeGroupId =(select PimAttributeGroupId from ZnodePimAttributeGroup where GroupCode = 'ProductSetting') AND
PimAttributeId = (select PimAttributeId from znodePimattribute where AttributeCode = 'TypicalLeadTime') )
GO

INSERT INTO ZnodePimFamilyGroupMapper (PimAttributeFamilyId,PimAttributeGroupId,PimAttributeId,GroupDisplayOrder,IsSystemDefined
,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT PimAttributeFamilyId, (SELECT TOP 1 PimAttributeGroupId FROM ZnodePimAttributeGroup WHERE GroupCode = 'ProductSetting'),
(SELECT TOP 1 PimAttributeId FROM  ZnodePimAttribute WHERE AttributeCode = 'TypicalLeadTime'),500,1,2,GETDATE(),2,GETDATE()
FROM  ZnodePimAttributeFamily PAF
WHERE NOT EXISTS (SELECT TOP 1 1 FROM ZnodePimFamilyGroupMapper PFG WHERE 
PimAttributeId = (SELECT TOP 1 PimAttributeId FROM  ZnodePimAttribute WHERE AttributeCode = 'TypicalLeadTime')
AND PimAttributeGroupId = (SELECT TOP 1 PimAttributeGroupId FROM ZnodePimAttributeGroup WHERE GroupCode = 'ProductSetting') 
AND PFG.PimAttributeFamilyId = PAF.PimAttributeFamilyId)
AND PAF.IsCategory = 0 and PAF.FamilyCode = 'Default'

--dt 19-06-2020 ZPD-10932 
insert into ZnodePimAttributeValidation(PimAttributeId,InputValidationId,InputValidationRuleId,Name,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 PimAttributeId from ZnodePimAttribute where AttributeCode = 'TypicalLeadTime'),
(select top 1 InputValidationId from ZnodeAttributeInputValidation where Name = 'AllowNegative' ),null,'false',2,GETDATE(),2,GETDATE()
where not exists(select * from ZnodePimAttributeValidation where PimAttributeId=(select top 1 PimAttributeId from ZnodePimAttribute where AttributeCode = 'TypicalLeadTime')
and InputValidationId=(select top 1 InputValidationId from ZnodeAttributeInputValidation where Name = 'AllowNegative' )
)

insert into ZnodePimAttributeValidation(PimAttributeId,InputValidationId,InputValidationRuleId,Name,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 PimAttributeId from ZnodePimAttribute where AttributeCode = 'TypicalLeadTime'),
(select top 1 InputValidationId from ZnodeAttributeInputValidation where Name = 'AllowDecimals' ),null,'false',2,GETDATE(),2,GETDATE()
where not exists(select * from ZnodePimAttributeValidation where PimAttributeId=(select top 1 PimAttributeId from ZnodePimAttribute where AttributeCode = 'TypicalLeadTime')
and InputValidationId=(select top 1 InputValidationId from ZnodeAttributeInputValidation where Name = 'AllowDecimals' )
)

insert into ZnodePimAttributeValidation(PimAttributeId,InputValidationId,InputValidationRuleId,Name,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 PimAttributeId from ZnodePimAttribute where AttributeCode = 'TypicalLeadTime'),
(select top 1 InputValidationId from ZnodeAttributeInputValidation where Name = 'MinNumber' ),null,'',2,GETDATE(),2,GETDATE()
where not exists(select * from ZnodePimAttributeValidation where PimAttributeId=(select top 1 PimAttributeId from ZnodePimAttribute where AttributeCode = 'TypicalLeadTime')
and InputValidationId=(select top 1 InputValidationId from ZnodeAttributeInputValidation where Name = 'MinNumber' )
)

insert into ZnodePimAttributeValidation(PimAttributeId,InputValidationId,InputValidationRuleId,Name,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 PimAttributeId from ZnodePimAttribute where AttributeCode = 'TypicalLeadTime'),
(select top 1 InputValidationId from ZnodeAttributeInputValidation where Name = 'MaxNumber' ),null,'',2,GETDATE(),2,GETDATE()
where not exists(select * from ZnodePimAttributeValidation where PimAttributeId=(select top 1 PimAttributeId from ZnodePimAttribute where AttributeCode = 'TypicalLeadTime')
and InputValidationId=(select top 1 InputValidationId from ZnodeAttributeInputValidation where Name = 'MaxNumber' )
)

--dt 21-07-2020 ZPD-11600 --> ZPD-10255
declare @PimAttributeGroupId int
set @PimAttributeGroupId = (select top 1 PimAttributeGroupId  from ZnodePimAttributeGroupMapper where PimAttributeId = (select top 1 PimAttributeId from ZnodePimAttribute where AttributeCode = 'TypicalLeadTime')
and PimAttributeGroupId = (select top 1 PimAttributeGroupId from ZnodePimAttributeGroup where GroupCode = 'ProductSetting'))

insert into ZnodePimFamilyGroupMapper(PimAttributeFamilyId,PimAttributeGroupId,PimAttributeId,GroupDisplayOrder,IsSystemDefined,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select PimAttributeFamilyId,@PimAttributeGroupId,(select top 1 PimAttributeId from ZnodePimAttribute where AttributeCode = 'TypicalLeadTime'),500,1,2,getdate(),2,getdate()
from znodepimattributefamily zpaf
where not exists(select * from ZnodePimFamilyGroupMapper zpfgm where zpfgm.PimAttributeFamilyId = zpaf.PimAttributeFamilyId
	and zpfgm.PimAttributeGroupId = @PimAttributeGroupId and zpfgm.PimAttributeId = (select top 1 PimAttributeId from ZnodePimAttribute where AttributeCode = 'TypicalLeadTime'))

update ZnodePimFamilyGroupMapper  set IsSystemDefined = 1 where PimAttributeId = (select top 1 PimAttributeId from ZnodePimAttribute where AttributeCode = 'TypicalLeadTime')
update ZnodePimAttributeGroupMapper set IsSystemDefined = 1 where PimAttributeId = (select top 1 PimAttributeId from ZnodePimAttribute where AttributeCode = 'TypicalLeadTime')

--dt 22-10-2020 ZPD-12682
insert into ZnodePimAttributegroupmapper
select PimAttributeGroupId,PimAttributeId, null,0,2,getdate(),2,getdate()
from ZnodePimAttributeGroup PAG 
inner join ZnodePimAttribute ZPA on  GroupCode ='ShippingSettings'and attributecode = 'FreeShipping'
where not exists (select top 1 1 from ZnodePimAttributegroupmapper ZPAG where ZPAG.PimAttributeGroupId = PAG.PimAttributeGroupId and ZPA.PimAttributeId = ZPAG.PimAttributeId)

insert into ZnodePimFamilyGroupMapper
select PimAttributeFamilyId, PimAttributeGroupId,PimAttributeId, 500,0,2,getdate(),2,getdate()
from ZnodePimAttributeFamily ZAF 
inner join ZnodePimAttributeGroup PAG  on  GroupCode ='ShippingSettings'
inner join ZnodePimAttribute ZPA on   attributecode = 'FreeShipping'
where not exists (select top 1 1 from ZnodePimFamilyGroupMapper ZPAG where ZPAG.PimAttributeFamilyId = ZAF.PimAttributeFamilyId and ZPAG.PimAttributeGroupId = PAG.PimAttributeGroupId and ZPA.PimAttributeId= ZPAG.PimAttributeId)

update ZnodePimAttributeGroupMapper set IsSystemDefined = 1
where PimAttributeId = (select top 1 PimAttributeId from znodePimAttribute where AttributeCode = 'FreeShipping' and IsSystemDefined = 1)
and isnull(IsSystemDefined,0) = 0

update ZnodePimFamilyGroupMapper set IsSystemDefined = 1
where PimAttributeId = (select top 1 PimAttributeId from znodePimAttribute where AttributeCode = 'FreeShipping' and IsSystemDefined = 1)
and isnull(IsSystemDefined,0) = 0