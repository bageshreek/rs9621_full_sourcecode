﻿Insert  INTO ZnodeCMSWidgets (Code,DisplayName,IsConfigurable,FileName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT 'HomeRecommendations' ,'RecommendedProducts',0,'Product_List.png',2,Getdate(),2,Getdate() where not exists
(SELECT * FROM ZnodeCMSWidgets where Code = 'HomeRecommendations')
UNION ALL 
SELECT 'PDPRecommendations' ,'RecommendedProducts',0,'Product_List.png',2,Getdate(),2,Getdate() where not exists
(SELECT * FROM ZnodeCMSWidgets where Code = 'PDPRecommendations')
UNION ALL
SELECT 'CartRecommendations' ,'RecommendedProducts',0,'Product_List.png',2,Getdate(),2,Getdate() where not exists
(SELECT * FROM ZnodeCMSWidgets where Code = 'CartRecommendations')

go
Insert  INTO ZnodeCMSWidgets (Code,DisplayName,IsConfigurable,FileName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT 'ImageWidget' ,'ImageWidget',1,'Text_Editor.png',2,Getdate(),2,Getdate() where not exists
(SELECT * FROM ZnodeCMSWidgets where Code = 'ImageWidget')
go
Insert  INTO ZnodeCMSWidgets (Code,DisplayName,IsConfigurable,FileName,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
SELECT 'VideoWidget' ,'Video Widget',1,'Text_Editor.png',2,Getdate(),2,Getdate() where not exists
(SELECT * FROM ZnodeCMSWidgets where Code = 'VideoWidget')

--dt 03-02-2020 ZPD-7565 --> ZPD-9001
insert into [ZnodeCMSWidgets] ([Code],[DisplayName],[IsConfigurable],[FileName],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate])
select 'MegaMenuNavigation','MegaMenuNavigation',0,'Top-Menu.png',2,GETDATE(),2,GETDATE()
where not exists(select * from [ZnodeCMSWidgets] where [Code] = 'MegaMenuNavigation')