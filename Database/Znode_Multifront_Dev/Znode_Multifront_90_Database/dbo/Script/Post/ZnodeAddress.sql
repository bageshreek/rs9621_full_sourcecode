﻿
--dt 09-03-2020 ZPD-9380 --> ZPD-9392
delete from ZnodeImportUpdatableColumns where ImportHeadId = (select top 1 ImportHeadId from ZnodeImportHead where Name = 'CustomerAddress')
and ColumnName in ('IsDefaultBilling', 'IsDefaultShipping')