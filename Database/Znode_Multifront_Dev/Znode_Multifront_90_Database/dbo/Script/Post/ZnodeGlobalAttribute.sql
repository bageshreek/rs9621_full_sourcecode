﻿
--dt 20-01-2020 ZPD-8291 --> ZPD-8921
insert into ZnodeGlobalAttribute(AttributeTypeId,AttributeCode,IsRequired,IsLocalizable,IsActive,DisplayOrder,HelpDescription,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsSystemDefined)
select (select top 1 AttributeTypeId from ZnodeAttributeType where AttributeTypeName = 'Yes/No'),'IsCloudflareEnabled',1,0,0,1,
'Decide is the Clouflare enabled on webstore?',2,getdate(),2,getdate(),0
where not exists(select * from ZnodeGlobalAttribute where  AttributeCode = 'IsCloudflareEnabled')

insert into ZnodeGlobalAttribute(AttributeTypeId,AttributeCode,IsRequired,IsLocalizable,IsActive,DisplayOrder,HelpDescription,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsSystemDefined)
select (select top 1 AttributeTypeId from ZnodeAttributeType where AttributeTypeName = 'Text'),'CloudflareZoneId',1,1,0,2,
'ZoneId for Cloudflare',2,getdate(),2,getdate(),0
where not exists(select * from ZnodeGlobalAttribute where  AttributeCode = 'CloudflareZoneId')

insert into ZnodePortalGlobalAttributeValue(PortalId,GlobalAttributeId,GlobalAttributeDefaultValueId,AttributeValue,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select PortalId,(select top 1 GlobalAttributeId from ZnodeGlobalAttribute where  AttributeCode = 'IsCloudflareEnabled'),null,null,2,getdate(),2,getdate()
from ZnodePortal ZP
where not exists(select * from ZnodePortalGlobalAttributeValue ZPGA where ZP.PortalId = ZPGA.PortalId
      and ZPGA.GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where  AttributeCode = 'IsCloudflareEnabled'))


insert into ZnodePortalGlobalAttributeValueLocale(PortalGlobalAttributeValueId,	LocaleId,	AttributeValue	,CreatedBy,	CreatedDate,	ModifiedBy	,ModifiedDate)
select PortalGlobalAttributeValueId,1,'false',2,getdate(),2,getdate()
from ZnodePortalGlobalAttributeValue ZPGA
where ZPGA.GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where  AttributeCode = 'IsCloudflareEnabled')
and not exists(select * from ZnodePortalGlobalAttributeValueLocale ZPGAVL where ZPGAVL.PortalGlobalAttributeValueId = ZPGA.PortalGlobalAttributeValueId)

insert into ZnodePortalGlobalAttributeValue(PortalId,GlobalAttributeId,GlobalAttributeDefaultValueId,AttributeValue,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select PortalId,(select top 1 GlobalAttributeId from ZnodeGlobalAttribute where  AttributeCode = 'CloudflareZoneId'),null,null,2,getdate(),2,getdate()
from ZnodePortal ZP
where not exists(select * from ZnodePortalGlobalAttributeValue ZPGA where ZP.PortalId = ZPGA.PortalId
      and ZPGA.GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where  AttributeCode = 'CloudflareZoneId'))

insert into ZnodePortalGlobalAttributeValueLocale(PortalGlobalAttributeValueId,	LocaleId,	AttributeValue	,CreatedBy,	CreatedDate,	ModifiedBy	,ModifiedDate)
select PortalGlobalAttributeValueId,1,'',2,getdate(),2,getdate()
from ZnodePortalGlobalAttributeValue ZPGA
where ZPGA.GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where  AttributeCode = 'CloudflareZoneId')
and not exists(select * from ZnodePortalGlobalAttributeValueLocale ZPGAVL where ZPGAVL.PortalGlobalAttributeValueId = ZPGA.PortalGlobalAttributeValueId)

insert into ZnodeGlobalAttributeLocale(LocaleId,GlobalAttributeId,AttributeName,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 1,GlobalAttributeId,'Enable Cloudflare',null,2,getdate(),2,getdate()
from ZnodeGlobalAttribute a
where AttributeCode = 'IsCloudflareEnabled' and 
not exists(select * from ZnodeGlobalAttributeLocale b where a.GlobalAttributeId = b.GlobalAttributeId and b.LocaleId = 1)

insert into ZnodeGlobalAttributeLocale(LocaleId,GlobalAttributeId,AttributeName,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 1,GlobalAttributeId,'ZoneId',null,2,getdate(),2,getdate()
from ZnodeGlobalAttribute a
where AttributeCode = 'CloudflareZoneId' and 
not exists(select * from ZnodeGlobalAttributeLocale b where a.GlobalAttributeId = b.GlobalAttributeId and b.LocaleId = 1)

----
insert into ZnodeGlobalAttributeGroup(GroupCode,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsSystemDefined)
select 'Cloudflaresetting',null,2,getdate(),2,getdate(),0
where not exists(select * from ZnodeGlobalAttributeGroup where GroupCode = 'Cloudflaresetting')

insert into ZnodeGlobalAttributeGroupLocale(LocaleId,GlobalAttributeGroupId,AttributeGroupName,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 1,GlobalAttributeGroupId,'Cloudflare Setting',null,2,getdate(),2,getdate()
from ZnodeGlobalAttributeGroup a
where GroupCode = 'Cloudflaresetting'
and not exists(select * from ZnodeGlobalAttributeGroupLocale b where b.GlobalAttributeGroupId= a.GlobalAttributeGroupId and b.LocaleId = 1)


insert into ZnodeGlobalAttributeGroupMapper (GlobalAttributeGroupId,GlobalAttributeId,AttributeDisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'Cloudflaresetting'),
(select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'IsCloudflareEnabled'),null,2,getdate(),2,getdate()
where not exists(select * from ZnodeGlobalAttributeGroupMapper 
     where GlobalAttributeGroupId = (select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'Cloudflaresetting')
	 and GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'IsCloudflareEnabled'))

insert into ZnodeGlobalAttributeGroupMapper (GlobalAttributeGroupId,GlobalAttributeId,AttributeDisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'Cloudflaresetting'),
(select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'CloudflareZoneId'),null,2,getdate(),2,getdate()
where not exists(select * from ZnodeGlobalAttributeGroupMapper 
     where GlobalAttributeGroupId = (select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'Cloudflaresetting')
	 and GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'CloudflareZoneId'))

	 
insert into ZnodeGlobalGroupEntityMapper(GlobalAttributeGroupId,GlobalEntityId,AttributeGroupDisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'Cloudflaresetting'),
(select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Store'),1, 2,getdate(),2,GETDATE()
where not exists(select * from ZnodeGlobalGroupEntityMapper where GlobalAttributeGroupId=(select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'Cloudflaresetting')
      and GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Store'))

--dt 06-02-2020 ZPD-8956 
update ZnodeGlobalAttribute set IsRequired = 0 where AttributeCode = 'CloudflareZoneId'

--dt 25-02-2020 ZPD-9275
update ZnodeGlobalAttribute set HelpDescription = 'When enabled, the Cloudflare cache will get purged when this store will be published and also users will have the ability to manually purge the Cloudflare cache for this store from the Global Settings.'
where AttributeCode = 'IsCloudflareEnabled'

update ZnodeGlobalAttribute set HelpDescription = 'Every Zone ID is associated with a domains. When Cloudflare cache purge activity is initiated the input in this field is used by Znode to identify the cache of which domain(and its subdomains) to clear.'
where AttributeCode = 'CloudflareZoneId'

update ZGAL set ZGAL.AttributeName = 'Zone ID'
from ZnodeGlobalAttribute ZGA
inner join ZnodeGlobalAttributeLocale ZGAL on ZGA.GlobalAttributeId = ZGAL.GlobalAttributeId
where ZGA.AttributeCode='CloudflareZoneId'

----dt 15-05-2020 ZPD-10318 --> ZPD-10408

insert into ZnodeGlobalAttribute(AttributeTypeId,AttributeCode,IsRequired,IsLocalizable,IsActive,DisplayOrder,HelpDescription,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsSystemDefined)
select (select top 1 AttributeTypeId from ZnodeAttributeType where AttributeTypeName = 'Yes/No'),'EnableQuoteRequest',1,0,0,500,
'When enabled, customers will be able to create Quote Requests from web store.',2,getdate(),2,getdate(),0
where not exists(select * from ZnodeGlobalAttribute where  AttributeCode = 'EnableQuoteRequest')

insert into ZnodeGlobalAttribute(AttributeTypeId,AttributeCode,IsRequired,IsLocalizable,IsActive,DisplayOrder,HelpDescription,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsSystemDefined)
select (select top 1 AttributeTypeId from ZnodeAttributeType where AttributeTypeName = 'Number'),'QuoteExpireInDays',1,0,0,500,
'Set a number of days after which you want the Quote to expire.',2,getdate(),2,getdate(),0
where not exists(select * from ZnodeGlobalAttribute where  AttributeCode = 'QuoteExpireInDays')

insert into ZnodeGlobalAttributeGroup(GroupCode,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsSystemDefined)
select 'QuotesSettings',null,2,getdate(),2,getdate(),0
where not exists(select * from ZnodeGlobalAttributeGroup where GroupCode = 'QuotesSettings')

insert into ZnodeGlobalAttributeDefaultValue(GlobalAttributeId,AttributeDefaultValueCode,IsEditable,DisplayOrder,MediaId,SwatchText,IsDefault,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where  AttributeCode = 'EnableQuoteRequest'),'false',null,null,null,null,null,2,getdate(),2,getdate()
where not exists(select * from ZnodeGlobalAttributeDefaultValue where GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where  AttributeCode = 'EnableQuoteRequest') and AttributeDefaultValueCode = 'false')

insert into ZnodeGlobalAttributeDefaultValue(GlobalAttributeId,AttributeDefaultValueCode,IsEditable,DisplayOrder,MediaId,SwatchText,IsDefault,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where  AttributeCode = 'QuoteExpireInDays'),'30',null,null,null,null,null,2,getdate(),2,getdate()
where not exists(select * from ZnodeGlobalAttributeDefaultValue where GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where  AttributeCode = 'QuoteExpireInDays') and AttributeDefaultValueCode = '30')


insert into ZnodeGlobalAttributeGroupMapper (GlobalAttributeGroupId,GlobalAttributeId,AttributeDisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'QuotesSettings'),
(select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'EnableQuoteRequest'),null,2,getdate(),2,getdate()
where not exists(select * from ZnodeGlobalAttributeGroupMapper 
     where GlobalAttributeGroupId = (select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'QuotesSettings')
	 and GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'EnableQuoteRequest'))

insert into ZnodeGlobalAttributeGroupMapper (GlobalAttributeGroupId,GlobalAttributeId,AttributeDisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'QuotesSettings'),
(select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'QuoteExpireInDays'),null,2,getdate(),2,getdate()
where not exists(select * from ZnodeGlobalAttributeGroupMapper 
     where GlobalAttributeGroupId = (select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'QuotesSettings')
	 and GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'QuoteExpireInDays'))

insert into ZnodeGlobalAttributeLocale(LocaleId,GlobalAttributeId,AttributeName,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 1,GlobalAttributeId,'Enable Quote Requests From Web Store',null,2,getdate(),2,getdate()
from ZnodeGlobalAttribute a
where AttributeCode = 'EnableQuoteRequest' and 
not exists(select * from ZnodeGlobalAttributeLocale b where a.GlobalAttributeId = b.GlobalAttributeId and b.LocaleId = 1)


insert into ZnodeGlobalAttributeLocale(LocaleId,GlobalAttributeId,AttributeName,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 1,GlobalAttributeId,'Quote Expire In (days)',null,2,getdate(),2,getdate()
from ZnodeGlobalAttribute a
where AttributeCode = 'QuoteExpireInDays' and 
not exists(select * from ZnodeGlobalAttributeLocale b where a.GlobalAttributeId = b.GlobalAttributeId and b.LocaleId = 1)

insert into ZnodeGlobalAttributeLocale(LocaleId,GlobalAttributeId,AttributeName,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 1,GlobalAttributeId,'Enable Cloudflare',null,2,getdate(),2,getdate()
from ZnodeGlobalAttribute a
where AttributeCode = 'IsCloudflareEnabled' and 
not exists(select * from ZnodeGlobalAttributeLocale b where a.GlobalAttributeId = b.GlobalAttributeId and b.LocaleId = 1)
  
insert into ZnodeGlobalAttributeValidation (GlobalAttributeId,InputValidationId,InputValidationRuleId,Name,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'QuoteExpireInDays'),(select top 1 InputValidationId from znodeattributeinputvalidation  where Name = 'AllowNegative')
,null,'false',2,getdate(),2,getdate()
where not exists(select * from ZnodeGlobalAttributeValidation where GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'QuoteExpireInDays')
and InputValidationId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'QuoteExpireInDays'))

insert into ZnodeGlobalAttributeValidation (GlobalAttributeId,InputValidationId,InputValidationRuleId,Name,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'QuoteExpireInDays'),(select top 1 InputValidationId from znodeattributeinputvalidation  where Name = 'AllowDecimals')
,null,'false',2,getdate(),2,getdate()
where not exists(select * from ZnodeGlobalAttributeValidation where GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'QuoteExpireInDays')
and InputValidationId = (select top 1 InputValidationId from znodeattributeinputvalidation  where Name = 'AllowDecimals'))

insert into ZnodeGlobalAttributeValidation (GlobalAttributeId,InputValidationId,InputValidationRuleId,Name,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'QuoteExpireInDays'),(select top 1 InputValidationId from znodeattributeinputvalidation  where Name = 'MinNumber')
,null,'1',2,getdate(),2,getdate()
where not exists(select * from ZnodeGlobalAttributeValidation where GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'QuoteExpireInDays')
and InputValidationId = (select top 1 InputValidationId from znodeattributeinputvalidation  where Name = 'MinNumber'))

insert into ZnodeGlobalAttributeValidation (GlobalAttributeId,InputValidationId,InputValidationRuleId,Name,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'QuoteExpireInDays'),(select top 1 InputValidationId from znodeattributeinputvalidation  where Name = 'MaxNumber')
,null,'999',2,getdate(),2,getdate()
where not exists(select * from ZnodeGlobalAttributeValidation where GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'QuoteExpireInDays')
and InputValidationId = (select top 1 InputValidationId from znodeattributeinputvalidation  where Name = 'MaxNumber'))

insert into ZnodePortalGlobalAttributeValue(PortalId,GlobalAttributeId,GlobalAttributeDefaultValueId,AttributeValue,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select PortalId,(select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'EnableQuoteRequest'),null,null,2,getdate(),2,getdate()
from ZnodePortal ZP
where not exists(select * from ZnodePortalGlobalAttributeValue z where zp.PortalId = z.PortalId and z.GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'EnableQuoteRequest'))

insert into ZnodePortalGlobalAttributeValue(PortalId,GlobalAttributeId,GlobalAttributeDefaultValueId,AttributeValue,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select PortalId,(select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'QuoteExpireInDays'),null,null,2,getdate(),2,getdate()
from ZnodePortal ZP
where not exists(select * from ZnodePortalGlobalAttributeValue z where zp.PortalId = z.PortalId and z.GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'QuoteExpireInDays'))

insert into ZnodeGlobalAttributeGroup(GroupCode,DisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsSystemDefined)
select 'QuotesSettings',null,2,getdate(),2,getdate(),0
where not exists(select * from ZnodeGlobalAttributeGroup where GroupCode = 'QuotesSettings')

update ZnodeGlobalAttributeGroupMapper set  GlobalAttributeGroupId = (select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'QuotesSettings')
where GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'EnableQuoteRequest')
and GlobalAttributeGroupId is null

update ZnodeGlobalAttributeGroupMapper set  GlobalAttributeGroupId = (select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'QuotesSettings')
where GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'QuoteExpireInDays')
and GlobalAttributeGroupId is null


insert into ZnodeGlobalAttributeGroupLocale(LocaleId,GlobalAttributeGroupId,AttributeGroupName,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 1,GlobalAttributeGroupId,'Quote Settings',null,2,getdate(),2,getdate()
from ZnodeGlobalAttributeGroup ag
where GroupCode = 'QuotesSettings' 
and not exists(select * from ZnodeGlobalAttributeGroupLocale agl where ag.GlobalAttributeGroupId = agl.GlobalAttributeGroupId and agl.LocaleId = 1)

insert into ZnodePortalGlobalAttributeValueLocale(PortalGlobalAttributeValueId,LocaleId,AttributeValue,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,GlobalAttributeDefaultValueId,MediaId,MediaPath)
select ZPGAV.PortalGlobalAttributeValueId, 1, 'true',2,getdate(),2,getdate(),null,null,null
from ZnodePortalGlobalAttributeValue ZPGAV
inner join ZnodePortal ZP ON ZP.PortalId = ZPGAV.PortalId
where ZPGAV.GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'EnableQuoteRequest')
and not exists(select * from ZnodePortalGlobalAttributeValueLocale ZPGAVL where ZPGAV.PortalGlobalAttributeValueId = ZPGAVL.PortalGlobalAttributeValueId and ZPGAVL.LocaleId = 1)

insert into ZnodePortalGlobalAttributeValueLocale(PortalGlobalAttributeValueId,LocaleId,AttributeValue,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,GlobalAttributeDefaultValueId,MediaId,MediaPath)
select ZPGAV.PortalGlobalAttributeValueId, 1, '30',2,getdate(),2,getdate(),null,null,null
from ZnodePortalGlobalAttributeValue ZPGAV
inner join ZnodePortal ZP ON ZP.PortalId = ZPGAV.PortalId
where ZPGAV.GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'QuoteExpireInDays')
and not exists(select * from ZnodePortalGlobalAttributeValueLocale ZPGAVL where ZPGAV.PortalGlobalAttributeValueId = ZPGAVL.PortalGlobalAttributeValueId and ZPGAVL.LocaleId = 1)

insert into ZnodeGlobalGroupEntityMapper(GlobalAttributeGroupId,GlobalEntityId,AttributeGroupDisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select GlobalAttributeGroupId,(select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Store'),9,2,getdate(),2,getdate()
from ZnodeGlobalAttributeGroup ZGAG
where GroupCode = 'QuotesSettings'
and not exists(Select * from ZnodeGlobalGroupEntityMapper ZGGEM where ZGAG.GlobalAttributeGroupId = ZGGEM.GlobalAttributeGroupId 
    and GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Store'))

insert into ZnodePortalGlobalAttributeValueLocale(PortalGlobalAttributeValueId,LocaleId,AttributeValue,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,GlobalAttributeDefaultValueId,MediaId,MediaPath)
select ZPGAV.PortalGlobalAttributeValueId, 1, 'true',2,getdate(),2,getdate(),null,null,null
from ZnodePortalGlobalAttributeValue ZPGAV
inner join ZnodePortal ZP ON ZP.PortalId = ZPGAV.PortalId
where ZPGAV.GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'EnableQuoteRequest')
and not exists(select * from ZnodePortalGlobalAttributeValueLocale ZPGAVL where ZPGAV.PortalGlobalAttributeValueId = ZPGAVL.PortalGlobalAttributeValueId and ZPGAVL.LocaleId = 1)

insert into ZnodePortalGlobalAttributeValueLocale(PortalGlobalAttributeValueId,LocaleId,AttributeValue,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,GlobalAttributeDefaultValueId,MediaId,MediaPath)
select ZPGAV.PortalGlobalAttributeValueId, 1, '30',2,getdate(),2,getdate(),null,null,null
from ZnodePortalGlobalAttributeValue ZPGAV
inner join ZnodePortal ZP ON ZP.PortalId = ZPGAV.PortalId
where ZPGAV.GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'QuoteExpireInDays')
and not exists(select * from ZnodePortalGlobalAttributeValueLocale ZPGAVL where ZPGAV.PortalGlobalAttributeValueId = ZPGAVL.PortalGlobalAttributeValueId and ZPGAVL.LocaleId = 1)

update ZnodeGlobalGroupEntityMapper set AttributeGroupDisplayOrder =2
where GlobalAttributeGroupId = (Select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'WebstoreAuthentication') 
and GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Store') 

update ZnodeGlobalGroupEntityMapper set AttributeGroupDisplayOrder =3
where GlobalAttributeGroupId = (Select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'Redirections') 
and GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Store') 

update ZnodeGlobalGroupEntityMapper set AttributeGroupDisplayOrder =999
where GlobalAttributeGroupId = (Select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'Budgets') 
and GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'User') 

update ZnodeGlobalGroupEntityMapper set AttributeGroupDisplayOrder =4
where GlobalAttributeGroupId = (Select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'EnableBudgetManagement') 
and GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Store') 

update ZnodeGlobalGroupEntityMapper set AttributeGroupDisplayOrder = 999
where GlobalAttributeGroupId = (Select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'OpenAccountBillingDetails') 
and GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'User') 

update ZnodeGlobalGroupEntityMapper set AttributeGroupDisplayOrder = 999
where GlobalAttributeGroupId = (Select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'UserAddressSettings') 
and GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'User') 

update ZnodeGlobalGroupEntityMapper set AttributeGroupDisplayOrder = 9
where GlobalAttributeGroupId = (Select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'StoreAddressSettings') 
and GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Store') 


update ZnodeGlobalGroupEntityMapper set AttributeGroupDisplayOrder = 10
where GlobalAttributeGroupId = (Select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'QuotesSettings') 
and GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Store') 

update ZnodeGlobalGroupEntityMapper set AttributeGroupDisplayOrder = 6
where GlobalAttributeGroupId = (Select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'Captcha') 
and GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Store') 

update ZnodeGlobalGroupEntityMapper set AttributeGroupDisplayOrder = 7
where GlobalAttributeGroupId = (Select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'ContentSecurityPolicy') 
and GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Store') 

update ZnodeGlobalGroupEntityMapper set AttributeGroupDisplayOrder = 8
where GlobalAttributeGroupId = (Select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'WarehouseStockLevels') 
and GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Store') 

update ZnodeGlobalGroupEntityMapper set AttributeGroupDisplayOrder = 1
where GlobalAttributeGroupId = (Select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'Cloudflaresetting') 
and GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Store') 

update ZnodeGlobalGroupEntityMapper set AttributeGroupDisplayOrder = 10
where GlobalAttributeGroupId = (Select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'QuotesSettings') 
and GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Store') 

--dt 06-07-2020 ZPD-11276
insert into ZnodeGlobalAttribute(AttributeTypeId,AttributeCode,IsRequired,IsLocalizable,IsActive,DisplayOrder,HelpDescription,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsSystemDefined)
select (select top 1 AttributeTypeId from ZnodeAttributeType where AttributeTypeName = 'Yes/No'),'EnablePowerBIReportOnWebStore',0,1,0,500,
null,2,getdate(),2,getdate(),0
where not exists(select * from ZnodeGlobalAttribute where  AttributeCode = 'EnablePowerBIReportOnWebStore')

insert into ZnodeGlobalAttributeLocale(LocaleId,GlobalAttributeId,AttributeName,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 1,GlobalAttributeId,'Enable Power BI Report On WebStore',null,2,getdate(),2,getdate()
from ZnodeGlobalAttribute a
where AttributeCode = 'EnablePowerBIReportOnWebStore' and 
not exists(select * from ZnodeGlobalAttributeLocale b where a.GlobalAttributeId = b.GlobalAttributeId and b.LocaleId = 1)

insert into ZnodeGlobalAttributeDefaultValue(GlobalAttributeId,AttributeDefaultValueCode,IsEditable,DisplayOrder,MediaId,SwatchText,IsDefault,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where  AttributeCode = 'EnablePowerBIReportOnWebStore'),'false',null,null,null,null,null,2,getdate(),2,getdate()
where not exists(select * from ZnodeGlobalAttributeDefaultValue where GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where  AttributeCode = 'EnablePowerBIReportOnWebStore') and AttributeDefaultValueCode = 'false')

insert into ZnodeGlobalAttributeGroup(GroupCode,	DisplayOrder,	CreatedBy,	CreatedDate,	ModifiedBy,	ModifiedDate,	IsSystemDefined)
select 'PowerBISettings',	NULL,	2,	getdate(),	2,	getdate(),	0
where not exists(select * from ZnodeGlobalAttributeGroup where GroupCode = 'PowerBISettings')

insert into ZnodeGlobalAttributeGroupMapper(GlobalAttributeGroupId,GlobalAttributeId,AttributeDisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'PowerBISettings'),
      (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where  AttributeCode = 'EnablePowerBIReportOnWebStore'),
	  NULL,	2,	getdate(),	2,	getdate()
where not exists(select * from  ZnodeGlobalAttributeGroupMapper where GlobalAttributeGroupId =  (select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'PowerBISettings')
      and  GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where  AttributeCode = 'EnablePowerBIReportOnWebStore'))

insert into ZnodeGlobalAttributeGroupLocale(LocaleId,GlobalAttributeGroupId,AttributeGroupName,Description
,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 1,(select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'PowerBISettings'),'Power BI Settings',
       null, 2,getdate(),2,getdate()
where not exists(select * from ZnodeGlobalAttributeGroupLocale where GlobalAttributeGroupId = (select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'PowerBISettings'))

insert into ZnodeGlobalGroupEntityMapper(GlobalAttributeGroupId,GlobalEntityId,AttributeGroupDisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'PowerBISettings'),
       (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'User'), 999,2,getdate(),2,getdate()
where not exists(select * from ZnodeGlobalGroupEntityMapper where GlobalAttributeGroupId = (select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'PowerBISettings')
      and GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'User'))

--dt 15-07-2020 ZPD-11382
update  ZnodeGlobalAttribute set DisplayOrder = 500 where AttributeCode = 'EnableQuoteRequest'

update  ZnodeGlobalAttribute set DisplayOrder = 499 where AttributeCode = 'QuoteExpireInDays'

---dt 30-07-2020 ZPD-11730
delete from ZnodeGlobalAttributeDefaultValueLocale where GlobalAttributeDefaultValueId in (
select GlobalAttributeDefaultValueId  from ZnodeGlobalAttributeDefaultValue where GlobalAttributeId in (
select GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode in ('firstname','lastname','workemailaddress','workphonenumber','businessname','city','zipcode')
))

delete from ZnodeGlobalAttributeDefaultValue where GlobalAttributeId in (
select GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode in ('firstname','lastname','workemailaddress','workphonenumber','businessname','city','zipcode')
)

--ZPD-1197
Declare @GlobalAttribute_EnableECertificate int,@GlobalAttribute_HideeProOrdersFromShopper int

select @GlobalAttribute_EnableECertificate = GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'EnableECertificate'

select @GlobalAttribute_HideeProOrdersFromShopper = GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'HideeProOrdersFromShopper'

delete from ZnodeGlobalAttributeGroupMapper
where GlobalAttributeId = @GlobalAttribute_EnableECertificate

delete from ZnodeGlobalAttributeGroupMapper
where GlobalAttributeId = @GlobalAttribute_HideeProOrdersFromShopper

execute [Znode_DeleteGlobalAttribute] @GlobalAttributeId = @GlobalAttribute_EnableECertificate,@Status = 0
execute [Znode_DeleteGlobalAttribute] @GlobalAttributeId = @GlobalAttribute_HideeProOrdersFromShopper, @Status = 0

--ZPD-11804 : dt- 16-09-2020
insert into ZnodeGlobalAttribute(AttributeTypeId,AttributeCode,IsRequired,IsLocalizable,IsActive,DisplayOrder, HelpDescription,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsSystemDefined,GlobalEntityId)
select (select top 1 AttributeTypeId from ZnodeAttributeType where AttributeTypeName = 'Number'),'AccountVoucherExpirationReminderEmailInDays',0,0,1,500,
'The value indicates that the set number of days prior to the expiration date a reminder email notification will be sent to the customers to use the voucher',2,getdate(),2,getdate(),0,(select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Account')
where not exists(select * from ZnodeGlobalAttribute where  AttributeCode = 'AccountVoucherExpirationReminderEmailInDays' AND GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Account'))

insert into ZnodeGlobalAttributeLocale(LocaleId,GlobalAttributeId,AttributeName,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 1,GlobalAttributeId,'Voucher Expiration Reminder Email (In Days)',null,2,getdate(),2,getdate()
from ZnodeGlobalAttribute a
where AttributeCode = 'AccountVoucherExpirationReminderEmailInDays' AND GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Account') and
not exists(select * from ZnodeGlobalAttributeLocale b where a.GlobalAttributeId = b.GlobalAttributeId and b.LocaleId = 1)


insert into ZnodeGlobalAttributeGroup(GroupCode, DisplayOrder, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, IsSystemDefined, GlobalEntityId)
select 'AccountVoucherSettings', NULL, 2, getdate(), 2, getdate(), 0, (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Account')
where not exists(select * from ZnodeGlobalAttributeGroup where GroupCode = 'AccountVoucherSettings' AND GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Account'))


insert into ZnodeGlobalAttributeGroupLocale(LocaleId,GlobalAttributeGroupId,AttributeGroupName,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select 1,GlobalAttributeGroupId,'Voucher Settings',null,2,getdate(),2,getdate()
from ZnodeGlobalAttributeGroup a
where a.GroupCode = 'AccountVoucherSettings' AND GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Account') and
not exists(select * from ZnodeGlobalAttributeGroupLocale b where a.GlobalAttributeGroupId = b.GlobalAttributeGroupId and b.LocaleId = 1)

insert into ZnodeGlobalAttributeGroupMapper(GlobalAttributeGroupId,GlobalAttributeId,AttributeDisplayOrder,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'AccountVoucherSettings'  AND GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Account')),
      (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where  AttributeCode = 'AccountVoucherExpirationReminderEmailInDays' AND GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Account')),
	  NULL,	2,	getdate(),	2,	getdate()
where not exists(select * from  ZnodeGlobalAttributeGroupMapper where GlobalAttributeGroupId =  (select top 1 GlobalAttributeGroupId from ZnodeGlobalAttributeGroup where GroupCode = 'AccountVoucherSettings' AND GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Account'))
      and  GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where  AttributeCode = 'AccountVoucherExpirationReminderEmailInDays' AND GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Account')))

INSERT INTO ZnodeGlobalFamilyGroupMapper (GlobalAttributeFamilyId, GlobalAttributeGroupId, AttributeGroupDisplayOrder,CreatedBy, CreatedDate, ModifiedBy, ModifiedDate)
SELECT
(SELECT top 1 GlobalAttributeFamilyId FROM ZnodeGlobalAttributeFamily WHERE FamilyCode = 'Account'),
(SELECT top 1 GlobalAttributeGroupId FROM ZnodeGlobalAttributeGroup WHERE GroupCode = 'AccountVoucherSettings' AND GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Account')), 999,
2, getdate(), 2, getdate()
WHERE NOT EXISTS(select * from ZnodeGlobalFamilyGroupMapper where
GlobalAttributeFamilyId = (SELECT top 1 GlobalAttributeFamilyId FROM ZnodeGlobalAttributeFamily WHERE FamilyCode = 'Account') and
GlobalAttributeGroupId = (SELECT top 1 GlobalAttributeGroupId FROM ZnodeGlobalAttributeGroup WHERE GroupCode = 'AccountVoucherSettings' AND GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Account'))
)


insert into ZnodeGlobalAttributeValidation (GlobalAttributeId,InputValidationId,InputValidationRuleId,Name,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'AccountVoucherExpirationReminderEmailInDays' AND GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Account')),(select top 1 InputValidationId from znodeattributeinputvalidation  where Name = 'AllowDecimals')
,null,'false',2,getdate(),2,getdate()
where not exists(select * from ZnodeGlobalAttributeValidation where GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'AccountVoucherExpirationReminderEmailInDays' AND GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Account'))
and InputValidationId = (select top 1 InputValidationId from znodeattributeinputvalidation  where Name = 'AllowDecimals'))

insert into ZnodeGlobalAttributeValidation (GlobalAttributeId,InputValidationId,InputValidationRuleId,Name,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'AccountVoucherExpirationReminderEmailInDays' AND GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Account')),(select top 1 InputValidationId from znodeattributeinputvalidation  where Name = 'AllowNegative')
,null,'false',2,getdate(),2,getdate()
where not exists(select * from ZnodeGlobalAttributeValidation where GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'AccountVoucherExpirationReminderEmailInDays' AND GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Account'))
and InputValidationId = (select top 1 InputValidationId from znodeattributeinputvalidation  where Name = 'AllowNegative'))


insert into ZnodeGlobalAttributeValidation (GlobalAttributeId,InputValidationId,InputValidationRuleId,Name,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'VoucherExpirationReminderEmailInDays' AND GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Store')),(select top 1 InputValidationId from znodeattributeinputvalidation  where Name = 'AllowDecimals')
,null,'false',2,getdate(),2,getdate()
where not exists(select * from ZnodeGlobalAttributeValidation where GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'VoucherExpirationReminderEmailInDays' AND GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Store'))
and InputValidationId = (select top 1 InputValidationId from znodeattributeinputvalidation  where Name = 'AllowDecimals'))


insert into ZnodeGlobalAttributeValidation (GlobalAttributeId,InputValidationId,InputValidationRuleId,Name,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
select (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'VoucherExpirationReminderEmailInDays' AND GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Store')),(select top 1 InputValidationId from znodeattributeinputvalidation  where Name = 'AllowNegative')
,null,'false',2,getdate(),2,getdate()
where not exists(select * from ZnodeGlobalAttributeValidation where GlobalAttributeId = (select top 1 GlobalAttributeId from ZnodeGlobalAttribute where AttributeCode = 'VoucherExpirationReminderEmailInDays' AND GlobalEntityId = (select top 1 GlobalEntityId from ZnodeGlobalEntity where EntityName = 'Store'))
and InputValidationId = (select top 1 InputValidationId from znodeattributeinputvalidation  where Name = 'AllowNegative'))



